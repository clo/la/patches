From 195cb8a108822b2b02c80da417e45d462ad17afe Mon Sep 17 00:00:00 2001
From: Kiran Rudramuni <kchitrik@codeaurora.org>
Date: Fri, 20 Mar 2015 17:17:45 -0700
Subject: [PATCH] msm: mdss: hdmi: cec: send power key on suspend resume

Handle CEC commands for suspend and resume and send power key event
to frameworks to suspend or resume device.

Change-Id: If7e5788d87788c77e3bf97541f67eb11a89dabf1
Signed-off-by: Ajay Singh Parmar <aparmar@codeaurora.org>
---
 drivers/video/msm/mdss/mdss_hdmi_cec.c | 89 ++++++++++++++++++++++++++++++----
 1 file changed, 80 insertions(+), 9 deletions(-)

diff --git a/drivers/video/msm/mdss/mdss_hdmi_cec.c b/drivers/video/msm/mdss/mdss_hdmi_cec.c
index b1a80c2..47e94ae 100644
--- a/drivers/video/msm/mdss/mdss_hdmi_cec.c
+++ b/drivers/video/msm/mdss/mdss_hdmi_cec.c
@@ -14,6 +14,7 @@
 #include <linux/list.h>
 #include <linux/types.h>
 #include <mach/board.h>
+#include <linux/input.h>
 
 #include "mdss_hdmi_cec.h"
 
@@ -25,6 +26,10 @@
 #define RETRANSMIT_MAX_NUM	5
 #define MAX_OPERAND_SIZE	15
 
+#define CEC_OP_SET_STREAM_PATH  0x86
+#define CEC_OP_KEY_PRESS        0x44
+#define CEC_OP_STANDBY          0x36
+
 /*
  * Ref. HDMI 1.4a: Supplement-1 CEC Section 6, 7
  */
@@ -47,6 +52,7 @@ struct hdmi_cec_ctrl {
 	bool compliance_response_enabled;
 	bool cec_engine_configed;
 	bool cec_wakeup_en;
+	bool tx_power_on;
 
 	u8 cec_logical_addr;
 	u32 cec_msg_wr_status;
@@ -56,6 +62,7 @@ struct hdmi_cec_ctrl {
 	struct work_struct cec_read_work;
 	struct completion cec_msg_wr_done;
 	struct hdmi_cec_init_data init_data;
+	struct input_dev *input;
 };
 
 static int hdmi_cec_msg_send(struct hdmi_cec_ctrl *cec_ctrl,
@@ -74,24 +81,25 @@ static void hdmi_cec_dump_msg(struct hdmi_cec_ctrl *cec_ctrl,
 	}
 
 	spin_lock_irqsave(&cec_ctrl->lock, flags);
-	DEV_DBG("=================%pS dump start =====================\n",
+	DEV_DBG("==%pS dump start==\n",
 		__builtin_return_address(0));
 
-	DEV_DBG("sender_id     : %d", msg->sender_id);
-	DEV_DBG("recvr_id      : %d", msg->recvr_id);
+	DEV_DBG("cec sender_id: %d\n", msg->sender_id);
+	DEV_DBG("cec recvr_id:  %d\n", msg->recvr_id);
 
 	if (msg->frame_size < 2) {
-		DEV_DBG("polling message");
+		DEV_DBG("polling message\n");
 		spin_unlock_irqrestore(&cec_ctrl->lock, flags);
 		return;
 	}
 
-	DEV_DBG("opcode        : %02x", msg->opcode);
+	DEV_DBG("cec opcode:    %02x\n", msg->opcode);
 	for (i = 0; i < msg->frame_size - 2; i++)
-		DEV_DBG("operand(%2d) : %02x", i + 1, msg->operand[i]);
+		DEV_DBG("cec operand(%2d) : %02x\n", i + 1, msg->operand[i]);
 
-	DEV_DBG("=================%pS dump end =====================\n",
+	DEV_DBG("==%pS dump end ==\n",
 		__builtin_return_address(0));
+
 	spin_unlock_irqrestore(&cec_ctrl->lock, flags);
 } /* hdmi_cec_dump_msg */
 
@@ -172,7 +180,6 @@ static int hdmi_cec_send_abort_opcode(struct hdmi_cec_ctrl *cec_ctrl,
 	out_msg.operand[i++] = in_msg->opcode;
 	out_msg.operand[i++] = reason_operand;
 	out_msg.frame_size = i + 2;
-
 	return hdmi_cec_msg_send(cec_ctrl, &out_msg);
 } /* hdmi_cec_send_abort_opcode */
 
@@ -389,6 +396,40 @@ static int hdmi_cec_msg_send(struct hdmi_cec_ctrl *cec_ctrl,
 	return rc;
 } /* hdmi_cec_msg_send */
 
+static void hdmi_cec_init_input_event(struct hdmi_cec_ctrl *cec_ctrl)
+{
+	int rc = 0;
+
+	/* Initialize CEC input events */
+	if (!cec_ctrl->input)
+		cec_ctrl->input = input_allocate_device();
+	if (!cec_ctrl->input) {
+		DEV_ERR("hdmi input device allocation failed\n");
+		return;
+	}
+
+	cec_ctrl->input->name = "HDMI CEC User or Deck Control";
+	cec_ctrl->input->phys = "hdmi/input0";
+	cec_ctrl->input->id.bustype = BUS_VIRTUAL;
+
+	input_set_capability(cec_ctrl->input, EV_KEY, KEY_POWER);
+
+	rc = input_register_device(cec_ctrl->input);
+	if (rc) {
+		DEV_ERR(KERN_ERR "cec input device registeration failed\n");
+		input_free_device(cec_ctrl->input);
+		cec_ctrl->input = NULL;
+		return;
+	}
+}
+
+static void hdmi_cec_deinit_input_event(struct hdmi_cec_ctrl *cec_ctrl)
+{
+	if (cec_ctrl->input)
+		input_unregister_device(cec_ctrl->input);
+	cec_ctrl->input = NULL;
+}
+
 static void hdmi_cec_msg_recv(struct work_struct *work)
 {
 	int i;
@@ -449,7 +490,32 @@ static void hdmi_cec_msg_recv(struct work_struct *work)
 	DEV_DBG("%s: CEC read frame done\n", __func__);
 	hdmi_cec_dump_msg(cec_ctrl, &msg_node->msg);
 
+	DEV_DBG("%s: opcode 0x%x, wakup_en %d, tx_power_on %d\n", __func__,
+		msg_node->msg.opcode, cec_ctrl->cec_wakeup_en,
+		cec_ctrl->tx_power_on);
+
 	spin_lock_irqsave(&cec_ctrl->lock, flags);
+	if ((msg_node->msg.opcode == CEC_OP_SET_STREAM_PATH ||
+		msg_node->msg.opcode == CEC_OP_KEY_PRESS) &&
+		cec_ctrl->input && cec_ctrl->cec_wakeup_en &&
+		!cec_ctrl->tx_power_on) {
+		DEV_DBG("%s: Sending power on at wakeup\n", __func__);
+		input_report_key(cec_ctrl->input, KEY_POWER, 1);
+		input_sync(cec_ctrl->input);
+		input_report_key(cec_ctrl->input, KEY_POWER, 0);
+		input_sync(cec_ctrl->input);
+	}
+
+	if ((msg_node->msg.opcode == CEC_OP_STANDBY) &&
+		cec_ctrl->input && cec_ctrl->cec_wakeup_en &&
+		cec_ctrl->tx_power_on) {
+		DEV_DBG("%s: Sending power off on standby\n", __func__);
+		input_report_key(cec_ctrl->input, KEY_POWER, 1);
+		input_sync(cec_ctrl->input);
+		input_report_key(cec_ctrl->input, KEY_POWER, 0);
+		input_sync(cec_ctrl->input);
+	}
+
 	if (cec_ctrl->compliance_response_enabled) {
 		spin_unlock_irqrestore(&cec_ctrl->lock, flags);
 
@@ -867,6 +933,8 @@ int hdmi_cec_deconfig(void *input)
 		return -EPERM;
 	}
 
+	cec_ctrl->tx_power_on = false;
+
 	if (cec_ctrl->cec_wakeup_en)
 		return 0;
 
@@ -922,6 +990,7 @@ int hdmi_cec_config(void *input)
 	}
 
 	cec_ctrl->cec_engine_configed = true;
+	cec_ctrl->tx_power_on = true;
 	spin_unlock_irqrestore(&cec_ctrl->lock, flags);
 
 	return 0;
@@ -942,6 +1011,8 @@ void hdmi_cec_deinit(void *input)
 		sysfs_remove_group(cec_ctrl->init_data.sysfs_kobj,
 			&hdmi_cec_fs_attr_group);
 
+		hdmi_cec_deinit_input_event(cec_ctrl);
+
 		kfree(cec_ctrl);
 	}
 } /* hdmi_cec_deinit */
@@ -973,7 +1044,7 @@ void *hdmi_cec_init(struct hdmi_cec_init_data *init_data)
 	INIT_LIST_HEAD(&cec_ctrl->msg_head);
 	INIT_WORK(&cec_ctrl->cec_read_work, hdmi_cec_msg_recv);
 	init_completion(&cec_ctrl->cec_msg_wr_done);
-
+	hdmi_cec_init_input_event(cec_ctrl);
 	goto exit;
 
 error:
-- 
1.8.2.1

