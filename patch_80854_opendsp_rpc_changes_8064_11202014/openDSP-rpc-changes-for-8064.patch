From d2d0581a7413b2006a299ed994866aa837445b80 Mon Sep 17 00:00:00 2001
From: Baji Patthan <bpatth@codeaurora.org>
Date: Tue, 18 Nov 2014 17:06:41 -0800
Subject: [PATCH] msm: open DSP changes for 8064

msm: Kernel open DSP changes for 8064

Change-Id: I29e918324981a1cfce45adb4deada3cfb3bd6ab9
Signed-off-by: Baji Patthan <bpatth@codeaurora.org>
---
 drivers/char/adsprpc.c        | 812 ++++++++++++++++++++++++++++++++----------
 drivers/char/adsprpc_shared.h |  31 +-
 2 files changed, 651 insertions(+), 192 deletions(-)
 mode change 100644 => 100755 drivers/char/adsprpc.c
 mode change 100644 => 100755 drivers/char/adsprpc_shared.h

diff --git a/drivers/char/adsprpc.c b/drivers/char/adsprpc.c
old mode 100644
new mode 100755
index 309368b..1dfb73d
--- a/drivers/char/adsprpc.c
+++ b/drivers/char/adsprpc.c
@@ -1,5 +1,5 @@
 /*
- * Copyright (c) 2012, The Linux Foundation. All rights reserved.
+ * Copyright (c) 2012-2014, The Linux Foundation. All rights reserved.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License version 2 and
@@ -13,8 +13,6 @@
  */
 #include "adsprpc_shared.h"
 
-#ifdef __KERNEL__
-
 #include <linux/slab.h>
 #include <linux/completion.h>
 #include <linux/pagemap.h>
@@ -32,6 +30,7 @@
 #include <linux/fs.h>
 #include <linux/uaccess.h>
 #include <linux/device.h>
+#include <linux/kref.h>
 
 #ifndef ION_ADSPRPC_HEAP_ID
 #define ION_ADSPRPC_HEAP_ID ION_AUDIO_HEAP_ID
@@ -55,6 +54,8 @@
 		} while (0)
 
 
+#define IS_CACHE_ALIGNED(x) (((x) & ((L1_CACHE_BYTES)-1)) == 0)
+
 static inline uint32_t buf_page_start(void *buf)
 {
 	uint32_t start = (uint32_t) buf & PAGE_MASK;
@@ -84,10 +85,11 @@ static inline uint32_t buf_page_size(uint32_t size)
 static inline int buf_get_pages(void *addr, int sz, int nr_pages, int access,
 				  struct smq_phy_page *pages, int nr_elems)
 {
-	struct vm_area_struct *vma;
+	struct vm_area_struct *vma, *vmaend;
 	uint32_t start = buf_page_start(addr);
+	uint32_t end = buf_page_start((void *)((uint32_t)addr + sz - 1));
 	uint32_t len = nr_pages << PAGE_SHIFT;
-	unsigned long pfn;
+	unsigned long pfn, pfnend;
 	int n = -1, err = 0;
 
 	VERIFY(err, 0 != access_ok(access ? VERIFY_WRITE : VERIFY_READ,
@@ -97,13 +99,19 @@ static inline int buf_get_pages(void *addr, int sz, int nr_pages, int access,
 	VERIFY(err, 0 != (vma = find_vma(current->mm, start)));
 	if (err)
 		goto bail;
-	VERIFY(err, ((uint32_t)addr + sz) <= vma->vm_end);
+	VERIFY(err, 0 != (vmaend = find_vma(current->mm, end)));
 	if (err)
 		goto bail;
 	n = 0;
 	VERIFY(err, 0 == follow_pfn(vma, start, &pfn));
 	if (err)
 		goto bail;
+	VERIFY(err, 0 == follow_pfn(vmaend, end, &pfnend));
+	if (err)
+		goto bail;
+	VERIFY(err, (pfn + nr_pages - 1) == pfnend);
+	if (err)
+		goto bail;
 	VERIFY(err, nr_elems > 0);
 	if (err)
 		goto bail;
@@ -114,18 +122,37 @@ static inline int buf_get_pages(void *addr, int sz, int nr_pages, int access,
 	return n;
 }
 
-#endif /*__KERNEL__*/
+struct fastrpc_buf {
+	struct ion_handle *handle;
+	void *virt;
+	ion_phys_addr_t phys;
+	int size;
+	int used;
+};
+
+struct smq_context_list;
 
 struct smq_invoke_ctx {
+	struct hlist_node hn;
 	struct completion work;
 	int retval;
-	atomic_t free;
+	int pid;
+	remote_arg_t *pra;
+	remote_arg_t *rpra;
+	struct fastrpc_buf obuf;
+	struct fastrpc_buf *abufs;
+	struct fastrpc_device *dev;
+	struct fastrpc_apps *apps;
+	int *fds;
+	struct ion_handle **handles;
+	int nbufs;
+	uint32_t sc;
 };
 
 struct smq_context_list {
-	struct smq_invoke_ctx *ls;
-	int size;
-	int last;
+	struct hlist_head pending;
+	struct hlist_head interrupted;
+	spinlock_t hlock;
 };
 
 struct fastrpc_apps {
@@ -136,18 +163,28 @@ struct fastrpc_apps {
 	struct cdev cdev;
 	struct class *class;
 	struct device *dev;
+	struct mutex smd_mutex;
 	dev_t dev_no;
 	spinlock_t wrlock;
 	spinlock_t hlock;
+	struct kref kref;
 	struct hlist_head htbl[RPC_HASH_SZ];
 };
 
-struct fastrpc_buf {
+struct fastrpc_mmap {
+	struct hlist_node hn;
 	struct ion_handle *handle;
 	void *virt;
 	ion_phys_addr_t phys;
+	uint32_t vaddrin;
+	uint32_t vaddrout;
 	int size;
-	int used;
+};
+
+struct file_data {
+	spinlock_t hlock;
+	struct hlist_head hlst;
+	uint32_t mode;
 };
 
 struct fastrpc_device {
@@ -162,8 +199,8 @@ static void free_mem(struct fastrpc_buf *buf)
 {
 	struct fastrpc_apps *me = &gfa;
 
-	if (buf->handle) {
-		if (buf->virt) {
+	if (!IS_ERR_OR_NULL(buf->handle)) {
+		if (!IS_ERR_OR_NULL(buf->virt)) {
 			ion_unmap_kernel(me->iclient, buf->handle);
 			buf->virt = 0;
 		}
@@ -172,27 +209,40 @@ static void free_mem(struct fastrpc_buf *buf)
 	}
 }
 
+static void free_map(struct fastrpc_mmap *map)
+{
+	struct fastrpc_apps *me = &gfa;
+	if (!IS_ERR_OR_NULL(map->handle)) {
+		if (!IS_ERR_OR_NULL(map->virt)) {
+			ion_unmap_kernel(me->iclient, map->handle);
+			map->virt = 0;
+		}
+		ion_free(me->iclient, map->handle);
+	}
+	map->handle = 0;
+}
+
 static int alloc_mem(struct fastrpc_buf *buf)
 {
 	struct ion_client *clnt = gfa.iclient;
 	struct sg_table *sg;
 	int err = 0;
-
-	buf->handle = ion_alloc(clnt, buf->size, SZ_4K,
-				ION_HEAP(ION_AUDIO_HEAP_ID), 0);
+	unsigned int heap;
+	buf->handle = 0;
+	buf->virt = 0;
+	buf->phys = 0;
+	heap = ION_HEAP(ION_ADSP_HEAP_ID) | ION_HEAP(ION_AUDIO_HEAP_ID);
+	buf->handle = ion_alloc(clnt, buf->size, SZ_4K, heap, ION_FLAG_CACHED);
 	VERIFY(err, 0 == IS_ERR_OR_NULL(buf->handle));
 	if (err)
 		goto bail;
-	buf->virt = 0;
-	VERIFY(err, 0 != (buf->virt = ion_map_kernel(clnt, buf->handle)));
+	buf->virt = ion_map_kernel(clnt, buf->handle);
+	VERIFY(err, 0 == IS_ERR_OR_NULL(buf->virt));
 	if (err)
 		goto bail;
 	VERIFY(err, 0 != (sg = ion_sg_table(clnt, buf->handle)));
 	if (err)
 		goto bail;
-	VERIFY(err, 1 == sg->nents);
-	if (err)
-		goto bail;
 	buf->phys = sg_dma_address(sg->sgl);
  bail:
 	if (err && !IS_ERR_OR_NULL(buf->handle))
@@ -200,66 +250,171 @@ static int alloc_mem(struct fastrpc_buf *buf)
 	return err;
 }
 
-static int context_list_ctor(struct smq_context_list *me, int size)
+static int context_restore_interrupted(struct fastrpc_apps *me,
+				struct fastrpc_ioctl_invoke_fd *invokefd,
+				struct smq_invoke_ctx **po)
 {
 	int err = 0;
-	VERIFY(err, 0 != (me->ls = kzalloc(size, GFP_KERNEL)));
-	if (err)
-		goto bail;
-	me->size = size / sizeof(*me->ls);
-	me->last = 0;
- bail:
+	struct smq_invoke_ctx *ctx = 0, *ictx = 0;
+	struct hlist_node *pos, *n;
+	struct fastrpc_ioctl_invoke *invoke = &invokefd->inv;
+	spin_lock(&me->clst.hlock);
+	hlist_for_each_entry_safe(ictx, pos, n, &me->clst.interrupted, hn) {
+		if (ictx->pid == current->pid) {
+			if (invoke->sc != ictx->sc)
+				err = -1;
+			else {
+				ctx = ictx;
+				hlist_del(&ctx->hn);
+				hlist_add_head(&ctx->hn, &me->clst.pending);
+			}
+			break;
+		}
+	}
+	spin_unlock(&me->clst.hlock);
+	if (ctx)
+		*po = ctx;
 	return err;
 }
 
-static void context_list_dtor(struct smq_context_list *me)
+static int context_alloc(struct fastrpc_apps *me, uint32_t kernel,
+				struct fastrpc_ioctl_invoke_fd *invokefd,
+				struct smq_invoke_ctx **po)
 {
-	kfree(me->ls);
-	me->ls = 0;
-}
+	int err = 0, bufs, size = 0;
+	struct smq_invoke_ctx *ctx = 0;
+	struct smq_context_list *clst = &me->clst;
+	struct fastrpc_ioctl_invoke *invoke = &invokefd->inv;
+
+	bufs = REMOTE_SCALARS_INBUFS(invoke->sc) +
+			REMOTE_SCALARS_OUTBUFS(invoke->sc);
+	if (bufs) {
+		size = bufs * sizeof(*ctx->pra);
+		if (invokefd->fds)
+			size = size + bufs * sizeof(*ctx->fds) +
+				bufs * sizeof(*ctx->handles);
+	}
 
-static void context_list_alloc_ctx(struct smq_context_list *me,
-					struct smq_invoke_ctx **po)
-{
-	int i = me->last;
-	struct smq_invoke_ctx *ctx;
+	VERIFY(err, 0 != (ctx = kzalloc(sizeof(*ctx) + size, GFP_KERNEL)));
+	if (err)
+		goto bail;
 
-	for (;;) {
-		i = i % me->size;
-		ctx = &me->ls[i];
-		if (atomic_read(&ctx->free) == 0)
-			if (atomic_cmpxchg(&ctx->free, 0, 1) == 0)
-				break;
-		i++;
+	INIT_HLIST_NODE(&ctx->hn);
+	ctx->pra = (remote_arg_t *)(&ctx[1]);
+	ctx->fds = invokefd->fds == 0 ? 0 : (int *)(&ctx->pra[bufs]);
+	ctx->handles = invokefd->fds == 0 ? 0 :
+					(struct ion_handle **)(&ctx->fds[bufs]);
+	if (!kernel) {
+		VERIFY(err, 0 == copy_from_user(ctx->pra, invoke->pra,
+					bufs * sizeof(*ctx->pra)));
+		if (err)
+			goto bail;
+	} else {
+		memmove(ctx->pra, invoke->pra, bufs * sizeof(*ctx->pra));
 	}
-	me->last = i;
+
+	if (invokefd->fds) {
+		if (!kernel) {
+			VERIFY(err, 0 == copy_from_user(ctx->fds, invokefd->fds,
+						bufs * sizeof(*ctx->fds)));
+			if (err)
+				goto bail;
+		} else {
+			memmove(ctx->fds, invokefd->fds,
+						bufs * sizeof(*ctx->fds));
+		}
+	}
+	ctx->sc = invoke->sc;
 	ctx->retval = -1;
+	ctx->pid = current->pid;
+	ctx->apps = me;
 	init_completion(&ctx->work);
+	spin_lock(&clst->hlock);
+	hlist_add_head(&ctx->hn, &clst->pending);
+	spin_unlock(&clst->hlock);
+
 	*po = ctx;
+bail:
+	if (ctx && err)
+		kfree(ctx);
+	return err;
+}
+
+static void context_save_interrupted(struct smq_invoke_ctx *ctx)
+{
+	struct smq_context_list *clst = &ctx->apps->clst;
+	spin_lock(&clst->hlock);
+	hlist_del(&ctx->hn);
+	hlist_add_head(&ctx->hn, &clst->interrupted);
+	spin_unlock(&clst->hlock);
 }
 
-static void context_free(struct smq_invoke_ctx *me)
+static void add_dev(struct fastrpc_apps *me, struct fastrpc_device *dev);
+
+static void context_free(struct smq_invoke_ctx *ctx, bool lock)
 {
-	if (me)
-		atomic_set(&me->free, 0);
+	struct smq_context_list *clst = &ctx->apps->clst;
+	struct fastrpc_apps *apps = ctx->apps;
+	struct fastrpc_buf *b;
+	int i;
+	for (i = 0, b = ctx->abufs; i < ctx->nbufs; ++i, ++b)
+		free_mem(b);
+
+	kfree(ctx->abufs);
+	if (ctx->dev) {
+		add_dev(apps, ctx->dev);
+		if (ctx->obuf.handle != ctx->dev->buf.handle)
+			free_mem(&ctx->obuf);
+	}
+	if (lock)
+		spin_lock(&clst->hlock);
+	hlist_del(&ctx->hn);
+	if (lock)
+		spin_unlock(&clst->hlock);
+	kfree(ctx);
 }
 
-static void context_notify_user(struct smq_invoke_ctx *me, int retval)
+static void context_notify_user(struct smq_invoke_ctx *ctx, int retval)
 {
-	me->retval = retval;
-	complete(&me->work);
+	ctx->retval = retval;
+	complete(&ctx->work);
 }
 
 static void context_notify_all_users(struct smq_context_list *me)
 {
-	int i;
+	struct smq_invoke_ctx *ictx = 0;
+	struct hlist_node *pos, *n;
+	spin_lock(&me->hlock);
+	hlist_for_each_entry_safe(ictx, pos, n, &me->pending, hn) {
+		complete(&ictx->work);
+	}
+	hlist_for_each_entry_safe(ictx, pos, n, &me->interrupted, hn) {
+		complete(&ictx->work);
+	}
+	spin_unlock(&me->hlock);
+
+}
 
-	if (!me->ls)
-		return;
-	for (i = 0; i < me->size; ++i) {
-		if (atomic_read(&me->ls[i].free) != 0)
-			complete(&me->ls[i].work);
+static void context_list_ctor(struct smq_context_list *me)
+{
+	INIT_HLIST_HEAD(&me->interrupted);
+	INIT_HLIST_HEAD(&me->pending);
+	spin_lock_init(&me->hlock);
+}
+
+static void context_list_dtor(struct fastrpc_apps *me,
+				struct smq_context_list *clst)
+{
+	struct smq_invoke_ctx *ictx = 0;
+	struct hlist_node *pos, *n;
+	spin_lock(&clst->hlock);
+	hlist_for_each_entry_safe(ictx, pos, n, &clst->interrupted, hn) {
+		context_free(ictx, 0);
+	}
+	hlist_for_each_entry_safe(ictx, pos, n, &clst->pending, hn) {
+		context_free(ictx, 0);
 	}
+	spin_unlock(&clst->hlock);
 }
 
 static int get_page_list(uint32_t kernel, uint32_t sc, remote_arg_t *pra,
@@ -281,7 +436,6 @@ static int get_page_list(uint32_t kernel, uint32_t sc, remote_arg_t *pra,
 	if (rlen < 0) {
 		rlen = ((uint32_t)pages - (uint32_t)obuf->virt) - obuf->size;
 		obuf->size += buf_page_size(rlen);
-		obuf->handle = 0;
 		VERIFY(err, 0 == alloc_mem(obuf));
 		if (err)
 			goto bail;
@@ -296,6 +450,9 @@ static int get_page_list(uint32_t kernel, uint32_t sc, remote_arg_t *pra,
 		list[i].num = 0;
 		list[i].pgidx = 0;
 		len = pra[i].buf.len;
+		VERIFY(err, len >= 0);
+		if (err)
+			goto bail;
 		if (!len)
 			continue;
 		buf = pra[i].buf.pv;
@@ -318,7 +475,6 @@ static int get_page_list(uint32_t kernel, uint32_t sc, remote_arg_t *pra,
 			if (obuf->handle != ibuf->handle)
 				free_mem(obuf);
 			obuf->size += buf_page_size(sizeof(*pages));
-			obuf->handle = 0;
 			VERIFY(err, 0 == alloc_mem(obuf));
 			if (err)
 				goto bail;
@@ -337,7 +493,7 @@ static int get_page_list(uint32_t kernel, uint32_t sc, remote_arg_t *pra,
 static int get_args(uint32_t kernel, uint32_t sc, remote_arg_t *pra,
 			remote_arg_t *rpra, remote_arg_t *upra,
 			struct fastrpc_buf *ibuf, struct fastrpc_buf **abufs,
-			int *nbufs)
+			int *nbufs, int *fds, struct ion_handle **handles)
 {
 	struct smq_invoke_buf *list;
 	struct fastrpc_buf *pbuf = ibuf, *obufs = 0;
@@ -353,7 +509,6 @@ static int get_args(uint32_t kernel, uint32_t sc, remote_arg_t *pra,
 	args = (void *)((char *)pbuf->virt + used);
 	rlen = pbuf->size - used;
 	for (i = 0; i < inbufs + outbufs; ++i) {
-		int num;
 
 		rpra[i].buf.len = pra[i].buf.len;
 		if (!rpra[i].buf.len)
@@ -380,18 +535,12 @@ static int get_args(uint32_t kernel, uint32_t sc, remote_arg_t *pra,
 			args = pbuf->virt;
 			rlen = pbuf->size;
 		}
-		num = buf_num_pages(args, pra[i].buf.len);
-		if (pbuf == ibuf) {
-			list[i].num = num;
-			list[i].pgidx = 0;
-		} else {
-			list[i].num = 1;
-			pages[list[i].pgidx].addr =
-				buf_page_start((void *)(pbuf->phys +
-							 (pbuf->size - rlen)));
-			pages[list[i].pgidx].size =
-				buf_page_size(pra[i].buf.len);
-		}
+		list[i].num = 1;
+		pages[list[i].pgidx].addr =
+			buf_page_start((void *)(pbuf->phys +
+						 (pbuf->size - rlen)));
+		pages[list[i].pgidx].size =
+			buf_page_size(pra[i].buf.len);
 		if (i < inbufs) {
 			if (!kernel) {
 				VERIFY(err, 0 == copy_from_user(args,
@@ -441,10 +590,15 @@ static int put_args(uint32_t kernel, uint32_t sc, remote_arg_t *pra,
 	outbufs = REMOTE_SCALARS_OUTBUFS(sc);
 	for (i = inbufs; i < inbufs + outbufs; ++i) {
 		if (rpra[i].buf.pv != pra[i].buf.pv) {
-			VERIFY(err, 0 == copy_to_user(pra[i].buf.pv,
+			if (!kernel) {
+				VERIFY(err, 0 == copy_to_user(pra[i].buf.pv,
 					rpra[i].buf.pv, rpra[i].buf.len));
-			if (err)
-				goto bail;
+				if (err)
+					goto bail;
+			} else {
+				memmove(pra[i].buf.pv, rpra[i].buf.pv,
+							rpra[i].buf.len);
+			}
 		}
 	}
 	size = sizeof(*rpra) * REMOTE_SCALARS_OUTHANDLES(sc);
@@ -463,6 +617,28 @@ static int put_args(uint32_t kernel, uint32_t sc, remote_arg_t *pra,
 	return err;
 }
 
+static void inv_args_pre(uint32_t sc, remote_arg_t *rpra)
+{
+	int i, inbufs, outbufs;
+	uint32_t end;
+
+	inbufs = REMOTE_SCALARS_INBUFS(sc);
+	outbufs = REMOTE_SCALARS_OUTBUFS(sc);
+	for (i = inbufs; i < inbufs + outbufs; ++i) {
+		if (!rpra[i].buf.len)
+			continue;
+		if (buf_page_start(rpra) == buf_page_start(rpra[i].buf.pv))
+			continue;
+		if (!IS_CACHE_ALIGNED((uint32_t)rpra[i].buf.pv))
+			dmac_flush_range(rpra[i].buf.pv,
+				(char *)rpra[i].buf.pv + 1);
+		end = (uint32_t)rpra[i].buf.pv + rpra[i].buf.len;
+		if (!IS_CACHE_ALIGNED(end))
+			dmac_flush_range((char *)end,
+				(char *)end + 1);
+	}
+}
+
 static void inv_args(uint32_t sc, remote_arg_t *rpra, int used)
 {
 	int i, inbufs, outbufs;
@@ -482,15 +658,17 @@ static void inv_args(uint32_t sc, remote_arg_t *rpra, int used)
 		dmac_inv_range(rpra, (char *)rpra + used);
 }
 
-static int fastrpc_invoke_send(struct fastrpc_apps *me, uint32_t handle,
+static int fastrpc_invoke_send(struct fastrpc_apps *me,
+				 uint32_t kernel, uint32_t handle,
 				 uint32_t sc, struct smq_invoke_ctx *ctx,
 				 struct fastrpc_buf *buf)
 {
 	struct smq_msg msg;
 	int err = 0, len;
-
 	msg.pid = current->tgid;
 	msg.tid = current->pid;
+	if (kernel)
+		msg.pid = 0;
 	msg.invoke.header.ctx = ctx;
 	msg.invoke.header.handle = handle;
 	msg.invoke.header.sc = sc;
@@ -507,11 +685,8 @@ static void fastrpc_deinit(void)
 {
 	struct fastrpc_apps *me = &gfa;
 
-	if (me->chan)
-		(void)smd_close(me->chan);
-	context_list_dtor(&me->clst);
-	if (me->iclient)
-		ion_client_destroy(me->iclient);
+	smd_close(me->chan);
+	ion_client_destroy(me->iclient);
 	me->iclient = 0;
 	me->chan = 0;
 }
@@ -560,29 +735,20 @@ static int fastrpc_init(void)
 		spin_lock_init(&me->hlock);
 		spin_lock_init(&me->wrlock);
 		init_completion(&me->work);
+		mutex_init(&me->smd_mutex);
+		context_list_ctor(&me->clst);
 		for (i = 0; i < RPC_HASH_SZ; ++i)
 			INIT_HLIST_HEAD(&me->htbl[i]);
-		VERIFY(err, 0 == context_list_ctor(&me->clst, SZ_4K));
-		if (err)
-			goto bail;
 		me->iclient = msm_ion_client_create(ION_HEAP_CARVEOUT_MASK,
 							DEVICE_NAME);
 		VERIFY(err, 0 == IS_ERR_OR_NULL(me->iclient));
 		if (err)
 			goto bail;
-		VERIFY(err, 0 == smd_named_open_on_edge(FASTRPC_SMD_GUID,
-						SMD_APPS_QDSP, &me->chan,
-						me, smd_event_handler));
-		if (err)
-			goto bail;
-		VERIFY(err, 0 != wait_for_completion_timeout(&me->work,
-							RPC_TIMEOUT));
-		if (err)
-			goto bail;
 	}
- bail:
-	if (err)
-		fastrpc_deinit();
+
+	return 0;
+
+bail:
 	return err;
 }
 
@@ -606,12 +772,15 @@ static int alloc_dev(struct fastrpc_device **dev)
 	VERIFY(err, 0 != (fd = kzalloc(sizeof(*fd), GFP_KERNEL)));
 	if (err)
 		goto bail;
+
+	INIT_HLIST_NODE(&fd->hn);
+
 	fd->buf.size = PAGE_SIZE;
 	VERIFY(err, 0 == alloc_mem(&fd->buf));
 	if (err)
 		goto bail;
 	fd->tgid = current->tgid;
-	INIT_HLIST_NODE(&fd->hn);
+
 	*dev = fd;
  bail:
 	if (err)
@@ -663,74 +832,81 @@ static void add_dev(struct fastrpc_apps *me, struct fastrpc_device *dev)
 
 static int fastrpc_release_current_dsp_process(void);
 
-static int fastrpc_internal_invoke(struct fastrpc_apps *me, uint32_t kernel,
-			struct fastrpc_ioctl_invoke *invoke, remote_arg_t *pra)
+static int fastrpc_internal_invoke(struct fastrpc_apps *me, uint32_t mode,
+			uint32_t kernel,
+			struct fastrpc_ioctl_invoke_fd *invokefd)
 {
-	remote_arg_t *rpra = 0;
-	struct fastrpc_device *dev = 0;
 	struct smq_invoke_ctx *ctx = 0;
-	struct fastrpc_buf obuf, *abufs = 0, *b;
+	struct fastrpc_ioctl_invoke *invoke = &invokefd->inv;
 	int interrupted = 0;
-	uint32_t sc;
-	int i, nbufs = 0, err = 0;
+	int err = 0;
 
-	sc = invoke->sc;
-	obuf.handle = 0;
-	if (REMOTE_SCALARS_LENGTH(sc)) {
-		VERIFY(err, 0 == get_dev(me, &dev));
+	if (!kernel) {
+		VERIFY(err, 0 == context_restore_interrupted(me, invokefd,
+								&ctx));
 		if (err)
 			goto bail;
-		VERIFY(err, 0 == get_page_list(kernel, sc, pra, &dev->buf,
-						&obuf));
+		if (ctx)
+			goto wait;
+	}
+
+	VERIFY(err, 0 == context_alloc(me, kernel, invokefd, &ctx));
+	if (err)
+		goto bail;
+
+	if (REMOTE_SCALARS_LENGTH(ctx->sc)) {
+		VERIFY(err, 0 == get_dev(me, &ctx->dev));
+		if (err)
+			goto bail;
+		VERIFY(err, 0 == get_page_list(kernel, ctx->sc, ctx->pra,
+					&ctx->dev->buf, &ctx->obuf));
 		if (err)
 			goto bail;
-		rpra = (remote_arg_t *)obuf.virt;
-		VERIFY(err, 0 == get_args(kernel, sc, pra, rpra, invoke->pra,
-					&obuf, &abufs, &nbufs));
+		ctx->rpra = (remote_arg_t *)ctx->obuf.virt;
+		VERIFY(err, 0 == get_args(kernel, ctx->sc, ctx->pra, ctx->rpra,
+				invoke->pra, &ctx->obuf, &ctx->abufs,
+				&ctx->nbufs, ctx->fds, ctx->handles));
 		if (err)
 			goto bail;
 	}
 
-	context_list_alloc_ctx(&me->clst, &ctx);
-	VERIFY(err, 0 == fastrpc_invoke_send(me, invoke->handle, sc, ctx,
-						&obuf));
-	if (err)
-		goto bail;
-	inv_args(sc, rpra, obuf.used);
-	VERIFY(err, 0 == (interrupted =
-			wait_for_completion_interruptible(&ctx->work)));
+	inv_args_pre(ctx->sc, ctx->rpra);
+	if (FASTRPC_MODE_SERIAL == mode)
+		inv_args(ctx->sc, ctx->rpra, ctx->obuf.used);
+	VERIFY(err, 0 == fastrpc_invoke_send(me, kernel, invoke->handle,
+						ctx->sc, ctx, &ctx->obuf));
 	if (err)
 		goto bail;
+	if (FASTRPC_MODE_PARALLEL == mode)
+		inv_args(ctx->sc, ctx->rpra, ctx->obuf.used);
+ wait:
+	if (kernel)
+		wait_for_completion(&ctx->work);
+	else {
+		interrupted = wait_for_completion_interruptible(&ctx->work);
+		VERIFY(err, 0 == (err = interrupted));
+		if (err)
+			goto bail;
+	}
 	VERIFY(err, 0 == (err = ctx->retval));
 	if (err)
 		goto bail;
-	VERIFY(err, 0 == put_args(kernel, sc, pra, rpra, invoke->pra));
+	VERIFY(err, 0 == put_args(kernel, ctx->sc, ctx->pra, ctx->rpra,
+					invoke->pra));
 	if (err)
 		goto bail;
  bail:
-	if (interrupted) {
-		if (!kernel)
-			(void)fastrpc_release_current_dsp_process();
-		wait_for_completion(&ctx->work);
-	}
-	context_free(ctx);
-
-	for (i = 0, b = abufs; i < nbufs; ++i, ++b)
-		free_mem(b);
-
-	kfree(abufs);
-	if (dev) {
-		add_dev(me, dev);
-		if (obuf.handle != dev->buf.handle)
-			free_mem(&obuf);
-	}
+	if (ctx && interrupted == -ERESTARTSYS)
+		context_save_interrupted(ctx);
+	else if (ctx)
+		context_free(ctx, 1);
 	return err;
 }
 
 static int fastrpc_create_current_dsp_process(void)
 {
 	int err = 0;
-	struct fastrpc_ioctl_invoke ioctl;
+	struct fastrpc_ioctl_invoke_fd ioctl;
 	struct fastrpc_apps *me = &gfa;
 	remote_arg_t ra[1];
 	int tgid = 0;
@@ -738,10 +914,12 @@ static int fastrpc_create_current_dsp_process(void)
 	tgid = current->tgid;
 	ra[0].buf.pv = &tgid;
 	ra[0].buf.len = sizeof(tgid);
-	ioctl.handle = 1;
-	ioctl.sc = REMOTE_SCALARS_MAKE(0, 1, 0);
-	ioctl.pra = ra;
-	VERIFY(err, 0 == fastrpc_internal_invoke(me, 1, &ioctl, ra));
+	ioctl.inv.handle = 1;
+	ioctl.inv.sc = REMOTE_SCALARS_MAKE(0, 1, 0);
+	ioctl.inv.pra = ra;
+	ioctl.fds = 0;
+	VERIFY(err, 0 == (err = fastrpc_internal_invoke(me,
+		FASTRPC_MODE_PARALLEL, 1, &ioctl)));
 	return err;
 }
 
@@ -749,17 +927,174 @@ static int fastrpc_release_current_dsp_process(void)
 {
 	int err = 0;
 	struct fastrpc_apps *me = &gfa;
-	struct fastrpc_ioctl_invoke ioctl;
+	struct fastrpc_ioctl_invoke_fd ioctl;
 	remote_arg_t ra[1];
 	int tgid = 0;
 
 	tgid = current->tgid;
 	ra[0].buf.pv = &tgid;
 	ra[0].buf.len = sizeof(tgid);
-	ioctl.handle = 1;
-	ioctl.sc = REMOTE_SCALARS_MAKE(1, 1, 0);
-	ioctl.pra = ra;
-	VERIFY(err, 0 == fastrpc_internal_invoke(me, 1, &ioctl, ra));
+	ioctl.inv.handle = 1;
+	ioctl.inv.sc = REMOTE_SCALARS_MAKE(1, 1, 0);
+	ioctl.inv.pra = ra;
+	ioctl.fds = 0;
+	VERIFY(err, 0 == (err = fastrpc_internal_invoke(me,
+		FASTRPC_MODE_PARALLEL, 1, &ioctl)));
+	return err;
+}
+
+static int fastrpc_mmap_on_dsp(struct fastrpc_apps *me,
+					 struct fastrpc_ioctl_mmap *mmap,
+					 struct smq_phy_page *pages,
+					 int num)
+{
+	struct fastrpc_ioctl_invoke_fd ioctl;
+	remote_arg_t ra[3];
+	int err = 0;
+	struct {
+		int pid;
+		uint32_t flags;
+		uint32_t vaddrin;
+		int num;
+	} inargs;
+
+	struct {
+		uint32_t vaddrout;
+	} routargs;
+	inargs.pid = current->tgid;
+	inargs.vaddrin = mmap->vaddrin;
+	inargs.flags = mmap->flags;
+	inargs.num = num;
+	ra[0].buf.pv = &inargs;
+	ra[0].buf.len = sizeof(inargs);
+
+	ra[1].buf.pv = pages;
+	ra[1].buf.len = num * sizeof(*pages);
+
+	ra[2].buf.pv = &routargs;
+	ra[2].buf.len = sizeof(routargs);
+
+	ioctl.inv.handle = 1;
+	ioctl.inv.sc = REMOTE_SCALARS_MAKE(2, 2, 1);
+	ioctl.inv.pra = ra;
+	ioctl.fds = 0;
+	VERIFY(err, 0 == (err = fastrpc_internal_invoke(me,
+		FASTRPC_MODE_PARALLEL, 1, &ioctl)));
+	mmap->vaddrout = routargs.vaddrout;
+	if (err)
+		goto bail;
+bail:
+	return err;
+}
+
+static int fastrpc_munmap_on_dsp(struct fastrpc_apps *me,
+				 struct fastrpc_ioctl_munmap *munmap)
+{
+	struct fastrpc_ioctl_invoke_fd ioctl;
+	remote_arg_t ra[1];
+	int err = 0;
+	struct {
+		int pid;
+		uint32_t vaddrout;
+		int size;
+	} inargs;
+
+	inargs.pid = current->tgid;
+	inargs.size = munmap->size;
+	inargs.vaddrout = munmap->vaddrout;
+	ra[0].buf.pv = &inargs;
+	ra[0].buf.len = sizeof(inargs);
+
+	ioctl.inv.handle = 1;
+	ioctl.inv.sc = REMOTE_SCALARS_MAKE(3, 1, 0);
+	ioctl.inv.pra = ra;
+	ioctl.fds = 0;
+	VERIFY(err, 0 == (err = fastrpc_internal_invoke(me,
+		FASTRPC_MODE_PARALLEL, 1, &ioctl)));
+	return err;
+}
+
+static int fastrpc_internal_munmap(struct fastrpc_apps *me,
+				   struct file_data *fdata,
+				   struct fastrpc_ioctl_munmap *munmap)
+{
+	int err = 0;
+	struct fastrpc_mmap *map = 0, *mapfree = 0;
+	struct hlist_node *pos, *n;
+	VERIFY(err, 0 == (err = fastrpc_munmap_on_dsp(me, munmap)));
+	if (err)
+		goto bail;
+	spin_lock(&fdata->hlock);
+	hlist_for_each_entry_safe(map, pos, n, &fdata->hlst, hn) {
+		if (map->vaddrout == munmap->vaddrout &&
+		    map->size == munmap->size) {
+			hlist_del(&map->hn);
+			mapfree = map;
+			map = 0;
+			break;
+		}
+	}
+	spin_unlock(&fdata->hlock);
+bail:
+	if (mapfree) {
+		free_map(mapfree);
+		kfree(mapfree);
+	}
+	return err;
+}
+
+
+static int fastrpc_internal_mmap(struct fastrpc_apps *me,
+				 struct file_data *fdata,
+				 struct fastrpc_ioctl_mmap *mmap)
+{
+	struct ion_client *clnt = gfa.iclient;
+	struct fastrpc_mmap *map = 0;
+	struct smq_phy_page *pages = 0;
+	void *buf;
+	unsigned long len;
+	int num;
+	int err = 0;
+
+	VERIFY(err, 0 != (map = kzalloc(sizeof(*map), GFP_KERNEL)));
+	if (err)
+		goto bail;
+	map->handle = ion_import_dma_buf(clnt, mmap->fd);
+	VERIFY(err, 0 == IS_ERR_OR_NULL(map->handle));
+	if (err)
+		goto bail;
+	map->virt = ion_map_kernel(clnt, map->handle);
+	VERIFY(err, 0 == IS_ERR_OR_NULL(map->virt));
+	if (err)
+		goto bail;
+	buf = (void *)mmap->vaddrin;
+	len =  mmap->size;
+	num = buf_num_pages(buf, len);
+	VERIFY(err, 0 != (pages = kzalloc(num * sizeof(*pages), GFP_KERNEL)));
+	if (err)
+		goto bail;
+
+	VERIFY(err, 0 < (num = buf_get_pages(buf, len, num, 1,
+						pages, num)));
+	if (err)
+		goto bail;
+
+	VERIFY(err, 0 == fastrpc_mmap_on_dsp(me, mmap, pages, num));
+	if (err)
+		goto bail;
+	map->vaddrin = mmap->vaddrin;
+	map->vaddrout = mmap->vaddrout;
+	map->size = mmap->size;
+	INIT_HLIST_NODE(&map->hn);
+	spin_lock(&fdata->hlock);
+	hlist_add_head(&map->hn, &fdata->hlst);
+	spin_unlock(&fdata->hlock);
+ bail:
+	if (err && map) {
+		free_map(map);
+		kfree(map);
+	}
+	kfree(pages);
 	return err;
 }
 
@@ -790,27 +1125,97 @@ static void cleanup_current_dev(void)
 	return;
 }
 
+static void fastrpc_channel_close(struct kref *kref)
+{
+	struct fastrpc_apps *me = &gfa;
+
+	smd_close(me->chan);
+	me->chan = 0;
+	pr_info("'closed /dev/%s c %d 0'\n", DEVICE_NAME,
+						MAJOR(me->dev_no));
+}
+
 static int fastrpc_device_release(struct inode *inode, struct file *file)
 {
+	struct file_data *fdata = (struct file_data *)file->private_data;
+	struct fastrpc_apps *me = &gfa;
+
 	(void)fastrpc_release_current_dsp_process();
 	cleanup_current_dev();
+	if (fdata) {
+		struct fastrpc_mmap *map = 0;
+		struct hlist_node *n, *pos;
+		file->private_data = 0;
+		hlist_for_each_entry_safe(map, pos, n, &fdata->hlst, hn) {
+			hlist_del(&map->hn);
+			free_map(map);
+			kfree(map);
+		}
+		kfree(fdata);
+		mutex_lock(&me->smd_mutex);
+		kref_put(&me->kref, fastrpc_channel_close);
+		mutex_unlock(&me->smd_mutex);
+	}
 	return 0;
 }
 
 static int fastrpc_device_open(struct inode *inode, struct file *filp)
 {
 	int err = 0;
+	struct fastrpc_apps *me = &gfa;
 
+	mutex_lock(&me->smd_mutex);
+	if (kref_get_unless_zero(&me->kref) == 0) {
+		VERIFY(err, 0 == smd_named_open_on_edge(FASTRPC_SMD_GUID,
+						SMD_APPS_QDSP, &me->chan,
+						me, smd_event_handler));
+		if (err)
+			goto smd_bail;
+		VERIFY(err, 0 != wait_for_completion_timeout(&me->work,
+							RPC_TIMEOUT));
+		if (err)
+			goto completion_bail;
+		kref_init(&me->kref);
+		pr_info("'opened /dev/%s c %d 0'\n", DEVICE_NAME,
+						MAJOR(me->dev_no));
+	}
+	mutex_unlock(&me->smd_mutex);
+
+	filp->private_data = 0;
 	if (0 != try_module_get(THIS_MODULE)) {
+		struct file_data *fdata = 0;
 		/* This call will cause a dev to be created
 		 * which will addref this module
 		 */
+		VERIFY(err, 0 != (fdata = kzalloc(sizeof(*fdata), GFP_KERNEL)));
+		if (err)
+			goto bail;
+
+		spin_lock_init(&fdata->hlock);
+		INIT_HLIST_HEAD(&fdata->hlst);
+
 		VERIFY(err, 0 == fastrpc_create_current_dsp_process());
 		if (err)
+			goto bail;
+		filp->private_data = fdata;
+bail:
+		if (err) {
 			cleanup_current_dev();
+			kfree(fdata);
+			mutex_lock(&me->smd_mutex);
+			kref_put(&me->kref, fastrpc_channel_close);
+			mutex_unlock(&me->smd_mutex);
+		}
 		module_put(THIS_MODULE);
 	}
 	return err;
+
+completion_bail:
+	smd_close(me->chan);
+	me->chan = 0;
+smd_bail:
+	mutex_unlock(&me->smd_mutex);
+	return err;
 }
 
 
@@ -818,88 +1223,113 @@ static long fastrpc_device_ioctl(struct file *file, unsigned int ioctl_num,
 				 unsigned long ioctl_param)
 {
 	struct fastrpc_apps *me = &gfa;
-	struct fastrpc_ioctl_invoke invoke;
-	remote_arg_t *pra = 0;
+	struct fastrpc_ioctl_invoke_fd invokefd;
+	struct fastrpc_ioctl_mmap mmap;
+	struct fastrpc_ioctl_munmap munmap;
 	void *param = (char *)ioctl_param;
-	int bufs, err = 0;
+	struct file_data *fdata = (struct file_data *)file->private_data;
+	int size = 0, err = 0;
 
 	switch (ioctl_num) {
+	case FASTRPC_IOCTL_INVOKE_FD:
 	case FASTRPC_IOCTL_INVOKE:
-		VERIFY(err, 0 == copy_from_user(&invoke, param,
-						sizeof(invoke)));
+		invokefd.fds = 0;
+		size = (ioctl_num == FASTRPC_IOCTL_INVOKE) ?
+				sizeof(invokefd.inv) : sizeof(invokefd);
+		VERIFY(err, 0 == copy_from_user(&invokefd, param, size));
 		if (err)
 			goto bail;
-		bufs = REMOTE_SCALARS_INBUFS(invoke.sc) +
-			REMOTE_SCALARS_OUTBUFS(invoke.sc);
-		if (bufs) {
-			bufs = bufs * sizeof(*pra);
-			VERIFY(err, 0 != (pra = kmalloc(bufs, GFP_KERNEL)));
-			if (err)
-				goto bail;
-		}
-		VERIFY(err, 0 == copy_from_user(pra, invoke.pra, bufs));
+		VERIFY(err, 0 == (err = fastrpc_internal_invoke(me, fdata->mode,
+						0, &invokefd)));
 		if (err)
 			goto bail;
-		VERIFY(err, 0 == (err = fastrpc_internal_invoke(me, 0, &invoke,
-								pra)));
+		break;
+	case FASTRPC_IOCTL_MMAP:
+		VERIFY(err, 0 == copy_from_user(&mmap, param,
+						sizeof(mmap)));
+		if (err)
+			goto bail;
+		VERIFY(err, 0 == (err = fastrpc_internal_mmap(me, fdata,
+							      &mmap)));
+		if (err)
+			goto bail;
+		VERIFY(err, 0 == copy_to_user(param, &mmap, sizeof(mmap)));
 		if (err)
 			goto bail;
 		break;
+	case FASTRPC_IOCTL_MUNMAP:
+		VERIFY(err, 0 == copy_from_user(&munmap, param,
+						sizeof(munmap)));
+		if (err)
+			goto bail;
+		VERIFY(err, 0 == (err = fastrpc_internal_munmap(me, fdata,
+								&munmap)));
+		if (err)
+			goto bail;
+		break;
+	case FASTRPC_IOCTL_SETMODE:
+		switch ((uint32_t)ioctl_param) {
+		case FASTRPC_MODE_PARALLEL:
+		case FASTRPC_MODE_SERIAL:
+			fdata->mode = (uint32_t)ioctl_param;
+			break;
+		default:
+			err = -ENOTTY;
+			break;
+		}
+		break;
 	default:
 		err = -ENOTTY;
 		break;
 	}
  bail:
-	kfree(pra);
 	return err;
 }
 
-#ifdef __KERNEL__
-
 static const struct file_operations fops = {
 	.open = fastrpc_device_open,
 	.release = fastrpc_device_release,
 	.unlocked_ioctl = fastrpc_device_ioctl,
 };
 
-#endif /*__KERNEL__*/
-
 static int __init fastrpc_device_init(void)
 {
 	struct fastrpc_apps *me = &gfa;
 	int err = 0;
 
+	memset(me, 0, sizeof(*me));
 	VERIFY(err, 0 == fastrpc_init());
 	if (err)
-		goto bail;
+		goto fastrpc_bail;
 	VERIFY(err, 0 == alloc_chrdev_region(&me->dev_no, 0, 1, DEVICE_NAME));
 	if (err)
-		goto bail;
+		goto alloc_chrdev_bail;
 	cdev_init(&me->cdev, &fops);
 	me->cdev.owner = THIS_MODULE;
 	VERIFY(err, 0 == cdev_add(&me->cdev, MKDEV(MAJOR(me->dev_no), 0), 1));
 	if (err)
-		goto bail;
-	me->class = class_create(THIS_MODULE, "chardrv");
+		goto cdev_init_bail;
+	me->class = class_create(THIS_MODULE, "fastrpc");
 	VERIFY(err, !IS_ERR(me->class));
 	if (err)
-		goto bail;
+		goto class_create_bail;
 	me->dev = device_create(me->class, NULL, MKDEV(MAJOR(me->dev_no), 0),
 				NULL, DEVICE_NAME);
 	VERIFY(err, !IS_ERR(me->dev));
 	if (err)
-		goto bail;
-	pr_info("'created /dev/%s c %d 0'\n", DEVICE_NAME, MAJOR(me->dev_no));
- bail:
-	if (err) {
-		if (me->dev_no)
-			unregister_chrdev_region(me->dev_no, 1);
-		if (me->class)
-			class_destroy(me->class);
-		if (me->cdev.owner)
-			cdev_del(&me->cdev);
-		fastrpc_deinit();
-	}
+		goto device_create_bail;
+
+	return 0;
+
+device_create_bail:
+	class_destroy(me->class);
+class_create_bail:
+	cdev_del(&me->cdev);
+cdev_init_bail:
+	unregister_chrdev_region(me->dev_no, 1);
+alloc_chrdev_bail:
+	fastrpc_deinit();
+fastrpc_bail:
 	return err;
 }
 
@@ -907,7 +1337,9 @@ static void __exit fastrpc_device_exit(void)
 {
 	struct fastrpc_apps *me = &gfa;
 
+	context_list_dtor(me, &me->clst);
 	fastrpc_deinit();
+	cleanup_current_dev();
 	device_destroy(me->class, MKDEV(MAJOR(me->dev_no), 0));
 	class_destroy(me->class);
 	cdev_del(&me->cdev);
diff --git a/drivers/char/adsprpc_shared.h b/drivers/char/adsprpc_shared.h
old mode 100644
new mode 100755
index 0354563..374bfeb
--- a/drivers/char/adsprpc_shared.h
+++ b/drivers/char/adsprpc_shared.h
@@ -14,16 +14,24 @@
 #ifndef ADSPRPC_SHARED_H
 #define ADSPRPC_SHARED_H
 
-#ifdef __KERNEL__
 #include <linux/types.h>
-#endif /*__KERNEL__*/
 
 
 
 #define FASTRPC_IOCTL_INVOKE _IOWR('R', 1, struct fastrpc_ioctl_invoke)
+#define FASTRPC_IOCTL_MMAP    _IOWR('R', 2, struct fastrpc_ioctl_mmap)
+#define FASTRPC_IOCTL_MUNMAP  _IOWR('R', 3, struct fastrpc_ioctl_munmap)
+#define FASTRPC_IOCTL_INVOKE_FD  _IOWR('R', 4, struct fastrpc_ioctl_invoke_fd)
+#define FASTRPC_IOCTL_SETMODE    _IOWR('R', 5, uint32_t)
 #define FASTRPC_SMD_GUID "fastrpcsmd-apps-dsp"
 #define DEVICE_NAME      "adsprpc-smd"
 
+/* Driver should operate in parallel with the co-processor */
+#define FASTRPC_MODE_PARALLEL    0
+
+/* Driver should operate in serial mode with the co-processor */
+#define FASTRPC_MODE_SERIAL      1
+
 /* Retrives number of input buffers from the scalars parameter */
 #define REMOTE_SCALARS_INBUFS(sc)        (((sc) >> 16) & 0x0ff)
 
@@ -96,6 +104,25 @@ struct fastrpc_ioctl_invoke {
 	remote_arg_t *pra;	/* remote arguments list */
 };
 
+struct fastrpc_ioctl_invoke_fd {
+	struct fastrpc_ioctl_invoke inv;
+	int *fds;		/* fd list */
+};
+
+struct fastrpc_ioctl_munmap {
+	uint32_t vaddrout;	/* address to unmap */
+	int  size;		/* size */
+};
+
+
+struct fastrpc_ioctl_mmap {
+	int fd;			/* ion fd */
+	uint32_t flags;		/* flags for dsp to map with */
+	uint32_t vaddrin;	/* optional virtual address */
+	int  size;		/* size */
+	uint32_t vaddrout;	/* dsps virtual address */
+};
+
 struct smq_null_invoke {
 	struct smq_invoke_ctx *ctx; /* invoke caller context */
 	uint32_t handle;	    /* handle to invoke */
-- 
1.8.2.1

