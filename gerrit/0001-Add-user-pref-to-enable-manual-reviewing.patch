From 6a5eab4e8a01cacf8eae436221b3e90ed8d888ae Mon Sep 17 00:00:00 2001
From: Martin Fick <mfick@codeaurora.org>
Date: Wed, 23 Nov 2011 10:30:29 -0700
Subject: [PATCH] Add user pref to enable manual reviewing

Add a checkbox to the preferences header on the diff
screen which allows a user to specify whether they
want manual-reviewing enabled or disabled.  Previously,
every file was auto marked reviewed when a user first
displayed it.  The new manual mode prevents this auto
marking and only marks a file reviewed when the user
explicitly clicks on the reviewed button.

Using the manual mode allows users to better decide
when they feel they have reviewed a file.  This mode
is particularly useful for larger reviews when a
user may need to jump around and look at many other
files while commenting on a specific one.  This
prevents their jumping around from file to file from
marking the files as reviewed.  The intention is to
allow the reviewed status to provide more meaning
than simply "I have seen the file" (which in many
cases might even be a stretch).

Change-Id: Iaedbcd717ef8a587a78c56e3d1b5f3fcc86a6d21
---
 .../google/gerrit/client/patches/PatchScreen.java  |   26 ++++++++++--
 .../client/patches/PatchScriptSettingsPanel.java   |    5 ++
 .../client/patches/PatchScriptSettingsPanel.ui.xml |   13 +++++-
 .../client/ui/ListenableAccountDiffPreference.java |    2 +-
 .../gerrit/client/ui/ListenableOldValue.java       |   44 ++++++++++++++++++++
 .../gerrit/reviewdb/AccountDiffPreference.java     |   13 ++++++
 .../google/gerrit/server/schema/SchemaVersion.java |    2 +-
 .../com/google/gerrit/server/schema/Schema_62.java |   25 +++++++++++
 8 files changed, 122 insertions(+), 8 deletions(-)
 create mode 100644 gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ListenableOldValue.java
 create mode 100644 gerrit-server/src/main/java/com/google/gerrit/server/schema/Schema_62.java

diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScreen.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScreen.java
index 1671d6e..582aca4 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScreen.java
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScreen.java
@@ -24,6 +24,7 @@ import com.google.gerrit.client.changes.Util;
 import com.google.gerrit.client.rpc.GerritCallback;
 import com.google.gerrit.client.rpc.ScreenLoadCallback;
 import com.google.gerrit.client.ui.ListenableAccountDiffPreference;
+import com.google.gerrit.client.ui.ListenableOldValue;
 import com.google.gerrit.client.ui.Screen;
 import com.google.gerrit.common.data.PatchScript;
 import com.google.gerrit.common.data.PatchSetDetail;
@@ -189,6 +190,13 @@ public abstract class PatchScreen extends Screen implements
   }
 
   private void update(AccountDiffPreference dp) {
+    // Did the user just turn on auto-review?
+    if (!reviewed.getValue() && prefs.getOld().isManualReview()
+        && !dp.isManualReview()) {
+      reviewed.setValue(true);
+      setReviewedByCurrentUser(true);
+    }
+
     if (lastScript != null && canReuse(dp, lastScript)) {
       lastScript.setDiffPrefs(dp);
       RpcStatus.INSTANCE.onRpcStart(null);
@@ -450,10 +458,20 @@ public abstract class PatchScreen extends Screen implements
       bottomNav.display(patchIndex, getPatchScreenType(), fileList);
     }
 
-    // Mark this file reviewed as soon we display the diff screen
-    if (Gerrit.isSignedIn() && isFirst) {
-      reviewed.setValue(true);
-      setReviewedByCurrentUser(true /* reviewed */);
+    if (Gerrit.isSignedIn()) {
+      boolean revd = false;
+      if (isFirst && !prefs.get().isManualReview()) {
+        revd = true;
+        setReviewedByCurrentUser(revd);
+      } else {
+        for (Patch p : patchSetDetail.getPatches()) {
+          if (p.getKey().equals(patchKey)) {
+            revd = p.isReviewedByCurrentUser();
+            break;
+          }
+        }
+      }
+      reviewed.setValue(revd);
     }
 
     intralineFailure = isFirst && script.hasIntralineFailure();
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScriptSettingsPanel.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScriptSettingsPanel.java
index df0fff5..941994a 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScriptSettingsPanel.java
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScriptSettingsPanel.java
@@ -79,6 +79,9 @@ public class PatchScriptSettingsPanel extends Composite implements
   CheckBox showTabs;
 
   @UiField
+  CheckBox manualReview;
+
+  @UiField
   CheckBox skipDeleted;
 
   @UiField
@@ -212,6 +215,7 @@ public class PatchScriptSettingsPanel extends Composite implements
     skipUncommented.setValue(dp.isSkipUncommented());
     expandAllComments.setValue(dp.isExpandAllComments());
     retainHeader.setValue(dp.isRetainHeader());
+    manualReview.setValue(dp.isManualReview());
   }
 
   @UiHandler("update")
@@ -243,6 +247,7 @@ public class PatchScriptSettingsPanel extends Composite implements
     dp.setSkipUncommented(skipUncommented.getValue());
     dp.setExpandAllComments(expandAllComments.getValue());
     dp.setRetainHeader(retainHeader.getValue());
+    dp.setManualReview(manualReview.getValue());
 
     listenablePrefs.set(dp);
   }
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScriptSettingsPanel.ui.xml b/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScriptSettingsPanel.ui.xml
index e819ffa..0a31a11 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScriptSettingsPanel.ui.xml
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScriptSettingsPanel.ui.xml
@@ -147,20 +147,29 @@ limitations under the License.
       </g:CheckBox>
     </td>
 
+    <td valign='bottom' rowspan='2'>
+      <g:CheckBox
+          ui:field='manualReview'
+          text='Manual-Review'
+          tabIndex='13'>
+        <ui:attribute name='text'/>
+      </g:CheckBox>
+    </td>
+
     <td rowspan='2'>
       <br/>
       <g:Button
           ui:field='update'
           text='Update'
           styleName='{style.updateButton}'
-          tabIndex='13'>
+          tabIndex='14'>
         <ui:attribute name='text'/>
       </g:Button>
       <g:Button
           ui:field='save'
           text='Save'
           styleName='{style.updateButton}'
-          tabIndex='14'>
+          tabIndex='15'>
         <ui:attribute name='text'/>
       </g:Button>
     </td>
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ListenableAccountDiffPreference.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ListenableAccountDiffPreference.java
index 41de2cd..da4c0d8 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ListenableAccountDiffPreference.java
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ListenableAccountDiffPreference.java
@@ -21,7 +21,7 @@ import com.google.gerrit.reviewdb.AccountDiffPreference;
 import com.google.gwtjsonrpc.client.VoidResult;
 
 public class ListenableAccountDiffPreference
-    extends ListenableValue<AccountDiffPreference> {
+    extends ListenableOldValue<AccountDiffPreference> {
 
   public ListenableAccountDiffPreference() {
     reset();
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ListenableOldValue.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ListenableOldValue.java
new file mode 100644
index 0000000..c957433
--- /dev/null
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ListenableOldValue.java
@@ -0,0 +1,44 @@
+// Copyright (c) 2012, Code Aurora Forum. All rights reserved.
+//
+// Redistribution and use in source and binary forms, with or without
+// modification, are permitted provided that the following conditions are
+// met:
+//     * Redistributions of source code must retain the above copyright
+//       notice, this list of conditions and the following disclaimer.
+//     * Redistributions in binary form must reproduce the above
+//       copyright notice, this list of conditions and the following
+//       disclaimer in the documentation and/or other materials provided
+//       with the distribution.
+//     * Neither the name of Code Aurora Forum, Inc. nor the names of its
+//       contributors may be used to endorse or promote products derived
+//       from this software without specific prior written permission.
+//
+// THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
+// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
+// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
+// ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
+// BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
+// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
+// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
+// BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
+// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
+// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
+// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.// Copyright (C) 2012 The Android Open Source Project
+
+package com.google.gerrit.client.ui;
+
+
+public class ListenableOldValue<T> extends ListenableValue<T> {
+
+  private T oldValue;
+
+  public T getOld() {
+    return oldValue;
+  }
+
+  public void set(final T value) {
+    oldValue = get();
+    super.set(value);
+    oldValue = null; // allow it to be gced before the next event
+  }
+}
diff --git a/gerrit-reviewdb/src/main/java/com/google/gerrit/reviewdb/AccountDiffPreference.java b/gerrit-reviewdb/src/main/java/com/google/gerrit/reviewdb/AccountDiffPreference.java
index c362f26..ccc379e 100644
--- a/gerrit-reviewdb/src/main/java/com/google/gerrit/reviewdb/AccountDiffPreference.java
+++ b/gerrit-reviewdb/src/main/java/com/google/gerrit/reviewdb/AccountDiffPreference.java
@@ -65,6 +65,7 @@ public class AccountDiffPreference {
     p.setIntralineDifference(true);
     p.setShowTabs(true);
     p.setContext(DEFAULT_CONTEXT);
+    p.setManualReview(false);
     return p;
   }
 
@@ -108,6 +109,9 @@ public class AccountDiffPreference {
   @Column(id = 13)
   protected boolean retainHeader;
 
+  @Column(id = 14)
+  protected boolean manualReview;
+
   protected AccountDiffPreference() {
   }
 
@@ -129,6 +133,7 @@ public class AccountDiffPreference {
     this.expandAllComments = p.expandAllComments;
     this.context = p.context;
     this.retainHeader = p.retainHeader;
+    this.manualReview = p.manualReview;
   }
 
   public Account.Id getAccountId() {
@@ -233,4 +238,12 @@ public class AccountDiffPreference {
   public void setRetainHeader(boolean retain) {
     retainHeader = retain;
   }
+
+  public boolean isManualReview() {
+    return manualReview;
+  }
+
+  public void setManualReview(boolean manual) {
+    manualReview = manual;
+  }
 }
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/schema/SchemaVersion.java b/gerrit-server/src/main/java/com/google/gerrit/server/schema/SchemaVersion.java
index d7ae35c..962cf94 100644
--- a/gerrit-server/src/main/java/com/google/gerrit/server/schema/SchemaVersion.java
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/schema/SchemaVersion.java
@@ -32,7 +32,7 @@ import java.util.List;
 /** A version of the database schema. */
 public abstract class SchemaVersion {
   /** The current schema version. */
-  private static final Class<? extends SchemaVersion> C = Schema_61.class;
+  private static final Class<? extends SchemaVersion> C = Schema_62.class;
 
   public static class Module extends AbstractModule {
     @Override
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/schema/Schema_62.java b/gerrit-server/src/main/java/com/google/gerrit/server/schema/Schema_62.java
new file mode 100644
index 0000000..4de8e55
--- /dev/null
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/schema/Schema_62.java
@@ -0,0 +1,25 @@
+// Copyright (C) 2011 The Android Open Source Project
+//
+// Licensed under the Apache License, Version 2.0 (the "License");
+// you may not use this file except in compliance with the License.
+// You may obtain a copy of the License at
+//
+// http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software
+// distributed under the License is distributed on an "AS IS" BASIS,
+// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+// See the License for the specific language governing permissions and
+// limitations under the License.
+
+package com.google.gerrit.server.schema;
+
+import com.google.inject.Inject;
+import com.google.inject.Provider;
+
+public class Schema_62 extends SchemaVersion {
+  @Inject
+  Schema_62(Provider<Schema_61> prior) {
+    super(prior);
+  }
+}
-- 
1.7.8

