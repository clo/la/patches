From 9ae2867b975f9170ea5ac88f3964d9e4f5b920c8 Mon Sep 17 00:00:00 2001
From: Martin Fick <mfick@codeaurora.org>
Date: Thu, 26 Aug 2010 16:47:41 -0600
Subject: [PATCH] Refactor widget creation in Watch UI into a separate function

Refactor out the widget creation in MyWatchedProjectsScreen
into a separate function.  This makes a clean separation
between business logic and layout logic.  Also renamed the
table widget to reduce naming conflicts between widgets and
data structures.

Change-Id: I7f97aa1e0db550653e270a6ec1d78b9399b48b1e
---
 .../client/account/MyWatchedProjectsScreen.java    |  142 ++++++++++----------
 1 files changed, 72 insertions(+), 70 deletions(-)

diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/MyWatchedProjectsScreen.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/MyWatchedProjectsScreen.java
index 18b192e..961a33f 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/MyWatchedProjectsScreen.java
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/MyWatchedProjectsScreen.java
@@ -48,12 +48,11 @@ import java.util.List;
 import java.util.Set;
 
 public class MyWatchedProjectsScreen extends SettingsScreen {
-  private WatchTable watches;
-
   private Button addNew;
   private HintTextBox nameBox;
   private SuggestBox nameTxt;
   private HintTextBox filterTxt;
+  private WatchTable watchesTab;
   private Button delSel;
   private boolean submitOnSelection;
 
@@ -61,88 +60,91 @@ public class MyWatchedProjectsScreen extends SettingsScreen {
   protected void onInitUI() {
     super.onInitUI();
 
-    {
-      nameBox = new HintTextBox();
-      nameTxt = new SuggestBox(new ProjectNameSuggestOracle(), nameBox);
-      nameBox.setVisibleLength(50);
-      nameBox.setHintText(Util.C.defaultProjectName());
-      nameBox.addKeyPressHandler(new KeyPressHandler() {
-        @Override
-        public void onKeyPress(KeyPressEvent event) {
-          submitOnSelection = false;
+    createWidgets();
 
-          if (event.getCharCode() == KeyCodes.KEY_ENTER) {
-            if (nameTxt.isSuggestionListShowing()) {
-              submitOnSelection = true;
-            } else {
-              doAddNew();
-            }
-          }
-        }
-      });
-      nameTxt.addSelectionHandler(new SelectionHandler<Suggestion>() {
-        @Override
-        public void onSelection(SelectionEvent<Suggestion> event) {
-          if (submitOnSelection) {
-            submitOnSelection = false;
-            doAddNew();
-          }
-        }
-      });
+    final Grid grid = new Grid(2, 2);
+    grid.setStyleName(Gerrit.RESOURCES.css().infoBlock());
+    grid.setText(0, 0, Util.C.watchedProjectName());
+    grid.setWidget(0, 1, nameTxt);
 
-      filterTxt = new HintTextBox();
-      filterTxt.setVisibleLength(50);
-      filterTxt.setHintText(Util.C.defaultFilter());
-      filterTxt.addKeyPressHandler(new KeyPressHandler() {
-        @Override
-        public void onKeyPress(KeyPressEvent event) {
-          if (event.getCharCode() == KeyCodes.KEY_ENTER) {
+    grid.setText(1, 0, Util.C.watchedProjectFilter());
+    grid.setWidget(1, 1, filterTxt);
+
+    final CellFormatter fmt = grid.getCellFormatter();
+    fmt.addStyleName(0, 0, Gerrit.RESOURCES.css().topmost());
+    fmt.addStyleName(0, 1, Gerrit.RESOURCES.css().topmost());
+    fmt.addStyleName(0, 0, Gerrit.RESOURCES.css().header());
+    fmt.addStyleName(1, 0, Gerrit.RESOURCES.css().header());
+    fmt.addStyleName(1, 0, Gerrit.RESOURCES.css().bottomheader());
+
+    final FlowPanel fp = new FlowPanel();
+    fp.setStyleName(Gerrit.RESOURCES.css().addWatchPanel());
+    fp.add(grid);
+    fp.add(addNew);
+    add(fp);
+
+    add(watchesTab);
+    add(delSel);
+  }
+
+  protected void createWidgets() {
+    nameBox = new HintTextBox();
+    nameTxt = new SuggestBox(new ProjectNameSuggestOracle(), nameBox);
+    nameBox.setVisibleLength(50);
+    nameBox.setHintText(Util.C.defaultProjectName());
+    nameBox.addKeyPressHandler(new KeyPressHandler() {
+      @Override
+      public void onKeyPress(KeyPressEvent event) {
+        submitOnSelection = false;
+
+        if (event.getCharCode() == KeyCodes.KEY_ENTER) {
+          if (nameTxt.isSuggestionListShowing()) {
+            submitOnSelection = true;
+          } else {
             doAddNew();
           }
         }
-      });
-
-      addNew = new Button(Util.C.buttonWatchProject());
-      addNew.addClickHandler(new ClickHandler() {
-        @Override
-        public void onClick(final ClickEvent event) {
+      }
+    });
+    nameTxt.addSelectionHandler(new SelectionHandler<Suggestion>() {
+      @Override
+      public void onSelection(SelectionEvent<Suggestion> event) {
+        if (submitOnSelection) {
+          submitOnSelection = false;
           doAddNew();
         }
-      });
-
-      final Grid grid = new Grid(2, 2);
-      grid.setStyleName(Gerrit.RESOURCES.css().infoBlock());
-      grid.setText(0, 0, Util.C.watchedProjectName());
-      grid.setWidget(0, 1, nameTxt);
-
-      grid.setText(1, 0, Util.C.watchedProjectFilter());
-      grid.setWidget(1, 1, filterTxt);
+      }
+    });
 
-      final CellFormatter fmt = grid.getCellFormatter();
-      fmt.addStyleName(0, 0, Gerrit.RESOURCES.css().topmost());
-      fmt.addStyleName(0, 1, Gerrit.RESOURCES.css().topmost());
-      fmt.addStyleName(0, 0, Gerrit.RESOURCES.css().header());
-      fmt.addStyleName(1, 0, Gerrit.RESOURCES.css().header());
-      fmt.addStyleName(1, 0, Gerrit.RESOURCES.css().bottomheader());
+    filterTxt = new HintTextBox();
+    filterTxt.setVisibleLength(50);
+    filterTxt.setHintText(Util.C.defaultFilter());
+    filterTxt.addKeyPressHandler(new KeyPressHandler() {
+      @Override
+      public void onKeyPress(KeyPressEvent event) {
+        if (event.getCharCode() == KeyCodes.KEY_ENTER) {
+          doAddNew();
+        }
+      }
+    });
 
-      final FlowPanel fp = new FlowPanel();
-      fp.setStyleName(Gerrit.RESOURCES.css().addWatchPanel());
-      fp.add(grid);
-      fp.add(addNew);
-      add(fp);
-    }
+    addNew = new Button(Util.C.buttonWatchProject());
+    addNew.addClickHandler(new ClickHandler() {
+      @Override
+      public void onClick(final ClickEvent event) {
+        doAddNew();
+      }
+    });
 
-    watches = new WatchTable();
-    add(watches);
+    watchesTab = new WatchTable();
 
     delSel = new Button(Util.C.buttonDeleteSshKey());
     delSel.addClickHandler(new ClickHandler() {
       @Override
       public void onClick(final ClickEvent event) {
-        watches.deleteChecked();
+        watchesTab.deleteChecked();
       }
     });
-    add(delSel);
   }
 
   @Override
@@ -175,7 +177,7 @@ public class MyWatchedProjectsScreen extends SettingsScreen {
             filterTxt.setEnabled(true);
 
             nameTxt.setText("");
-            watches.insertWatch(result);
+            watchesTab.insertWatch(result);
           }
 
           @Override
@@ -194,7 +196,7 @@ public class MyWatchedProjectsScreen extends SettingsScreen {
         new ScreenLoadCallback<List<AccountProjectWatchInfo>>(this) {
       @Override
       public void preDisplay(final List<AccountProjectWatchInfo> result) {
-        watches.display(result);
+        watchesTab.display(result);
       }
     });
   }
-- 
1.7.2.2

