From 5c8323f0c866ba248804e809b1626a7e3ce75225 Mon Sep 17 00:00:00 2001
From: Raviteja Sunkara <raviteja@codeaurora.org>
Date: Thu, 9 May 2013 02:42:51 +0530
Subject: [PATCH 1/2] optimize label query operator for open changes.

Create rewrite rule for "status:open label:*", which
reduces the data set to the changes that have the
label values asked in the query.

Change-Id: I1e1e1b6e99239d28562390cbb12e6bc5ba14fe56
---
 .../reviewdb/server/PatchSetApprovalAccess.java    |   12 +++++
 .../gerrit/reviewdb/server/index_generic.sql       |    6 ++
 .../gerrit/reviewdb/server/index_postgres.sql      |    6 ++
 .../server/query/change/ChangeQueryRewriter.java   |   49 ++++++++++++++++++++
 .../gerrit/server/query/change/LabelPredicate.java |   39 ++++++++++++++--
 .../google/gerrit/server/schema/SchemaVersion.java |    2 +-
 .../com/google/gerrit/server/schema/Schema_80.java |   45 ++++++++++++++++++
 7 files changed, 153 insertions(+), 6 deletions(-)
 create mode 100644 gerrit-server/src/main/java/com/google/gerrit/server/schema/Schema_80.java

diff --git a/gerrit-reviewdb/src/main/java/com/google/gerrit/reviewdb/server/PatchSetApprovalAccess.java b/gerrit-reviewdb/src/main/java/com/google/gerrit/reviewdb/server/PatchSetApprovalAccess.java
index dae8e6d..567e394 100644
--- a/gerrit-reviewdb/src/main/java/com/google/gerrit/reviewdb/server/PatchSetApprovalAccess.java
+++ b/gerrit-reviewdb/src/main/java/com/google/gerrit/reviewdb/server/PatchSetApprovalAccess.java
@@ -51,4 +51,16 @@ public interface PatchSetApprovalAccess extends
   @Query("WHERE changeOpen = false AND key.accountId = ? ORDER BY changeSortKey")
   ResultSet<PatchSetApproval> closedByUserAll(Account.Id account)
       throws OrmException;
+
+  @Query("WHERE changeOpen = true AND key.categoryId = ? AND value >= ?")
+  ResultSet<PatchSetApproval> byOpenLabelValueGE(PatchSetApproval.LabelId id,
+      short value) throws OrmException;
+
+  @Query("WHERE changeOpen = true AND key.categoryId = ? AND value <= ?")
+  ResultSet<PatchSetApproval> byOpenLabelValueLE(PatchSetApproval.LabelId id,
+      short value) throws OrmException;
+
+  @Query("WHERE changeOpen = true AND key.categoryId = ? AND value = ?")
+  ResultSet<PatchSetApproval> byOpenLabelValueEQ(PatchSetApproval.LabelId id,
+      short value) throws OrmException;
 }
diff --git a/gerrit-reviewdb/src/main/resources/com/google/gerrit/reviewdb/server/index_generic.sql b/gerrit-reviewdb/src/main/resources/com/google/gerrit/reviewdb/server/index_generic.sql
index d6609e7..87efcf2 100644
--- a/gerrit-reviewdb/src/main/resources/com/google/gerrit/reviewdb/server/index_generic.sql
+++ b/gerrit-reviewdb/src/main/resources/com/google/gerrit/reviewdb/server/index_generic.sql
@@ -117,6 +117,12 @@ ON patch_set_approvals (change_open, account_id);
 CREATE INDEX patch_set_approvals_closedByUser
 ON patch_set_approvals (change_open, account_id, change_sort_key);
 
+--    covers:             byOpenLabelValueGE, byOpenLabelValueLE,
+--                        byOpenLabelValueEQ
+CREATE INDEX patch_set_approvals_byOpenLabel
+ON patch_set_approvals (category_id, value)
+WHERE change_open = 'Y';
+
 
 -- *********************************************************************
 -- ChangeMessageAccess
diff --git a/gerrit-reviewdb/src/main/resources/com/google/gerrit/reviewdb/server/index_postgres.sql b/gerrit-reviewdb/src/main/resources/com/google/gerrit/reviewdb/server/index_postgres.sql
index 3b62e84..3b74ffe 100644
--- a/gerrit-reviewdb/src/main/resources/com/google/gerrit/reviewdb/server/index_postgres.sql
+++ b/gerrit-reviewdb/src/main/resources/com/google/gerrit/reviewdb/server/index_postgres.sql
@@ -174,6 +174,12 @@ CREATE INDEX patch_set_approvals_closedByUser
 ON patch_set_approvals (account_id, change_sort_key)
 WHERE change_open = 'N';
 
+--    covers:             byOpenLabelValueGE, byOpenLabelValueLE,
+--                        byOpenLabelValueEQ
+CREATE INDEX patch_set_approvals_byOpenLabel
+ON patch_set_approvals (category_id, value)
+WHERE change_open = 'Y';
+
 
 -- *********************************************************************
 -- ChangeMessageAccess
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/query/change/ChangeQueryRewriter.java b/gerrit-server/src/main/java/com/google/gerrit/server/query/change/ChangeQueryRewriter.java
index e6251bc..7793e17 100644
--- a/gerrit-server/src/main/java/com/google/gerrit/server/query/change/ChangeQueryRewriter.java
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/query/change/ChangeQueryRewriter.java
@@ -16,6 +16,7 @@ package com.google.gerrit.server.query.change;
 
 import com.google.gerrit.reviewdb.client.Branch;
 import com.google.gerrit.reviewdb.client.Change;
+import com.google.gerrit.reviewdb.client.PatchSetApproval;
 import com.google.gerrit.reviewdb.server.ChangeAccess;
 import com.google.gerrit.reviewdb.server.ReviewDb;
 import com.google.gerrit.server.ChangeUtil;
@@ -119,6 +120,54 @@ public class ChangeQueryRewriter extends QueryRewriter<ChangeData> {
     return a.getValue().compareTo(b.getValue()) >= 0 ? a : b;
   }
 
+  @Rewrite("status:open L=(label:*)")
+  public Predicate<ChangeData> r03_byLabel(
+      @Named("L") final LabelPredicate l) {
+    return new Source() {
+      {
+        init("r03_byLabel", l);
+      }
+
+      @Override
+      public ResultSet<ChangeData> read() throws OrmException {
+        ResultSet<PatchSetApproval> patchSetApprovals;
+        LabelPredicate.LabelOperator comp = l.getLabelOperator();
+        short value = l.getApprovalValue();
+        PatchSetApproval.LabelId label =
+            new PatchSetApproval.LabelId(l.getType());
+        switch (comp) {
+          case GT_EQ:
+            patchSetApprovals = dbProvider.get().patchSetApprovals().
+                byOpenLabelValueGE(label, value);
+            break;
+          case LT_EQ:
+            patchSetApprovals = dbProvider.get().patchSetApprovals().
+                byOpenLabelValueLE(label, value);
+            break;
+          default:
+            patchSetApprovals = dbProvider.get().patchSetApprovals().
+                byOpenLabelValueEQ(label, value);
+        }
+        return ChangeDataResultSet.patchSetApproval(patchSetApprovals);
+      }
+
+      @Override
+      public boolean match(ChangeData cd) throws OrmException {
+        return cd.change(dbProvider) != null && l.match(cd);
+      }
+
+      @Override
+      public int getCardinality() {
+        return 1000;
+      }
+
+      @Override
+      public int getCost() {
+        return ChangeCosts.cost(ChangeCosts.APPROVALS_SCAN, getCardinality());
+      }
+    };
+  }
+
   @Rewrite("status:open P=(project:*) B=(branch:*)")
   public Predicate<ChangeData> r05_byBranchOpen(
       @Named("P") final ProjectPredicate p,
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/query/change/LabelPredicate.java b/gerrit-server/src/main/java/com/google/gerrit/server/query/change/LabelPredicate.java
index 936a47d..94bc1d5 100644
--- a/gerrit-server/src/main/java/com/google/gerrit/server/query/change/LabelPredicate.java
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/query/change/LabelPredicate.java
@@ -59,7 +59,13 @@ class LabelPredicate extends OperatorPredicate<ChangeData> {
     abstract boolean match(int psValue, int expValue);
   }
 
-  private static LabelType type(LabelTypes types, String toFind) {
+  public static enum LabelOperator {
+    EQ,
+    GT_EQ,
+    LT_EQ
+  }
+
+  public static LabelType type(LabelTypes types, String toFind) {
     if (types.byLabel(toFind) != null) {
       return types.byLabel(toFind);
     }
@@ -94,11 +100,11 @@ class LabelPredicate extends OperatorPredicate<ChangeData> {
     }
   }
 
-  private static int value(String value) {
+  private static short value(String value) {
     if (value.startsWith("+")) {
       value = value.substring(1);
     }
-    return Integer.parseInt(value);
+    return Short.valueOf(value);
   }
 
   private final ProjectCache projectCache;
@@ -107,7 +113,8 @@ class LabelPredicate extends OperatorPredicate<ChangeData> {
   private final Provider<ReviewDb> dbProvider;
   private final Test test;
   private final String type;
-  private final int expVal;
+  private final short expVal;
+  private final LabelOperator comp;
 
   LabelPredicate(ProjectCache projectCache,
       ChangeControl.GenericFactory ccFactory,
@@ -124,21 +131,43 @@ class LabelPredicate extends OperatorPredicate<ChangeData> {
     Matcher m2 = Pattern.compile("([+-]\\d+)$").matcher(value);
     if (m1.find()) {
       type = value.substring(0, m1.start());
-      test = op(m1.group(1));
+      String opStr = m1.group(1);
+      test = op(opStr);
       expVal = value(m1.group(2));
+      if ("=".equals(opStr)) {
+        comp = LabelOperator.EQ;
+      } else if (">=".equals(opStr)) {
+        comp = LabelOperator.GT_EQ;
+      } else {
+        comp = LabelOperator.LT_EQ;
+      }
 
     } else if (m2.find()) {
       type = value.substring(0, m2.start());
+      comp = LabelOperator.EQ;
       test = Test.EQ;
       expVal = value(m2.group(1));
 
     } else {
       type = value;
+      comp = LabelOperator.EQ;
       test = Test.EQ;
       expVal = 1;
     }
   }
 
+  public String getType() {
+    return type;
+  }
+
+  public LabelOperator getLabelOperator() {
+    return comp;
+  }
+
+  public short getApprovalValue() {
+    return expVal;
+  }
+
   @Override
   public boolean match(final ChangeData object) throws OrmException {
     final Change c = object.change(dbProvider);
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/schema/SchemaVersion.java b/gerrit-server/src/main/java/com/google/gerrit/server/schema/SchemaVersion.java
index ac59f65..9a89064 100644
--- a/gerrit-server/src/main/java/com/google/gerrit/server/schema/SchemaVersion.java
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/schema/SchemaVersion.java
@@ -32,7 +32,7 @@ import java.util.List;
 /** A version of the database schema. */
 public abstract class SchemaVersion {
   /** The current schema version. */
-  public static final Class<Schema_79> C = Schema_79.class;
+  public static final Class<Schema_80> C = Schema_80.class;
 
   public static class Module extends AbstractModule {
     @Override
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/schema/Schema_80.java b/gerrit-server/src/main/java/com/google/gerrit/server/schema/Schema_80.java
new file mode 100644
index 0000000..87394ba
--- /dev/null
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/schema/Schema_80.java
@@ -0,0 +1,45 @@
+// Copyright (C) 2013 The Android Open Source Project
+//
+// Licensed under the Apache License, Version 2.0 (the "License");
+// you may not use this file except in compliance with the License.
+// You may obtain a copy of the License at
+//
+// http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software
+// distributed under the License is distributed on an "AS IS" BASIS,
+// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+// See the License for the specific language governing permissions and
+// limitations under the License.
+
+package com.google.gerrit.server.schema;
+
+import com.google.gerrit.reviewdb.server.ReviewDb;
+import com.google.gwtorm.jdbc.JdbcSchema;
+import com.google.inject.Inject;
+import com.google.inject.Provider;
+
+import java.sql.SQLException;
+import java.sql.Statement;
+
+
+public class Schema_80 extends SchemaVersion {
+  @Inject
+  Schema_80(Provider<Schema_79> prior) {
+    super(prior);
+  }
+
+  @Override
+  protected void migrateData(final ReviewDb db, final UpdateUI ui)
+      throws SQLException {
+    final Statement stmt = ((JdbcSchema) db).getConnection().createStatement();
+    try {
+      stmt.executeUpdate("CREATE INDEX patch_set_approvals_byOpenLabel " +
+                         "ON patch_set_approvals (category_id, value) " +
+                         "WHERE change_open = 'Y'");
+    }
+    finally {
+      stmt.close();
+    }
+  }
+}
-- 
1.7.8.3

