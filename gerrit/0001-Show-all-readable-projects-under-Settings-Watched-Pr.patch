From ce7ccfc1ef81f954411fa4b337ec9f5e0d51e42b Mon Sep 17 00:00:00 2001
From: Martin Fick <mfick@codeaurora.org>
Date: Thu, 26 Aug 2010 16:47:41 -0600
Subject: [PATCH] Show all readable projects under Settings > Watched Projects

Add a disclosure panel with a list of unwatched projects
and their descriptions to the Settings > Watched Projects
screen. Selecting a project from the list populates the
project text box.

Bug: issue 204
Change-Id: Ifb24da3ba870b8d4fee09a0d8b7a1960b4ea6c98
---
 .../gerrit/client/account/AccountConstants.java    |    6 +
 .../client/account/AccountConstants.properties     |    6 +
 .../client/account/MyUnwatchedProjectsTable.java   |  107 ++++++
 .../client/account/MyWatchedProjectsScreen.java    |  371 +++++++++-----------
 .../gerrit/client/account/MyWatchesTable.java      |  185 ++++++++++
 .../com/google/gerrit/client/account/Util.java     |    6 +
 6 files changed, 468 insertions(+), 213 deletions(-)
 create mode 100644 gerrit-gwtui/src/main/java/com/google/gerrit/client/account/MyUnwatchedProjectsTable.java
 create mode 100644 gerrit-gwtui/src/main/java/com/google/gerrit/client/account/MyWatchesTable.java

diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/AccountConstants.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/AccountConstants.java
index 3756bed..1a385a4 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/AccountConstants.java
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/AccountConstants.java
@@ -81,6 +81,12 @@ public interface AccountConstants extends Constants {
   String buttonWatchProject();
   String defaultProjectName();
   String defaultFilter();
+  String unwatchedProjects();
+  String unwatchedProjectName();
+  String unwatchedProjectDescription();
+  String unwatchedProjectListOpen();
+  String unwatchedProjectListPrev();
+  String unwatchedProjectListNext();
   String watchedProjectName();
   String watchedProjectFilter();
   String watchedProjectColumnEmailNotifications();
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/AccountConstants.properties b/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/AccountConstants.properties
index 29ee14a..a66e639 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/AccountConstants.properties
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/AccountConstants.properties
@@ -62,6 +62,12 @@ sshJavaAppletNotAvailable = Open Key Unavailable: Java not enabled
 buttonWatchProject = Watch
 defaultProjectName = Project Name
 defaultFilter = branch:name, or other search expression
+unwatchedProjects = My Unwatched Projects
+unwatchedProjectName = Project Name
+unwatchedProjectDescription = Project Description
+unwatchedProjectListOpen = Select project
+unwatchedProjectListPrev = Previous project
+unwatchedProjectListNext = Next project
 watchedProjectName = Project Name
 watchedProjectFilter = Only If
 watchedProjectColumnEmailNotifications = Email Notifications
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/MyUnwatchedProjectsTable.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/MyUnwatchedProjectsTable.java
new file mode 100644
index 0000000..99f10ec
--- /dev/null
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/MyUnwatchedProjectsTable.java
@@ -0,0 +1,107 @@
+// Copyright (C) 2010 The Android Open Source Project
+//
+// Licensed under the Apache License, Version 2.0 (the "License");
+// you may not use this file except in compliance with the License.
+// You may obtain a copy of the License at
+//
+// http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software
+// distributed under the License is distributed on an "AS IS" BASIS,
+// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+// See the License for the specific language governing permissions and
+// limitations under the License.
+
+package com.google.gerrit.client.account;
+
+import com.google.gerrit.client.Gerrit;
+import com.google.gerrit.client.ui.NavigationTable;
+import com.google.gerrit.common.PageLinks;
+import com.google.gerrit.reviewdb.Project;
+import com.google.gwt.event.dom.client.ClickEvent;
+import com.google.gwt.event.dom.client.ClickHandler;
+import com.google.gwt.event.dom.client.KeyCodes;
+import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;
+import com.google.gwt.event.logical.shared.ValueChangeEvent;
+import com.google.gwt.event.logical.shared.ValueChangeHandler;
+import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
+import com.google.gwt.event.shared.HandlerRegistration;
+
+import java.util.List;
+
+public class MyUnwatchedProjectsTable extends NavigationTable<Project>
+    implements HasValueChangeHandlers<Project> {
+
+
+  public MyUnwatchedProjectsTable() {
+    setSavePointerId(PageLinks.SETTINGS_PROJECTS);
+    keysNavigation.add(new PrevKeyCommand(0, 'k',
+      Util.C.unwatchedProjectListPrev()));
+    keysNavigation.add(new NextKeyCommand(0, 'j',
+      Util.C.unwatchedProjectListNext()));
+    keysNavigation.add(new OpenKeyCommand(0, KeyCodes.KEY_ENTER,
+      Util.C.unwatchedProjectListOpen()));
+
+    table.setText(0, 1, Util.C.unwatchedProjectName());
+    table.setText(0, 2, Util.C.unwatchedProjectDescription());
+
+    final FlexCellFormatter fmt = table.getFlexCellFormatter();
+    fmt.addStyleName(0, 1, Gerrit.RESOURCES.css().dataHeader());
+    fmt.addStyleName(0, 2, Gerrit.RESOURCES.css().dataHeader());
+
+    table.addClickHandler(new ClickHandler() {
+      @Override
+      public void onClick(final ClickEvent event) {
+        onOpenRow(table.getCellForEvent(event).getRowIndex());
+      }
+    });
+  }
+
+  @Override
+  protected Object getRowItemKey(final Project item) {
+    return item.getNameKey();
+  }
+
+  @Override
+  protected void onOpenRow(final int row) {
+    if (row > 0) {
+      movePointerTo(row);
+      ValueChangeEvent.fire(MyUnwatchedProjectsTable.this, getRowItem(row));
+    }
+  }
+
+  void display(final List<Project> projects) {
+    while (1 < table.getRowCount())
+      table.removeRow(table.getRowCount() - 1);
+
+    for (final Project k : projects)
+      insert(table.getRowCount(), k);
+
+    finishDisplay();
+  }
+
+  void insert(final int row, final Project k) {
+    table.insertRow(row);
+
+    applyDataRowStyle(row);
+
+    final FlexCellFormatter fmt = table.getFlexCellFormatter();
+    fmt.addStyleName(row, 1, Gerrit.RESOURCES.css().dataCell());
+    fmt.addStyleName(row, 1, Gerrit.RESOURCES.css().cPROJECT());
+    fmt.addStyleName(row, 2, Gerrit.RESOURCES.css().dataCell());
+
+    populate(row, k);
+  }
+
+  void populate(final int row, final Project k) {
+    table.setText(row, 1, k.getName());
+    table.setText(row, 2, k.getDescription());
+
+    setRowItem(row, k);
+  }
+
+  public HandlerRegistration addValueChangeHandler(
+      final ValueChangeHandler<Project> handler) {
+    return addHandler(handler, ValueChangeEvent.getType());
+  }
+}
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/MyWatchedProjectsScreen.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/MyWatchedProjectsScreen.java
index 18b192e..1359202 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/MyWatchedProjectsScreen.java
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/MyWatchedProjectsScreen.java
@@ -17,132 +17,189 @@ package com.google.gerrit.client.account;
 import com.google.gerrit.client.Gerrit;
 import com.google.gerrit.client.rpc.GerritCallback;
 import com.google.gerrit.client.rpc.ScreenLoadCallback;
-import com.google.gerrit.client.ui.FancyFlexTable;
 import com.google.gerrit.client.ui.HintTextBox;
-import com.google.gerrit.client.ui.ProjectLink;
 import com.google.gerrit.client.ui.ProjectNameSuggestOracle;
 import com.google.gerrit.common.data.AccountProjectWatchInfo;
 import com.google.gerrit.reviewdb.AccountProjectWatch;
-import com.google.gerrit.reviewdb.Change.Status;
+import com.google.gerrit.reviewdb.Project;
 import com.google.gwt.event.dom.client.ClickEvent;
 import com.google.gwt.event.dom.client.ClickHandler;
 import com.google.gwt.event.dom.client.KeyCodes;
 import com.google.gwt.event.dom.client.KeyPressEvent;
 import com.google.gwt.event.dom.client.KeyPressHandler;
+import com.google.gwt.event.logical.shared.OpenEvent;
+import com.google.gwt.event.logical.shared.OpenHandler;
 import com.google.gwt.event.logical.shared.SelectionEvent;
 import com.google.gwt.event.logical.shared.SelectionHandler;
-import com.google.gwt.user.client.DOM;
+import com.google.gwt.event.logical.shared.ValueChangeEvent;
+import com.google.gwt.event.logical.shared.ValueChangeHandler;
+import com.google.gwt.event.shared.HandlerRegistration;
 import com.google.gwt.user.client.ui.Button;
-import com.google.gwt.user.client.ui.CheckBox;
+import com.google.gwt.user.client.ui.DisclosurePanel;
 import com.google.gwt.user.client.ui.FlowPanel;
 import com.google.gwt.user.client.ui.Grid;
-import com.google.gwt.user.client.ui.Label;
 import com.google.gwt.user.client.ui.SuggestBox;
-import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;
+import com.google.gwt.user.client.ui.HasHorizontalAlignment;
+import com.google.gwt.user.client.ui.HasVerticalAlignment;
 import com.google.gwt.user.client.ui.HTMLTable.CellFormatter;
 import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
-import com.google.gwtjsonrpc.client.VoidResult;
 
+import java.util.ArrayList;
 import java.util.HashSet;
 import java.util.List;
 import java.util.Set;
 
 public class MyWatchedProjectsScreen extends SettingsScreen {
-  private WatchTable watches;
-
   private Button addNew;
   private HintTextBox nameBox;
   private SuggestBox nameTxt;
   private HintTextBox filterTxt;
+  private MyWatchesTable watchesTab;
+  private DisclosurePanel unwatchedDp;
+  private MyUnwatchedProjectsTable unwatchedTab;
   private Button delSel;
+
+  private HandlerRegistration dpReg;
   private boolean submitOnSelection;
 
+  private List<Project> allProjects;
+  private Set<Project.NameKey> watchedKeys;
+
   @Override
   protected void onInitUI() {
     super.onInitUI();
+    createWidgets();
+
+    final Grid grid = new Grid(2, 2);
+    grid.setStyleName(Gerrit.RESOURCES.css().infoBlock());
+    grid.setText(0, 0, Util.C.watchedProjectName());
+    grid.setWidget(0, 1, nameTxt);
+
+    grid.setText(1, 0, Util.C.watchedProjectFilter());
+    grid.setWidget(1, 1, filterTxt);
+
+    final CellFormatter fmt = grid.getCellFormatter();
+    fmt.addStyleName(0, 0, Gerrit.RESOURCES.css().topmost());
+    fmt.addStyleName(0, 1, Gerrit.RESOURCES.css().topmost());
+    fmt.addStyleName(0, 0, Gerrit.RESOURCES.css().header());
+    fmt.addStyleName(1, 0, Gerrit.RESOURCES.css().header());
+    fmt.addStyleName(1, 0, Gerrit.RESOURCES.css().bottomheader());
+
+    unwatchedDp.setContent(unwatchedTab);
+
+    final Grid grid2 = new Grid(1, 2);
+    final FlowPanel fp2 = new FlowPanel();
+    fp2.add(addNew);
+    grid2.setWidget(0, 0, fp2);
+    grid2.setWidget(0, 1, unwatchedDp);
+
+    final CellFormatter fmt2 = grid2.getCellFormatter();
+    fmt2.setAlignment(0, 0, HasHorizontalAlignment.ALIGN_LEFT,
+                            HasVerticalAlignment.ALIGN_TOP);
+
+    final FlowPanel fp = new FlowPanel();
+    fp.setStyleName(Gerrit.RESOURCES.css().addWatchPanel());
+    fp.add(grid);
+    fp.add(grid2);
+    add(fp);
+
+    add(watchesTab);
+    add(delSel);
+  }
 
-    {
-      nameBox = new HintTextBox();
-      nameTxt = new SuggestBox(new ProjectNameSuggestOracle(), nameBox);
-      nameBox.setVisibleLength(50);
-      nameBox.setHintText(Util.C.defaultProjectName());
-      nameBox.addKeyPressHandler(new KeyPressHandler() {
-        @Override
-        public void onKeyPress(KeyPressEvent event) {
-          submitOnSelection = false;
+  protected void createWidgets() {
+    nameBox = new HintTextBox();
+    nameTxt = new SuggestBox(new ProjectNameSuggestOracle(), nameBox);
+    nameBox.setVisibleLength(50);
+    nameBox.setHintText(Util.C.defaultProjectName());
+    nameBox.addKeyPressHandler(new KeyPressHandler() {
+      @Override
+      public void onKeyPress(KeyPressEvent event) {
+        submitOnSelection = false;
 
-          if (event.getCharCode() == KeyCodes.KEY_ENTER) {
-            if (nameTxt.isSuggestionListShowing()) {
-              submitOnSelection = true;
-            } else {
-              doAddNew();
-            }
-          }
-        }
-      });
-      nameTxt.addSelectionHandler(new SelectionHandler<Suggestion>() {
-        @Override
-        public void onSelection(SelectionEvent<Suggestion> event) {
-          if (submitOnSelection) {
-            submitOnSelection = false;
+        if (event.getCharCode() == KeyCodes.KEY_ENTER) {
+          if (nameTxt.isSuggestionListShowing()) {
+            submitOnSelection = true;
+          } else {
             doAddNew();
           }
         }
-      });
-
-      filterTxt = new HintTextBox();
-      filterTxt.setVisibleLength(50);
-      filterTxt.setHintText(Util.C.defaultFilter());
-      filterTxt.addKeyPressHandler(new KeyPressHandler() {
-        @Override
-        public void onKeyPress(KeyPressEvent event) {
-          if (event.getCharCode() == KeyCodes.KEY_ENTER) {
-            doAddNew();
-          }
+      }
+    });
+    nameTxt.addSelectionHandler(new SelectionHandler<Suggestion>() {
+      @Override
+      public void onSelection(SelectionEvent<Suggestion> event) {
+        if (submitOnSelection) {
+          submitOnSelection = false;
+          doAddNew();
         }
-      });
+      }
+    });
 
-      addNew = new Button(Util.C.buttonWatchProject());
-      addNew.addClickHandler(new ClickHandler() {
-        @Override
-        public void onClick(final ClickEvent event) {
+    filterTxt = new HintTextBox();
+    filterTxt.setVisibleLength(50);
+    filterTxt.setHintText(Util.C.defaultFilter());
+    filterTxt.addKeyPressHandler(new KeyPressHandler() {
+      @Override
+      public void onKeyPress(KeyPressEvent event) {
+        if (event.getCharCode() == KeyCodes.KEY_ENTER) {
           doAddNew();
         }
-      });
-
-      final Grid grid = new Grid(2, 2);
-      grid.setStyleName(Gerrit.RESOURCES.css().infoBlock());
-      grid.setText(0, 0, Util.C.watchedProjectName());
-      grid.setWidget(0, 1, nameTxt);
-
-      grid.setText(1, 0, Util.C.watchedProjectFilter());
-      grid.setWidget(1, 1, filterTxt);
-
-      final CellFormatter fmt = grid.getCellFormatter();
-      fmt.addStyleName(0, 0, Gerrit.RESOURCES.css().topmost());
-      fmt.addStyleName(0, 1, Gerrit.RESOURCES.css().topmost());
-      fmt.addStyleName(0, 0, Gerrit.RESOURCES.css().header());
-      fmt.addStyleName(1, 0, Gerrit.RESOURCES.css().header());
-      fmt.addStyleName(1, 0, Gerrit.RESOURCES.css().bottomheader());
-
-      final FlowPanel fp = new FlowPanel();
-      fp.setStyleName(Gerrit.RESOURCES.css().addWatchPanel());
-      fp.add(grid);
-      fp.add(addNew);
-      add(fp);
-    }
+      }
+    });
+
+    addNew = new Button(Util.C.buttonWatchProject());
+    addNew.addClickHandler(new ClickHandler() {
+      @Override
+      public void onClick(final ClickEvent event) {
+        doAddNew();
+      }
+    });
 
-    watches = new WatchTable();
-    add(watches);
+    unwatchedTab = new MyUnwatchedProjectsTable();
+    unwatchedTab.addValueChangeHandler(new ValueChangeHandler<Project>() {
+      @Override
+      public void onValueChange(ValueChangeEvent<Project> event) {
+        nameBox.setText(event.getValue().getName());
+      }
+    });
+
+    unwatchedDp = new DisclosurePanel(
+        Util.C.unwatchedProjects());
+    dpReg = unwatchedDp.addOpenHandler(new OpenHandler<DisclosurePanel>(){
+      @Override
+      public void onOpen(OpenEvent<DisclosurePanel> event) {
+        populateUnwatched();
+        dpReg.removeHandler();
+      }
+    });
+
+    watchesTab = new MyWatchesTable() {
+      @Override
+      protected List<AccountProjectWatchInfo> remove(
+          Set<AccountProjectWatch.Key> ids) {
+        List<AccountProjectWatchInfo> removed = super.remove(ids);
+        for (AccountProjectWatchInfo w : removed) {
+          watchedKeys.remove(w.getProject().getNameKey());
+        }
+        updateUnwatchedProjects();
+        return removed;
+      }
+      @Override
+      public void insertWatch(final AccountProjectWatchInfo k) {
+        super.insertWatch(k);
+        watchedKeys.add(k.getProject().getNameKey());
+        updateUnwatchedProjects();
+      }
+    };
 
     delSel = new Button(Util.C.buttonDeleteSshKey());
     delSel.addClickHandler(new ClickHandler() {
       @Override
       public void onClick(final ClickEvent event) {
-        watches.deleteChecked();
+        watchesTab.deleteChecked();
       }
     });
-    add(delSel);
   }
 
   @Override
@@ -151,7 +208,7 @@ public class MyWatchedProjectsScreen extends SettingsScreen {
     populateWatches();
   }
 
-  void doAddNew() {
+  protected void doAddNew() {
     final String projectName = nameTxt.getText();
     if ("".equals(projectName)) {
       return;
@@ -175,7 +232,7 @@ public class MyWatchedProjectsScreen extends SettingsScreen {
             filterTxt.setEnabled(true);
 
             nameTxt.setText("");
-            watches.insertWatch(result);
+            watchesTab.insertWatch(result);
           }
 
           @Override
@@ -194,150 +251,38 @@ public class MyWatchedProjectsScreen extends SettingsScreen {
         new ScreenLoadCallback<List<AccountProjectWatchInfo>>(this) {
       @Override
       public void preDisplay(final List<AccountProjectWatchInfo> result) {
-        watches.display(result);
+        watchedKeys = new HashSet<Project.NameKey>(result.size());
+        for (AccountProjectWatchInfo w : result) {
+          watchedKeys.add(w.getProject().getNameKey());
+        }
+        watchesTab.display(result);
+        updateUnwatchedProjects();
       }
     });
   }
 
-  private class WatchTable extends FancyFlexTable<AccountProjectWatchInfo> {
-    WatchTable() {
-      table.setWidth("");
-      table.insertRow(1);
-      table.setText(0, 2, Util.C.watchedProjectName());
-      table.setText(0, 3, Util.C.watchedProjectColumnEmailNotifications());
-
-      final FlexCellFormatter fmt = table.getFlexCellFormatter();
-      fmt.addStyleName(0, 1, Gerrit.RESOURCES.css().iconHeader());
-      fmt.addStyleName(0, 2, Gerrit.RESOURCES.css().dataHeader());
-      fmt.addStyleName(0, 3, Gerrit.RESOURCES.css().dataHeader());
-      fmt.setRowSpan(0, 0, 2);
-      fmt.setRowSpan(0, 1, 2);
-      fmt.setRowSpan(0, 2, 2);
-      DOM.setElementProperty(fmt.getElement(0, 3), "align", "center");
-
-      fmt.setColSpan(0, 3, 3);
-      table.setText(1, 0, Util.C.watchedProjectColumnNewChanges());
-      table.setText(1, 1, Util.C.watchedProjectColumnAllComments());
-      table.setText(1, 2, Util.C.watchedProjectColumnSubmittedChanges());
-      fmt.addStyleName(1, 0, Gerrit.RESOURCES.css().dataHeader());
-      fmt.addStyleName(1, 1, Gerrit.RESOURCES.css().dataHeader());
-      fmt.addStyleName(1, 2, Gerrit.RESOURCES.css().dataHeader());
-    }
-
-    void deleteChecked() {
-      final Set<AccountProjectWatch.Key> ids = getCheckedIds();
-      if (!ids.isEmpty()) {
-        Util.ACCOUNT_SVC.deleteProjectWatches(ids,
-            new GerritCallback<VoidResult>() {
-              public void onSuccess(final VoidResult result) {
-                remove(ids);
-              }
-            });
-      }
-    }
-
-    void remove(Set<AccountProjectWatch.Key> ids) {
-      for (int row = 1; row < table.getRowCount();) {
-        final AccountProjectWatchInfo k = getRowItem(row);
-        if (k != null && ids.contains(k.getWatch().getKey())) {
-          table.removeRow(row);
-        } else {
-          row++;
-        }
-      }
-    }
-
-    Set<AccountProjectWatch.Key> getCheckedIds() {
-      final Set<AccountProjectWatch.Key> ids =
-          new HashSet<AccountProjectWatch.Key>();
-      for (int row = 1; row < table.getRowCount(); row++) {
-        final AccountProjectWatchInfo k = getRowItem(row);
-        if (k != null && ((CheckBox) table.getWidget(row, 1)).getValue()) {
-          ids.add(k.getWatch().getKey());
-        }
-      }
-      return ids;
-    }
-
-    void insertWatch(final AccountProjectWatchInfo k) {
-      final String newName = k.getProject().getName();
-      int row = 1;
-      for (; row < table.getRowCount(); row++) {
-        final AccountProjectWatchInfo i = getRowItem(row);
-        if (i != null && i.getProject().getName().compareTo(newName) >= 0) {
-          break;
-        }
+  protected void populateUnwatched() {
+    Util.PROJECT_SVC.visibleProjects(
+        new ScreenLoadCallback<List<Project>>(this) {
+      @Override
+      protected void preDisplay(final List<Project> result) {
+        allProjects = result;
+        updateUnwatchedProjects();
       }
+    });
+  }
 
-      table.insertRow(row);
-      applyDataRowStyle(row);
-      populate(row, k);
-    }
-
-    void display(final List<AccountProjectWatchInfo> result) {
-      while (2 < table.getRowCount())
-        table.removeRow(table.getRowCount() - 1);
-
-      for (final AccountProjectWatchInfo k : result) {
-        final int row = table.getRowCount();
-        table.insertRow(row);
-        applyDataRowStyle(row);
-        populate(row, k);
-      }
+  protected void updateUnwatchedProjects() {
+    if (watchedKeys == null || allProjects == null) {
+      return;
     }
 
-    void populate(final int row, final AccountProjectWatchInfo info) {
-      final FlowPanel fp = new FlowPanel();
-      fp.add(new ProjectLink(info.getProject().getNameKey(), Status.NEW));
-      if (info.getWatch().getFilter() != null) {
-        Label filter = new Label(info.getWatch().getFilter());
-        filter.setStyleName(Gerrit.RESOURCES.css().watchedProjectFilter());
-        fp.add(filter);
+    List<Project> unwatchedPs = new ArrayList<Project>(allProjects.size());
+    for (Project p : allProjects) {
+      if (!watchedKeys.contains(p.getNameKey())) {
+        unwatchedPs.add(p);
       }
-
-      table.setWidget(row, 1, new CheckBox());
-      table.setWidget(row, 2, fp);
-
-      addNotifyButton(AccountProjectWatch.Type.NEW_CHANGES, info, row, 3);
-      addNotifyButton(AccountProjectWatch.Type.COMMENTS, info, row, 4);
-      addNotifyButton(AccountProjectWatch.Type.SUBMITS, info, row, 5);
-
-      final FlexCellFormatter fmt = table.getFlexCellFormatter();
-      fmt.addStyleName(row, 1, Gerrit.RESOURCES.css().iconCell());
-      fmt.addStyleName(row, 2, Gerrit.RESOURCES.css().dataCell());
-      fmt.addStyleName(row, 3, Gerrit.RESOURCES.css().dataCell());
-      fmt.addStyleName(row, 4, Gerrit.RESOURCES.css().dataCell());
-      fmt.addStyleName(row, 5, Gerrit.RESOURCES.css().dataCell());
-
-      setRowItem(row, info);
-    }
-
-    protected void addNotifyButton(final AccountProjectWatch.Type type,
-        final AccountProjectWatchInfo info, final int row, final int col) {
-      final CheckBox cbox = new CheckBox();
-
-      cbox.addClickHandler(new ClickHandler() {
-        @Override
-        public void onClick(final ClickEvent event) {
-          final boolean oldVal = info.getWatch().isNotify(type);
-          info.getWatch().setNotify(type, cbox.getValue());
-          Util.ACCOUNT_SVC.updateProjectWatch(info.getWatch(),
-              new GerritCallback<VoidResult>() {
-                public void onSuccess(final VoidResult result) {
-                }
-
-                @Override
-                public void onFailure(final Throwable caught) {
-                  info.getWatch().setNotify(type, oldVal);
-                  cbox.setValue(oldVal);
-                  super.onFailure(caught);
-                }
-              });
-        }
-      });
-
-      cbox.setValue(info.getWatch().isNotify(type));
-      table.setWidget(row, col, cbox);
     }
+    unwatchedTab.display(unwatchedPs);
   }
 }
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/MyWatchesTable.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/MyWatchesTable.java
new file mode 100644
index 0000000..4c25312
--- /dev/null
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/MyWatchesTable.java
@@ -0,0 +1,185 @@
+// Copyright (C) 2010 The Android Open Source Project
+//
+// Licensed under the Apache License, Version 2.0 (the "License");
+// you may not use this file except in compliance with the License.
+// You may obtain a copy of the License at
+//
+// http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software
+// distributed under the License is distributed on an "AS IS" BASIS,
+// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+// See the License for the specific language governing permissions and
+// limitations under the License.
+
+package com.google.gerrit.client.account;
+
+import com.google.gerrit.client.Gerrit;
+import com.google.gerrit.client.rpc.GerritCallback;
+import com.google.gerrit.client.ui.FancyFlexTable;
+import com.google.gerrit.client.ui.ProjectLink;
+import com.google.gerrit.common.data.AccountProjectWatchInfo;
+import com.google.gerrit.reviewdb.AccountProjectWatch;
+import com.google.gerrit.reviewdb.Change.Status;
+import com.google.gwt.event.dom.client.ClickEvent;
+import com.google.gwt.event.dom.client.ClickHandler;
+import com.google.gwt.user.client.DOM;
+import com.google.gwt.user.client.ui.CheckBox;
+import com.google.gwt.user.client.ui.FlowPanel;
+import com.google.gwt.user.client.ui.Label;
+import com.google.gwt.user.client.ui.FlexTable.FlexCellFormatter;
+import com.google.gwtjsonrpc.client.VoidResult;
+
+import java.util.ArrayList;
+import java.util.HashSet;
+import java.util.List;
+import java.util.Set;
+
+public class MyWatchesTable extends FancyFlexTable<AccountProjectWatchInfo> {
+
+  public MyWatchesTable() {
+    table.setWidth("");
+    table.insertRow(1);
+    table.setText(0, 2, Util.C.watchedProjectName());
+    table.setText(0, 3, Util.C.watchedProjectColumnEmailNotifications());
+
+    final FlexCellFormatter fmt = table.getFlexCellFormatter();
+    fmt.addStyleName(0, 1, Gerrit.RESOURCES.css().iconHeader());
+    fmt.addStyleName(0, 2, Gerrit.RESOURCES.css().dataHeader());
+    fmt.addStyleName(0, 3, Gerrit.RESOURCES.css().dataHeader());
+    fmt.setRowSpan(0, 0, 2);
+    fmt.setRowSpan(0, 1, 2);
+    fmt.setRowSpan(0, 2, 2);
+    DOM.setElementProperty(fmt.getElement(0, 3), "align", "center");
+
+    fmt.setColSpan(0, 3, 3);
+    table.setText(1, 0, Util.C.watchedProjectColumnNewChanges());
+    table.setText(1, 1, Util.C.watchedProjectColumnAllComments());
+    table.setText(1, 2, Util.C.watchedProjectColumnSubmittedChanges());
+    fmt.addStyleName(1, 0, Gerrit.RESOURCES.css().dataHeader());
+    fmt.addStyleName(1, 1, Gerrit.RESOURCES.css().dataHeader());
+    fmt.addStyleName(1, 2, Gerrit.RESOURCES.css().dataHeader());
+  }
+
+  public void deleteChecked() {
+    final Set<AccountProjectWatch.Key> ids = getCheckedIds();
+    if (!ids.isEmpty()) {
+      Util.ACCOUNT_SVC.deleteProjectWatches(ids,
+          new GerritCallback<VoidResult>() {
+            public void onSuccess(final VoidResult result) {
+              remove(ids);
+            }
+          });
+    }
+  }
+
+  protected List<AccountProjectWatchInfo> remove(
+      Set<AccountProjectWatch.Key> ids) {
+    List<AccountProjectWatchInfo> removed =
+      new ArrayList<AccountProjectWatchInfo>(ids.size());
+
+    for (int row = 1; row < table.getRowCount();) {
+      final AccountProjectWatchInfo k = getRowItem(row);
+      if (k != null && ids.contains(k.getWatch().getKey())) {
+        table.removeRow(row);
+        removed.add(k);
+      } else {
+        row++;
+      }
+    }
+    return removed;
+  }
+
+  protected Set<AccountProjectWatch.Key> getCheckedIds() {
+    final Set<AccountProjectWatch.Key> ids =
+        new HashSet<AccountProjectWatch.Key>();
+    for (int row = 1; row < table.getRowCount(); row++) {
+      final AccountProjectWatchInfo k = getRowItem(row);
+      if (k != null && ((CheckBox) table.getWidget(row, 1)).getValue()) {
+        ids.add(k.getWatch().getKey());
+      }
+    }
+    return ids;
+  }
+
+  public void insertWatch(final AccountProjectWatchInfo k) {
+    final String newName = k.getProject().getName();
+    int row = 1;
+    for (; row < table.getRowCount(); row++) {
+      final AccountProjectWatchInfo i = getRowItem(row);
+      if (i != null && i.getProject().getName().compareTo(newName) >= 0) {
+        break;
+      }
+    }
+
+    table.insertRow(row);
+    applyDataRowStyle(row);
+    populate(row, k);
+  }
+
+  public void display(final List<AccountProjectWatchInfo> result) {
+    while (2 < table.getRowCount())
+      table.removeRow(table.getRowCount() - 1);
+
+    for (final AccountProjectWatchInfo k : result) {
+      final int row = table.getRowCount();
+      table.insertRow(row);
+      applyDataRowStyle(row);
+      populate(row, k);
+    }
+  }
+
+  protected void populate(final int row, final AccountProjectWatchInfo info) {
+    final FlowPanel fp = new FlowPanel();
+    fp.add(new ProjectLink(info.getProject().getNameKey(), Status.NEW));
+    if (info.getWatch().getFilter() != null) {
+      Label filter = new Label(info.getWatch().getFilter());
+      filter.setStyleName(Gerrit.RESOURCES.css().watchedProjectFilter());
+      fp.add(filter);
+    }
+
+    table.setWidget(row, 1, new CheckBox());
+    table.setWidget(row, 2, fp);
+
+    addNotifyButton(AccountProjectWatch.Type.NEW_CHANGES, info, row, 3);
+    addNotifyButton(AccountProjectWatch.Type.COMMENTS, info, row, 4);
+    addNotifyButton(AccountProjectWatch.Type.SUBMITS, info, row, 5);
+
+    final FlexCellFormatter fmt = table.getFlexCellFormatter();
+    fmt.addStyleName(row, 1, Gerrit.RESOURCES.css().iconCell());
+    fmt.addStyleName(row, 2, Gerrit.RESOURCES.css().dataCell());
+    fmt.addStyleName(row, 3, Gerrit.RESOURCES.css().dataCell());
+    fmt.addStyleName(row, 4, Gerrit.RESOURCES.css().dataCell());
+    fmt.addStyleName(row, 5, Gerrit.RESOURCES.css().dataCell());
+
+    setRowItem(row, info);
+  }
+
+  protected void addNotifyButton(final AccountProjectWatch.Type type,
+      final AccountProjectWatchInfo info, final int row, final int col) {
+    final CheckBox cbox = new CheckBox();
+
+    cbox.addClickHandler(new ClickHandler() {
+      @Override
+      public void onClick(final ClickEvent event) {
+        final boolean oldVal = info.getWatch().isNotify(type);
+        info.getWatch().setNotify(type, cbox.getValue());
+        Util.ACCOUNT_SVC.updateProjectWatch(info.getWatch(),
+            new GerritCallback<VoidResult>() {
+              public void onSuccess(final VoidResult result) {
+              }
+
+              @Override
+              public void onFailure(final Throwable caught) {
+                info.getWatch().setNotify(type, oldVal);
+                cbox.setValue(oldVal);
+                super.onFailure(caught);
+              }
+            });
+      }
+    });
+
+    cbox.setValue(info.getWatch().isNotify(type));
+    table.setWidget(row, col, cbox);
+  }
+}
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/Util.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/Util.java
index 5ed79e6..59e439d 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/Util.java
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/account/Util.java
@@ -16,6 +16,8 @@ package com.google.gerrit.client.account;
 
 import com.google.gerrit.common.data.AccountSecurity;
 import com.google.gerrit.common.data.AccountService;
+import com.google.gerrit.common.data.ProjectAdminService;
+import com.google.gerrit.reviewdb.Project;
 import com.google.gwt.core.client.GWT;
 import com.google.gwtjsonrpc.client.JsonUtil;
 
@@ -24,6 +26,7 @@ public class Util {
   public static final AccountMessages M = GWT.create(AccountMessages.class);
   public static final AccountService ACCOUNT_SVC;
   public static final AccountSecurity ACCOUNT_SEC;
+  public static final ProjectAdminService PROJECT_SVC;
 
   static {
     ACCOUNT_SVC = GWT.create(AccountService.class);
@@ -31,5 +34,8 @@ public class Util {
 
     ACCOUNT_SEC = GWT.create(AccountSecurity.class);
     JsonUtil.bind(ACCOUNT_SEC, "rpc/AccountSecurity");
+
+    PROJECT_SVC = GWT.create(ProjectAdminService.class);
+    JsonUtil.bind(PROJECT_SVC, "rpc/ProjectAdminService");
   }
 }
-- 
1.7.2.2

