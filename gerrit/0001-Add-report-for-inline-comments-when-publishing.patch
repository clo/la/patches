From c34ff3a3e0eabaf4da12b2fb4f1300ec79148b60 Mon Sep 17 00:00:00 2001
From: Martin Fick <mfick@codeaurora.org>
Date: Fri, 11 Jun 2010 16:36:30 -0600
Subject: [PATCH] Add report for inline comments when publishing

Add a rpeort to the main changeset page whenever inline comments
are published even if no other changes are published.
Additionally, prevent 'no-score' approvals from being recorded
unless they are changes.  And, finally, make both the WUI and
SSH cmdline review use the same PublishComments class to process
approvals and comments.  This fixes many cmdline oddities with
duplicate approvals.

Bug: issue 396
---
 .../gerrit/server/patch/PublishComments.java       |   77 +++++++++---
 .../google/gerrit/sshd/commands/ReviewCommand.java |  126 +++----------------
 2 files changed, 77 insertions(+), 126 deletions(-)

diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/patch/PublishComments.java b/gerrit-server/src/main/java/com/google/gerrit/server/patch/PublishComments.java
index 60f34b9..60bdb4c 100644
--- a/gerrit-server/src/main/java/com/google/gerrit/server/patch/PublishComments.java
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/patch/PublishComments.java
@@ -50,6 +50,7 @@ import java.util.Map;
 import java.util.Set;
 import java.util.concurrent.Callable;
 
+
 public class PublishComments implements Callable<VoidResult> {
   private static final Logger log =
       LoggerFactory.getLogger(PublishComments.class);
@@ -143,6 +144,7 @@ public class PublishComments implements Callable<VoidResult> {
     final Set<ApprovalCategory.Id> dirty = new HashSet<ApprovalCategory.Id>();
     final List<PatchSetApproval> ins = new ArrayList<PatchSetApproval>();
     final List<PatchSetApproval> upd = new ArrayList<PatchSetApproval>();
+    final List<ApprovalCategoryValue.Id> ignore = new ArrayList<ApprovalCategoryValue.Id>();
     final Collection<PatchSetApproval> all =
         db.patchSetApprovals().byPatchSet(patchSetId).toList();
     final Map<ApprovalCategory.Id, PatchSetApproval> mine = mine(all);
@@ -152,13 +154,17 @@ public class PublishComments implements Callable<VoidResult> {
     for (final ApprovalCategoryValue.Id want : approvals) {
       PatchSetApproval a = mine.get(want.getParentKey());
       if (a == null) {
-        a = new PatchSetApproval(new PatchSetApproval.Key(//
-            patchSetId, user.getAccountId(), want.getParentKey()), want.get());
-        a.cache(change);
-        ins.add(a);
-        all.add(a);
-        mine.put(a.getCategoryId(), a);
-        dirty.add(a.getCategoryId());
+       if (want.get() != 0) {
+          a = new PatchSetApproval(new PatchSetApproval.Key(
+              patchSetId, user.getAccountId(), want.getParentKey()), want.get());
+          a.cache(change);
+          ins.add(a);
+          all.add(a);
+          mine.put(a.getCategoryId(), a);
+          dirty.add(a.getCategoryId());
+        } else {
+          ignore.add(want);
+        }
       }
     }
 
@@ -167,19 +173,21 @@ public class PublishComments implements Callable<VoidResult> {
     final FunctionState functionState =
         functionStateFactory.create(change, patchSetId, all);
     for (final ApprovalCategoryValue.Id want : approvals) {
-      final PatchSetApproval a = mine.get(want.getParentKey());
-      final short o = a.getValue();
-      a.setValue(want.get());
-      a.cache(change);
-      functionState.normalize(types.getApprovalType(a.getCategoryId()), a);
-      if (o != a.getValue()) {
-        // Value changed, ensure we update the database.
-        //
-        a.setGranted();
-        dirty.add(a.getCategoryId());
-      }
-      if (!ins.contains(a)) {
-        upd.add(a);
+      if (ignore.indexOf(want) == -1) {
+        final PatchSetApproval a = mine.get(want.getParentKey());
+        final short o = a.getValue();
+        a.setValue(want.get());
+        a.cache(change);
+        functionState.normalize(types.getApprovalType(a.getCategoryId()), a);
+        if (o != a.getValue()) {
+          // Value changed, ensure we update the database.
+          //
+          a.setGranted();
+          dirty.add(a.getCategoryId());
+        }
+        if (!ins.contains(a)) {
+          upd.add(a);
+        }
       }
     }
 
@@ -215,6 +223,8 @@ public class PublishComments implements Callable<VoidResult> {
 
     db.patchSetApprovals().update(upd);
     db.patchSetApprovals().insert(ins);
+
+    summarizeInlineComments(msgbuf);
     message(msgbuf.toString());
   }
 
@@ -293,4 +303,31 @@ public class PublishComments implements Callable<VoidResult> {
 
     hooks.doCommentAddedHook(change, user.getAccount(), patchSet, messageText, changed);
   }
+
+  private void summarizeInlineComments(StringBuilder in) {
+    StringBuilder txt = new StringBuilder();
+
+    String prev = null;
+    int count = 0;
+    for (final PatchLineComment c : drafts) {
+      String name = c.getKey().getParentKey().get();
+      if(prev != null && prev != name) {
+        txt.append("\t"+ prev +"("+ count +")\n");
+        count = 1;
+      } else {
+        count++;
+      }
+      prev = name;
+    }
+    if (count != 0) {
+      txt.append("\t"+ prev +"("+ count +")\n");
+    }
+
+    if(txt.length() != 0) {
+      if(in.length() != 0) {
+        in.append("\n\n");
+      }
+      in.append("Inline Comments:\n" + txt);
+    }
+  }
 }
diff --git a/gerrit-sshd/src/main/java/com/google/gerrit/sshd/commands/ReviewCommand.java b/gerrit-sshd/src/main/java/com/google/gerrit/sshd/commands/ReviewCommand.java
index d9f9eae..236273e 100644
--- a/gerrit-sshd/src/main/java/com/google/gerrit/sshd/commands/ReviewCommand.java
+++ b/gerrit-sshd/src/main/java/com/google/gerrit/sshd/commands/ReviewCommand.java
@@ -14,24 +14,20 @@
 
 package com.google.gerrit.sshd.commands;
 
-import com.google.gerrit.common.ChangeHookRunner;
 import com.google.gerrit.common.data.ApprovalType;
 import com.google.gerrit.common.data.ApprovalTypes;
 import com.google.gerrit.reviewdb.ApprovalCategory;
 import com.google.gerrit.reviewdb.ApprovalCategoryValue;
 import com.google.gerrit.reviewdb.Change;
-import com.google.gerrit.reviewdb.ChangeMessage;
 import com.google.gerrit.reviewdb.PatchSet;
-import com.google.gerrit.reviewdb.PatchSetApproval;
 import com.google.gerrit.reviewdb.RevId;
 import com.google.gerrit.reviewdb.ReviewDb;
 import com.google.gerrit.server.ChangeUtil;
 import com.google.gerrit.server.IdentifiedUser;
 import com.google.gerrit.server.git.MergeQueue;
-import com.google.gerrit.server.mail.CommentSender;
 import com.google.gerrit.server.mail.EmailException;
-import com.google.gerrit.server.patch.PatchSetInfoFactory;
 import com.google.gerrit.server.patch.PatchSetInfoNotAvailableException;
+import com.google.gerrit.server.patch.PublishComments;
 import com.google.gerrit.server.project.ChangeControl;
 import com.google.gerrit.server.project.NoSuchChangeException;
 import com.google.gerrit.server.project.ProjectControl;
@@ -51,10 +47,8 @@ import org.slf4j.LoggerFactory;
 import java.io.IOException;
 import java.util.ArrayList;
 import java.util.Collections;
-import java.util.HashMap;
 import java.util.HashSet;
 import java.util.List;
-import java.util.Map;
 import java.util.Set;
 
 public class ReviewCommand extends BaseCommand {
@@ -102,12 +96,6 @@ public class ReviewCommand extends BaseCommand {
   private MergeQueue merger;
 
   @Inject
-  private CommentSender.Factory commentSenderFactory;
-
-  @Inject
-  private PatchSetInfoFactory patchSetInfoFactory;
-
-  @Inject
   private ApprovalTypes approvalTypes;
 
   @Inject
@@ -116,11 +104,11 @@ public class ReviewCommand extends BaseCommand {
   @Inject
   private FunctionState.Factory functionStateFactory;
 
-  @Inject
-  private ChangeHookRunner hooks;
-
   private List<ApproveOption> optionList;
 
+  @Inject
+  private PublishComments.Factory publishCommentsFactory;
+
   @Override
   public final void start(final Environment env) {
     startThread(new CommandRunnable() {
@@ -138,9 +126,10 @@ public class ReviewCommand extends BaseCommand {
             writeError("error: " + e.getMessage() + "\n");
           } catch (Exception e) {
             ok = false;
-            writeError("fatal: internal server error while approving "
-                + patchSetId + "\n");
-            log.error("internal error while approving " + patchSetId);
+            String err = "internal server error while approving "
+                + patchSetId +"\n\t"+ e.getMessage() +"\n";
+            writeError("fatal: "+ err);
+            log.error(err);
           }
         }
         if (!ok) {
@@ -154,70 +143,25 @@ public class ReviewCommand extends BaseCommand {
   private void approveOne(final PatchSet.Id patchSetId)
       throws NoSuchChangeException, UnloggedFailure, OrmException,
       PatchSetInfoNotAvailableException, EmailException {
-    final Change.Id changeId = patchSetId.getParentKey();
-    final ChangeControl changeControl =
-        changeControlFactory.validateFor(changeId);
-    final Change change = changeControl.getChange();
-
-    final StringBuffer msgBuf = new StringBuffer();
-    msgBuf.append("Patch Set ");
-    msgBuf.append(patchSetId.get());
-    msgBuf.append(": ");
-
-    final Map<ApprovalCategory.Id, ApprovalCategoryValue.Id> approvalsMap =
-        new HashMap<ApprovalCategory.Id, ApprovalCategoryValue.Id>();
-
-    if (change.getStatus().isOpen()) {
-      for (ApproveOption co : optionList) {
-        final ApprovalCategory.Id category = co.getCategoryId();
-        PatchSetApproval.Key psaKey =
-            new PatchSetApproval.Key(patchSetId, currentUser.getAccountId(),
-                category);
-        PatchSetApproval psa = db.patchSetApprovals().get(psaKey);
-
-        Short score = co.value();
-
-        if (score != null) {
-          addApproval(psaKey, score, change, co);
-        } else {
-          if (psa == null) {
-            score = 0;
-            addApproval(psaKey, score, change, co);
-          } else {
-            score = psa.getValue();
-          }
-        }
-
-        final ApprovalCategoryValue.Id val =
-            new ApprovalCategoryValue.Id(category, score);
 
-        String message = db.approvalCategoryValues().get(val).getName();
-        msgBuf.append(" " + message + ";");
-        approvalsMap.put(category, val);
-      }
+    if(changeComment == null) {
+      changeComment = "";
     }
 
-    msgBuf.deleteCharAt(msgBuf.length() - 1);
-    msgBuf.append("\n\n");
-
-    if (changeComment != null) {
-      msgBuf.append(changeComment);
+    Set<ApprovalCategoryValue.Id> aps = new HashSet<ApprovalCategoryValue.Id>();
+    for (ApproveOption ao : optionList) {
+      Short v = ao.value();
+      if(v != null) {
+        aps.add(new ApprovalCategoryValue.Id(ao.getCategoryId(), v));
+      }
     }
 
-    String uuid = ChangeUtil.messageUUID(db);
-    ChangeMessage cm =
-        new ChangeMessage(new ChangeMessage.Key(changeId, uuid), currentUser
-            .getAccountId());
-    cm.setMessage(msgBuf.toString());
-    db.changeMessages().insert(Collections.singleton(cm));
-
-    ChangeUtil.touch(change, db);
-    sendMail(change, change.currentPatchSetId(), cm);
-
-    hooks.doCommentAddedHook(change, currentUser.getAccount(), db.patchSets()
-        .get(patchSetId), changeComment, approvalsMap);
+    publishCommentsFactory.create(patchSetId, changeComment, aps).call();
 
     if(submitChange) {
+      ChangeControl changeControl =
+        changeControlFactory.validateFor(patchSetId.getParentKey());
+
       String err = changeControl.canSubmit(patchSetId, db, approvalTypes,
           functionStateFactory);
       if(err != null) { throw error(err); }
@@ -290,36 +234,6 @@ public class ReviewCommand extends BaseCommand {
     return projectControl.getProject().getNameKey().equals(change.getProject());
   }
 
-  private void sendMail(final Change c, final PatchSet.Id psid,
-      final ChangeMessage message) throws PatchSetInfoNotAvailableException,
-      EmailException, OrmException {
-    PatchSet ps = db.patchSets().get(psid);
-    final CommentSender cm;
-    cm = commentSenderFactory.create(c);
-    cm.setFrom(currentUser.getAccountId());
-    cm.setPatchSet(ps, patchSetInfoFactory.get(psid));
-    cm.setChangeMessage(message);
-    cm.setReviewDb(db);
-    cm.send();
-  }
-
-  private void addApproval(final PatchSetApproval.Key psaKey,
-      final Short score, final Change c, final ApproveOption co)
-      throws OrmException, UnloggedFailure {
-    final PatchSetApproval psa = new PatchSetApproval(psaKey, score);
-    final List<PatchSetApproval> approvals = Collections.emptyList();
-    final FunctionState fs =
-        functionStateFactory.create(c, psaKey.getParentKey(), approvals);
-    psa.setValue(score);
-    fs.normalize(approvalTypes.getApprovalType(psa.getCategoryId()), psa);
-    if (score != psa.getValue()) {
-      throw error(co.name() + "=" + co.value() + " not permitted");
-    }
-
-    psa.setGranted();
-    db.patchSetApprovals().upsert(Collections.singleton(psa));
-  }
-
   private void initOptionList() {
     optionList = new ArrayList<ApproveOption>();
 
-- 
1.7.1

