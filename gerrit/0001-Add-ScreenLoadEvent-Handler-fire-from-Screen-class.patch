From 2a70bdfaa3192c46eae4dd6ff2adbbdaf4945614 Mon Sep 17 00:00:00 2001
From: Martin Fick <mfick@codeaurora.org>
Date: Tue, 4 Dec 2012 11:20:03 -0700
Subject: [PATCH] Add ScreenLoadEvent/Handler, fire from Screen class

The ScreenLoadEvent will fire every time a new Screen is loaded.
This can be used to update menus in a distributed fashion.  A
menu can listen for ScreenLoadEvents and adapt to the newly
loaded Screen.

Change-Id: I594f7e85ca4d1f8624899db466b9d1271dd62e37
---
 .../java/com/google/gerrit/client/ui/Screen.java   |    6 +++
 .../google/gerrit/client/ui/ScreenLoadEvent.java   |   42 ++++++++++++++++++++
 .../google/gerrit/client/ui/ScreenLoadHandler.java |   28 +++++++++++++
 3 files changed, 76 insertions(+), 0 deletions(-)
 create mode 100644 gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ScreenLoadEvent.java
 create mode 100644 gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ScreenLoadHandler.java

diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/Screen.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/Screen.java
index 47cc038..629722b 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/Screen.java
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/Screen.java
@@ -21,6 +21,7 @@ import com.google.gwt.user.client.ui.HasHorizontalAlignment;
 import com.google.gwt.user.client.ui.InlineLabel;
 import com.google.gwt.user.client.ui.Widget;
 import com.google.gwtexpui.user.client.View;
+import com.google.gwt.event.shared.HandlerRegistration;
 
 /**
   *  A Screen layout with a header and a body.
@@ -164,7 +165,12 @@ public abstract class Screen extends View {
       Gerrit.setWindowTitle(this, windowTitle);
     }
     Gerrit.updateMenus(this);
+    Gerrit.EVENT_BUS.fireEvent(new ScreenLoadEvent(this));
     Gerrit.setQueryString(null);
     registerKeys();
   }
+
+  public static HandlerRegistration addScreenLoadHandler(ScreenLoadHandler handler) {
+    return Gerrit.EVENT_BUS.addHandler(ScreenLoadEvent.TYPE, handler);
+  }
 }
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ScreenLoadEvent.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ScreenLoadEvent.java
new file mode 100644
index 0000000..562e53a
--- /dev/null
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ScreenLoadEvent.java
@@ -0,0 +1,42 @@
+// Copyright (C) 2012 The Android Open Source Project
+//
+// Licensed under the Apache License, Version 2.0 (the "License");
+// you may not use this file except in compliance with the License.
+// You may obtain a copy of the License at
+//
+// http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software
+// distributed under the License is distributed on an "AS IS" BASIS,
+// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+// See the License for the specific language governing permissions and
+// limitations under the License.
+
+package com.google.gerrit.client.ui;
+
+import com.google.gwt.event.shared.GwtEvent;
+
+public class ScreenLoadEvent extends GwtEvent<ScreenLoadHandler> {
+  private final Screen screen;
+
+  public ScreenLoadEvent(Screen screen) {
+    super();
+    this.screen = screen;
+  }
+
+  public static final Type<ScreenLoadHandler> TYPE = new Type<ScreenLoadHandler>();
+
+  @Override
+  protected void dispatch(ScreenLoadHandler handler) {
+    handler.onScreenLoad(this);
+  }
+
+  @Override
+  public GwtEvent.Type<ScreenLoadHandler> getAssociatedType() {
+    return TYPE;
+  }
+
+  public Screen getScreen(){
+    return screen;
+  }
+}
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ScreenLoadHandler.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ScreenLoadHandler.java
new file mode 100644
index 0000000..00724ce
--- /dev/null
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ScreenLoadHandler.java
@@ -0,0 +1,28 @@
+// Copyright (C) 2012 The Android Open Source Project
+//
+// Licensed under the Apache License, Version 2.0 (the "License");
+// you may not use this file except in compliance with the License.
+// You may obtain a copy of the License at
+//
+// http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software
+// distributed under the License is distributed on an "AS IS" BASIS,
+// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+// See the License for the specific language governing permissions and
+// limitations under the License.
+
+package com.google.gerrit.client.ui;
+
+import com.google.gwt.event.shared.EventHandler;
+import com.google.gwt.event.shared.HandlerRegistration;
+
+public interface ScreenLoadHandler extends EventHandler {
+  public interface HasScreenLoadEvents {
+    public HandlerRegistration addScreenLoadHandler(ScreenLoadHandler handler);
+  }
+
+  public void onScreenLoad(ScreenLoadEvent event);
+}
+
+
-- 
1.7.8

