From 06f73b8471d6039bcb0550f90538266f6b34f499 Mon Sep 17 00:00:00 2001
From: Martin Fick <mfick@codeaurora.org>
Date: Fri, 6 Jan 2012 13:58:42 -0700
Subject: [PATCH] Create and use a StarCache in the ChangeScreen

A StarCache keeps track of the starred state of a
Change and can inform its listeners when it changes.
It is also able to create star Images and KeyCommands
which already know how to update the StarCache, the
Images will also update themselves to reflect the
proper starred state of the Change they are bound to.

Use the StarCache in the ChangeScreen instead of
creating the star Image and KeyCommand and getting
and updating the starred value, and updating the star
Image.

Change-Id: I3f47fb1153674c6a172bf3319ad2d39df3762138
---
 .../google/gerrit/client/changes/ChangeCache.java  |    8 ++
 .../google/gerrit/client/changes/ChangeScreen.java |   57 +--------
 .../google/gerrit/client/changes/StarCache.java    |  130 ++++++++++++++++++++
 3 files changed, 143 insertions(+), 52 deletions(-)
 create mode 100644 gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/StarCache.java

diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/ChangeCache.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/ChangeCache.java
index 42e9a6b..5852eab 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/ChangeCache.java
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/ChangeCache.java
@@ -35,6 +35,7 @@ public class ChangeCache {
 
   private Change.Id changeId;
   private ChangeDetailCache detail;
+  private StarCache starred;
 
   protected ChangeCache(Change.Id chg) {
     changeId = chg;
@@ -50,4 +51,11 @@ public class ChangeCache {
     }
     return detail;
   }
+
+  public StarCache getStarCache() {
+    if (starred == null) {
+      starred = new StarCache(changeId);
+    }
+    return starred;
+  }
 }
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/ChangeScreen.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/ChangeScreen.java
index fbc949d..2c0e7af 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/ChangeScreen.java
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/ChangeScreen.java
@@ -66,9 +66,9 @@ public class ChangeScreen extends Screen
   private final Change.Id changeId;
   private final PatchSet.Id openPatchSetId;
   private ChangeDetailCache detailCache;
+  private StarCache starred;
 
   private Image starChange;
-  private boolean starred;
   private ChangeDescriptionBlock descriptionBlock;
   private ApprovalTable approvals;
 
@@ -160,15 +160,6 @@ public class ChangeScreen extends Screen
     }
   }
 
-  private void setStarred(final boolean s) {
-    if (s) {
-      starChange.setResource(Gerrit.RESOURCES.starFilled());
-    } else {
-      starChange.setResource(Gerrit.RESOURCES.starOpen());
-    }
-    starred = s;
-  }
-
   @Override
   protected void onInitUI() {
     super.onInitUI();
@@ -178,6 +169,8 @@ public class ChangeScreen extends Screen
     detailCache = cache.getChangeDetailCache();
     detailCache.addValueChangeHandler(this);
 
+    starred = cache.getStarCache();
+
     addStyleName(Gerrit.RESOURCES.css().changeScreen());
 
     keysNavigation = new KeyCommandSet(Gerrit.C.sectionNavigation());
@@ -186,19 +179,12 @@ public class ChangeScreen extends Screen
     keysNavigation.add(new ExpandCollapseDependencySectionKeyCommand(0, 'd', Util.C.expandCollapseDependencies()));
 
     if (Gerrit.isSignedIn()) {
-      keysAction.add(new StarKeyCommand(0, 's', Util.C.changeTableStar()));
+      keysAction.add(starred.new KeyCommand(0, 's', Util.C.changeTableStar()));
       keysAction.add(new PublishCommentsKeyCommand(0, 'r', Util.C
           .keyPublishComments()));
 
-      starChange = new Image(Gerrit.RESOURCES.starOpen());
+      starChange = starred.createStar();
       starChange.setStyleName(Gerrit.RESOURCES.css().changeScreenStarIcon());
-      starChange.setVisible(Gerrit.isSignedIn());
-      starChange.addClickHandler(new ClickHandler() {
-        @Override
-        public void onClick(final ClickEvent event) {
-          toggleStar();
-        }
-      });
       setTitleWest(starChange);
     }
 
@@ -289,10 +275,6 @@ public class ChangeScreen extends Screen
   private void display(final ChangeDetail detail) {
     displayTitle(detail.getChange().getKey(), detail.getChange().getSubject());
 
-    if (starChange != null) {
-      setStarred(detail.isStarred());
-    }
-
     if (Status.MERGED == detail.getChange().getStatus()) {
       includedInPanel.setVisible(true);
       includedInPanel.addOpenHandler(includedInTable);
@@ -419,24 +401,6 @@ public class ChangeScreen extends Screen
     return menuBar;
   }
 
-  private void toggleStar() {
-    final boolean prior = starred;
-    setStarred(!prior);
-
-    final ToggleStarRequest req = new ToggleStarRequest();
-    req.toggle(changeId, starred);
-    Util.LIST_SVC.toggleStars(req, new GerritCallback<VoidResult>() {
-      public void onSuccess(final VoidResult result) {
-      }
-
-      @Override
-      public void onFailure(final Throwable caught) {
-        super.onFailure(caught);
-        setStarred(prior);
-      }
-    });
-  }
-
   public class UpToListKeyCommand extends KeyCommand {
     public UpToListKeyCommand(int mask, char key, String help) {
       super(mask, key, help);
@@ -459,17 +423,6 @@ public class ChangeScreen extends Screen
     }
   }
 
-  public class StarKeyCommand extends NeedsSignInKeyCommand {
-    public StarKeyCommand(int mask, char key, String help) {
-      super(mask, key, help);
-    }
-
-    @Override
-    public void onKeyPress(final KeyPressEvent event) {
-      toggleStar();
-    }
-  }
-
   public class PublishCommentsKeyCommand extends NeedsSignInKeyCommand {
     public PublishCommentsKeyCommand(int mask, char key, String help) {
       super(mask, key, help);
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/StarCache.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/StarCache.java
new file mode 100644
index 0000000..859261a
--- /dev/null
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/StarCache.java
@@ -0,0 +1,130 @@
+// Copyright (C) 2012 The Android Open Source Project
+//
+// Licensed under the Apache License, Version 2.0 (the "License");
+// you may not use this file except in compliance with the License.
+// You may obtain a copy of the License at
+//
+// http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software
+// distributed under the License is distributed on an "AS IS" BASIS,
+// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+// See the License for the specific language governing permissions and
+// limitations under the License.
+
+package com.google.gerrit.client.changes;
+
+import com.google.gerrit.client.Gerrit;
+import com.google.gerrit.client.rpc.GerritCallback;
+import com.google.gerrit.client.ui.ListenableValue;
+import com.google.gerrit.client.ui.NeedsSignInKeyCommand;
+import com.google.gerrit.common.data.ChangeDetail;
+import com.google.gerrit.common.data.ChangeInfo;
+import com.google.gerrit.common.data.ToggleStarRequest;
+import com.google.gerrit.reviewdb.Change;
+
+import com.google.gwt.event.dom.client.ClickEvent;
+import com.google.gwt.event.dom.client.ClickHandler;
+import com.google.gwt.event.dom.client.KeyPressEvent;
+import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
+import com.google.gwt.event.logical.shared.ValueChangeEvent;
+import com.google.gwt.event.logical.shared.ValueChangeHandler;
+import com.google.gwt.event.shared.GwtEvent;
+import com.google.gwt.event.shared.HandlerManager;
+import com.google.gwt.event.shared.HandlerRegistration;
+import com.google.gwt.resources.client.ImageResource;
+import com.google.gwt.user.client.ui.Image;
+import com.google.gwtjsonrpc.client.VoidResult;
+
+public class StarCache implements HasValueChangeHandlers<Boolean> {
+  public class KeyCommand extends NeedsSignInKeyCommand {
+    public KeyCommand(int mask, char key, String help) {
+      super(mask, key, help);
+    }
+
+    @Override
+    public void onKeyPress(final KeyPressEvent event) {
+      StarCache.this.toggleStar();
+    }
+  }
+
+  ChangeCache cache;
+
+  private HandlerManager manager = new HandlerManager(this);
+
+  public StarCache(final Change.Id chg) {
+    cache = ChangeCache.get(chg);
+  }
+
+  public boolean get() {
+    ChangeDetail detail = cache.getChangeDetailCache().get();
+    if (detail != null) {
+      return detail.isStarred();
+    }
+    return false;
+  }
+
+  public void set(final boolean s) {
+    if (Gerrit.isSignedIn() && s != get()) {
+      final ToggleStarRequest req = new ToggleStarRequest();
+      req.toggle(cache.getChangeId(), s);
+
+      com.google.gerrit.client.changes.Util.LIST_SVC.toggleStars(req, new GerritCallback<VoidResult>() {
+        public void onSuccess(final VoidResult result) {
+          setStarred(s);
+          fireEvent(new ValueChangeEvent<Boolean>(s){});
+        }
+      });
+    }
+  }
+
+  private void setStarred(final boolean s) {
+    ChangeDetail detail = cache.getChangeDetailCache().get();
+    if (detail != null) {
+      detail.setStarred(s);
+    }
+  }
+
+
+  public void toggleStar() {
+    set(!get());
+  }
+
+  public Image createStar() {
+    final Image star = new Image(getResource());
+    star.setVisible(Gerrit.isSignedIn());
+
+    star.addClickHandler(new ClickHandler() {
+      @Override
+      public void onClick(final ClickEvent event) {
+        StarCache.this.toggleStar();
+      }
+    });
+
+    ValueChangeHandler starUpdater = new ValueChangeHandler() {
+        @Override
+        public void onValueChange(ValueChangeEvent event) {
+          star.setResource(StarCache.this.getResource());
+        }
+      };
+
+    cache.getChangeDetailCache().addValueChangeHandler(starUpdater);
+
+    this.addValueChangeHandler(starUpdater);
+
+    return star;
+  }
+
+  private ImageResource getResource() {
+    return get() ? Gerrit.RESOURCES.starFilled() : Gerrit.RESOURCES.starOpen();
+  }
+
+  public void fireEvent(GwtEvent<?> event) {
+    manager.fireEvent(event);
+  }
+
+  public HandlerRegistration addValueChangeHandler(
+      ValueChangeHandler<Boolean> handler) {
+    return manager.addHandler(ValueChangeEvent.getType(), handler);
+  }
+}
-- 
1.7.8

