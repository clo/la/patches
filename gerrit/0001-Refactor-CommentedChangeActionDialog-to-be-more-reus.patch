From 3200c9a6d53c95b806cc19c7c7242b028551ff38 Mon Sep 17 00:00:00 2001
From: Martin Fick <mfick@codeaurora.org>
Date: Mon, 9 Jan 2012 14:17:56 -0700
Subject: [PATCH] Refactor CommentedChangeActionDialog to be more reusable

This dialog was refactored out originally to be used
by the revert command, then it got used by the restore
command.  The dialog became a bit messy with way too
many arguments to the constructor and some fields which
were only used once.

This change reduces some of the dialog's functionality
in favor of allowing differentiation to be done via
extending the class instead.  This change also chooses
a few more common defaults for the dialog making it a
bit less cumbersome to setup.  And finally, since the
dialog is a bit more generic and no longer tied to a
change, the file is moved from the change package to the
ui package.

Change-Id: Ic6deabadf4aa983c6632517168fead93623e66f7
---
 .../java/com/google/gerrit/client/GerritCss.java   |    6 +-
 .../gerrit/client/changes/ChangeConstants.java     |    3 -
 .../client/changes/ChangeConstants.properties      |    3 -
 .../changes/CommentedChangeActionDialog.java       |  158 --------------------
 .../changes/PatchSetComplexDisclosurePanel.java    |   92 +++++++-----
 .../main/java/com/google/gerrit/client/gerrit.css  |   41 +-----
 .../gerrit/client/ui/CommentedActionDialog.java    |  142 ++++++++++++++++++
 .../google/gerrit/client/ui/ProjectConstants.java  |   25 ---
 .../gerrit/client/ui/ProjectConstants.properties   |    5 -
 .../com/google/gerrit/client/ui/UIConstants.java   |   28 ++++
 .../google/gerrit/client/ui/UIConstants.properties |    8 +
 .../java/com/google/gerrit/client/ui/Util.java     |    2 +-
 12 files changed, 241 insertions(+), 272 deletions(-)
 delete mode 100644 gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/CommentedChangeActionDialog.java
 create mode 100644 gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/CommentedActionDialog.java
 delete mode 100644 gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ProjectConstants.java
 delete mode 100644 gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ProjectConstants.properties
 create mode 100644 gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/UIConstants.java
 create mode 100644 gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/UIConstants.properties

diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/GerritCss.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/GerritCss.java
index 70cfdbc..5801c8e 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/GerritCss.java
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/GerritCss.java
@@ -18,10 +18,6 @@ import com.google.gwt.resources.client.CssResource;
 
 public interface GerritCss extends CssResource {
   String greenCheckClass();
-  String abandonChangeDialog();
-  String abandonMessage();
-  String revertChangeDialog();
-  String revertMessage();
   String accountContactOnFile();
   String accountContactPrivacyDetails();
   String accountDashboard();
@@ -58,6 +54,8 @@ public interface GerritCss extends CssResource {
   String changeTypeCell();
   String changeid();
   String closedstate();
+  String commentedActionDialog();
+  String commentedActionMessage();
   String commentCell();
   String commentEditorPanel();
   String commentHolder();
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/ChangeConstants.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/ChangeConstants.java
index 3cddd2b..70697e4 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/ChangeConstants.java
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/ChangeConstants.java
@@ -113,13 +113,11 @@ public interface ChangeConstants extends Constants {
 
   String buttonRevertChangeBegin();
   String buttonRevertChangeSend();
-  String buttonRevertChangeCancel();
   String headingRevertMessage();
   String revertChangeTitle();
 
   String buttonAbandonChangeBegin();
   String buttonAbandonChangeSend();
-  String buttonAbandonChangeCancel();
   String headingAbandonMessage();
   String abandonChangeTitle();
   String oldVersionHistory();
@@ -134,7 +132,6 @@ public interface ChangeConstants extends Constants {
 
   String buttonRestoreChangeBegin();
   String restoreChangeTitle();
-  String buttonRestoreChangeCancel();
   String headingRestoreMessage();
   String buttonRestoreChangeSend();
 
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/ChangeConstants.properties b/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/ChangeConstants.properties
index 35991f4..c564ec2 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/ChangeConstants.properties
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/ChangeConstants.properties
@@ -90,7 +90,6 @@ initialCommit = Initial Commit
 
 buttonAbandonChangeBegin = Abandon Change
 buttonAbandonChangeSend = Abandon Change
-buttonAbandonChangeCancel = Cancel
 headingAbandonMessage = Abandon Message:
 abandonChangeTitle = Code Review - Abandon Change
 oldVersionHistory = Old Version History:
@@ -98,13 +97,11 @@ baseDiffItem = Base
 
 buttonRevertChangeBegin = Revert Change
 buttonRevertChangeSend = Revert Change
-buttonRevertChangeCancel = Cancel
 headingRevertMessage = Revert Commit Message:
 revertChangeTitle = Code Review - Revert Merged Change
 
 buttonRestoreChangeBegin = Restore Change
 restoreChangeTitle = Code Review - Restore Change
-buttonRestoreChangeCancel = Cancel
 headingRestoreMessage = Restore Message:
 buttonRestoreChangeSend = Restore Change
 
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/CommentedChangeActionDialog.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/CommentedChangeActionDialog.java
deleted file mode 100644
index 01d6299..0000000
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/CommentedChangeActionDialog.java
+++ /dev/null
@@ -1,158 +0,0 @@
-// Copyright (C) 2009 The Android Open Source Project
-//
-// Licensed under the Apache License, Version 2.0 (the "License");
-// you may not use this file except in compliance with the License.
-// You may obtain a copy of the License at
-//
-// http://www.apache.org/licenses/LICENSE-2.0
-//
-// Unless required by applicable law or agreed to in writing, software
-// distributed under the License is distributed on an "AS IS" BASIS,
-// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-// See the License for the specific language governing permissions and
-// limitations under the License.
-
-package com.google.gerrit.client.changes;
-
-import com.google.gerrit.client.rpc.GerritCallback;
-import com.google.gerrit.client.ui.SmallHeading;
-import com.google.gerrit.common.data.ChangeDetail;
-import com.google.gerrit.reviewdb.PatchSet;
-import com.google.gwt.event.dom.client.ClickEvent;
-import com.google.gwt.event.dom.client.ClickHandler;
-import com.google.gwt.event.logical.shared.CloseEvent;
-import com.google.gwt.event.logical.shared.CloseHandler;
-import com.google.gwt.user.client.DOM;
-import com.google.gwt.user.client.rpc.AsyncCallback;
-import com.google.gwt.user.client.ui.Button;
-import com.google.gwt.user.client.ui.FlowPanel;
-import com.google.gwt.user.client.ui.PopupPanel;
-import com.google.gwtexpui.globalkey.client.GlobalKey;
-import com.google.gwtexpui.globalkey.client.NpTextArea;
-import com.google.gwtexpui.user.client.AutoCenterDialogBox;
-
-public abstract class CommentedChangeActionDialog extends AutoCenterDialogBox implements CloseHandler<PopupPanel>{
-  private final FlowPanel panel;
-  private final NpTextArea message;
-  private final Button sendButton;
-  private final Button cancelButton;
-  private final PatchSet.Id psid;
-  private final AsyncCallback<ChangeDetail> callback;
-
-  private boolean buttonClicked = false;
-
-  public CommentedChangeActionDialog(final PatchSet.Id psi,
-      final AsyncCallback<ChangeDetail> callback, final String dialogTitle,
-      final String dialogHeading, final String buttonSend,
-      final String buttonCancel, final String dialogStyle,
-      final String messageStyle) {
-     this(psi, callback, dialogTitle, dialogHeading, buttonSend, buttonCancel, dialogStyle, messageStyle, null);
-  }
-
-  public CommentedChangeActionDialog(final PatchSet.Id psi,
-      final AsyncCallback<ChangeDetail> callback, final String dialogTitle,
-      final String dialogHeading, final String buttonSend,
-      final String buttonCancel, final String dialogStyle,
-      final String messageStyle, final String defaultMessage) {
-    super(/* auto hide */false, /* modal */true);
-    setGlassEnabled(true);
-
-    psid = psi;
-    this.callback = callback;
-    addStyleName(dialogStyle);
-    setText(dialogTitle);
-
-    panel = new FlowPanel();
-    add(panel);
-
-    panel.add(new SmallHeading(dialogHeading));
-
-    final FlowPanel mwrap = new FlowPanel();
-    mwrap.setStyleName(messageStyle);
-    panel.add(mwrap);
-
-    message = new NpTextArea();
-    message.setCharacterWidth(60);
-    message.setVisibleLines(10);
-    message.setText(defaultMessage);
-    DOM.setElementPropertyBoolean(message.getElement(), "spellcheck", true);
-    mwrap.add(message);
-
-    final FlowPanel buttonPanel = new FlowPanel();
-    panel.add(buttonPanel);
-    sendButton = new Button(buttonSend);
-    sendButton.addClickHandler(new ClickHandler() {
-      @Override
-      public void onClick(final ClickEvent event) {
-        sendButton.setEnabled(false);
-        cancelButton.setEnabled(false);
-        onSend();
-      }
-    });
-    buttonPanel.add(sendButton);
-
-    cancelButton = new Button(buttonCancel);
-    DOM.setStyleAttribute(cancelButton.getElement(), "marginLeft", "300px");
-    cancelButton.addClickHandler(new ClickHandler() {
-      @Override
-      public void onClick(final ClickEvent event) {
-        buttonClicked = true;
-        if (callback != null) {
-          callback.onFailure(null);
-        }
-        hide();
-      }
-    });
-    buttonPanel.add(cancelButton);
-
-    addCloseHandler(this);
-  }
-
-  @Override
-  public void center() {
-    super.center();
-    GlobalKey.dialog(this);
-    message.setFocus(true);
-  }
-
-  @Override
-  public void onClose(CloseEvent<PopupPanel> event) {
-    if (!buttonClicked) {
-      // the dialog was closed without one of the buttons being pressed
-      // e.g. the user pressed ESC to close the dialog
-      if (callback != null) {
-        callback.onFailure(null);
-      }
-    }
-  }
-
-  public abstract void onSend();
-
-  public PatchSet.Id getPatchSetId() {
-    return psid;
-  }
-
-  public String getMessageText() {
-    return message.getText().trim();
-  }
-
-  public GerritCallback<ChangeDetail> createCallback() {
-    return new GerritCallback<ChangeDetail>(){
-      @Override
-      public void onSuccess(ChangeDetail result) {
-        buttonClicked = true;
-        if (callback != null) {
-          callback.onSuccess(result);
-        }
-        hide();
-      }
-
-      @Override
-      public void onFailure(Throwable caught) {
-        sendButton.setEnabled(true);
-        cancelButton.setEnabled(true);
-        super.onFailure(caught);
-      }
-    };
-  }
-}
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/PatchSetComplexDisclosurePanel.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/PatchSetComplexDisclosurePanel.java
index d3c3f76..4c30336 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/PatchSetComplexDisclosurePanel.java
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/PatchSetComplexDisclosurePanel.java
@@ -20,6 +20,7 @@ import com.google.gerrit.client.Gerrit;
 import com.google.gerrit.client.patches.PatchUtil;
 import com.google.gerrit.client.rpc.GerritCallback;
 import com.google.gerrit.client.ui.AccountDashboardLink;
+import com.google.gerrit.client.ui.CommentedActionDialog;
 import com.google.gerrit.client.ui.ComplexDisclosurePanel;
 import com.google.gerrit.client.ui.ListenableAccountDiffPreference;
 import com.google.gerrit.common.PageLinks;
@@ -49,6 +50,7 @@ import com.google.gwt.user.client.ui.Anchor;
 import com.google.gwt.user.client.ui.Button;
 import com.google.gwt.user.client.ui.DisclosurePanel;
 import com.google.gwt.user.client.ui.FlowPanel;
+import com.google.gwt.user.client.ui.FocusWidget;
 import com.google.gwt.user.client.ui.Grid;
 import com.google.gwt.user.client.ui.HTMLTable.CellFormatter;
 import com.google.gwt.user.client.ui.InlineLabel;
@@ -468,15 +470,20 @@ class PatchSetComplexDisclosurePanel extends ComplexDisclosurePanel implements O
         @Override
         public void onClick(final ClickEvent event) {
           b.setEnabled(false);
-          new CommentedChangeActionDialog(patchSet.getId(), createCommentedCallback(b, true),
-              Util.C.revertChangeTitle(), Util.C.headingRevertMessage(),
-              Util.C.buttonRevertChangeSend(), Util.C.buttonRevertChangeCancel(),
-              Gerrit.RESOURCES.css().revertChangeDialog(), Gerrit.RESOURCES.css().revertMessage(),
-              Util.M.revertChangeDefaultMessage(detail.getInfo().getSubject(), detail.getPatchSet().getRevision().get())) {
-                public void onSend() {
-                  Util.MANAGE_SVC.revertChange(getPatchSetId() , getMessageText(), createCallback());
-                }
-              }.center();
+          new ActionDialog(b, true, Util.C.revertChangeTitle(),
+              Util.C.headingRevertMessage()) {
+            {
+              sendButton.setText(Util.C.buttonRevertChangeSend());
+              message.setText(Util.M.revertChangeDefaultMessage(
+                  detail.getInfo().getSubject(),
+                  detail.getPatchSet().getRevision().get())
+              );
+            }
+            public void onSend() {
+              Util.MANAGE_SVC.revertChange(patchSet.getId(), getMessageText(),
+                 createCallback());
+            }
+          }.center();
         }
       });
       actionsPanel.add(b);
@@ -488,14 +495,16 @@ class PatchSetComplexDisclosurePanel extends ComplexDisclosurePanel implements O
         @Override
         public void onClick(final ClickEvent event) {
           b.setEnabled(false);
-          new CommentedChangeActionDialog(patchSet.getId(), createCommentedCallback(b, false),
-              Util.C.abandonChangeTitle(), Util.C.headingAbandonMessage(),
-              Util.C.buttonAbandonChangeSend(), Util.C.buttonAbandonChangeCancel(),
-              Gerrit.RESOURCES.css().abandonChangeDialog(), Gerrit.RESOURCES.css().abandonMessage()) {
-                public void onSend() {
-                  Util.MANAGE_SVC.abandonChange(getPatchSetId() , getMessageText(), createCallback());
-                }
-              }.center();
+          new ActionDialog(b, false, Util.C.abandonChangeTitle(),
+              Util.C.headingAbandonMessage()) {
+            {
+              sendButton.setText(Util.C.buttonAbandonChangeSend());
+            }
+            public void onSend() {
+              Util.MANAGE_SVC.abandonChange(patchSet.getId(), getMessageText(),
+                  createCallback());
+            }
+          }.center();
         }
       });
       actionsPanel.add(b);
@@ -531,14 +540,16 @@ class PatchSetComplexDisclosurePanel extends ComplexDisclosurePanel implements O
         @Override
         public void onClick(final ClickEvent event) {
           b.setEnabled(false);
-          new CommentedChangeActionDialog(patchSet.getId(), createCommentedCallback(b, false),
-              Util.C.restoreChangeTitle(), Util.C.headingRestoreMessage(),
-              Util.C.buttonRestoreChangeSend(), Util.C.buttonRestoreChangeCancel(),
-              Gerrit.RESOURCES.css().abandonChangeDialog(), Gerrit.RESOURCES.css().abandonMessage()) {
-                public void onSend() {
-                  Util.MANAGE_SVC.restoreChange(getPatchSetId(), getMessageText(), createCallback());
-                }
-              }.center();
+          new ActionDialog(b, false, Util.C.restoreChangeTitle(),
+              Util.C.headingRestoreMessage()) {
+            {
+              sendButton.setText(Util.C.buttonRestoreChangeSend());
+            }
+            public void onSend() {
+              Util.MANAGE_SVC.restoreChange(patchSet.getId(), getMessageText(),
+                  createCallback());
+            }
+          }.center();
         }
       });
       actionsPanel.add(b);
@@ -739,19 +750,24 @@ class PatchSetComplexDisclosurePanel extends ComplexDisclosurePanel implements O
     }
   }
 
-  private AsyncCallback<ChangeDetail> createCommentedCallback(final Button b, final boolean redirect) {
-    return new AsyncCallback<ChangeDetail>() {
-      public void onSuccess(ChangeDetail result) {
-        if (redirect) {
-          Gerrit.display(PageLinks.toChange(result.getChange().getId()));
-        } else {
-          changeScreen.update(result);
-        }
-      }
+  public abstract class ActionDialog extends CommentedActionDialog<ChangeDetail> {
+    public ActionDialog(final FocusWidget enableOnFailure, final boolean redirect,
+        String dialogTitle, String dialogHeading) {
+      super(dialogTitle, dialogHeading, new AsyncCallback<ChangeDetail>() {
+          @Override
+          public void onSuccess(ChangeDetail result) {
+            if (redirect) {
+              Gerrit.display(PageLinks.toChange(result.getChange().getId()));
+            } else {
+              changeScreen.update(result);
+            }
+          }
 
-      public void onFailure(Throwable caught) {
-        b.setEnabled(true);
-      }
-    };
+          @Override
+          public void onFailure(Throwable caught) {
+            enableOnFailure.setEnabled(true);
+          }
+        });
+    }
   }
 }
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/gerrit.css b/gerrit-gwtui/src/main/java/com/google/gerrit/client/gerrit.css
index 20837c4..3510820 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/gerrit.css
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/gerrit.css
@@ -1209,7 +1209,6 @@ a:hover.downloadLink {
 
 
 /** PublishCommentsScreen **/
-
 .publishCommentsScreen .smallHeading {
   font-size: small;
   font-weight: bold;
@@ -1259,57 +1258,29 @@ a:hover.downloadLink {
 }
 
 
-/** AbandonChangeDialog **/
-
-.abandonChangeDialog .gwt-DisclosurePanel .header td {
+/** CommentedActionDialog **/
+.commentedActionDialog .gwt-DisclosurePanel .header td {
   font-weight: bold;
   white-space: nowrap;
 }
-
-.abandonChangeDialog .smallHeading {
+.commentedActionDialog .smallHeading {
   font-size: small;
   font-weight: bold;
   white-space: nowrap;
 }
-.abandonChangeDialog .abandonMessage {
+.commentedActionDialog .commentedActionMessage {
   margin-left: 10px;
   background: trimColor;
   padding: 5px 5px 5px 5px;
 }
-.abandonChangeDialog .abandonMessage textarea {
+.commentedActionDialog .commentedActionMessage textarea {
   font-size: small;
 }
-.abandonChangeDialog .gwt-Hyperlink {
+.commentedActionDialog .gwt-Hyperlink {
   white-space: nowrap;
   font-size: small;
 }
 
-/** RevertChangeDialog **/
-
-.revertChangeDialog .gwt-DisclosurePanel .header td {
-  font-weight: bold;
-  white-space: nowrap;
-}
-
-.revertChangeDialog .smallHeading {
-  font-size: small;
-  font-weight: bold;
-  white-space: nowrap;
-}
-.revertChangeDialog .revertMessage {
-  margin-left: 10px;
-  background: trimColor;
-  padding: 5px 5px 5px 5px;
-}
-.revertChangeDialog .revertMessage textarea {
-  font-size: small;
-}
-.revertChangeDialog .gwt-Hyperlink {
-  white-space: nowrap;
-  font-size: small;
-}
-
-
 /** PatchBrowserPopup **/
 .patchBrowserPopup {
   opacity: 0.90;
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/CommentedActionDialog.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/CommentedActionDialog.java
new file mode 100644
index 0000000..3842653
--- /dev/null
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/CommentedActionDialog.java
@@ -0,0 +1,142 @@
+// Copyright (C) 2009 The Android Open Source Project
+//
+// Licensed under the Apache License, Version 2.0 (the "License");
+// you may not use this file except in compliance with the License.
+// You may obtain a copy of the License at
+//
+// http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software
+// distributed under the License is distributed on an "AS IS" BASIS,
+// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+// See the License for the specific language governing permissions and
+// limitations under the License.
+
+package com.google.gerrit.client.ui;
+
+import com.google.gerrit.client.Gerrit;
+import com.google.gerrit.client.rpc.GerritCallback;
+import com.google.gwt.event.dom.client.ClickEvent;
+import com.google.gwt.event.dom.client.ClickHandler;
+import com.google.gwt.event.logical.shared.CloseEvent;
+import com.google.gwt.event.logical.shared.CloseHandler;
+import com.google.gwt.user.client.DOM;
+import com.google.gwt.user.client.rpc.AsyncCallback;
+import com.google.gwt.user.client.ui.Button;
+import com.google.gwt.user.client.ui.FlowPanel;
+import com.google.gwt.user.client.ui.PopupPanel;
+import com.google.gwtexpui.globalkey.client.GlobalKey;
+import com.google.gwtexpui.globalkey.client.NpTextArea;
+import com.google.gwtexpui.user.client.AutoCenterDialogBox;
+
+public abstract class CommentedActionDialog<T> extends AutoCenterDialogBox implements CloseHandler<PopupPanel>{
+  protected final FlowPanel panel;
+  protected final NpTextArea message;
+  protected final Button sendButton;
+  protected final Button cancelButton;
+  protected final FlowPanel buttonPanel;
+  protected AsyncCallback<T> callback;
+
+  protected boolean sent = false;
+
+  public CommentedActionDialog(final String title, final String heading,
+      AsyncCallback<T> callback) {
+    super(/* auto hide */false, /* modal */true);
+    this.callback = callback;
+
+    setGlassEnabled(true);
+    setText(title);
+
+    addStyleName(Gerrit.RESOURCES.css().commentedActionDialog());
+
+    message = new NpTextArea();
+    message.setCharacterWidth(60);
+    message.setVisibleLines(10);
+    DOM.setElementPropertyBoolean(message.getElement(), "spellcheck", true);
+
+    sendButton = new Button(Util.C.commentedActionButtonSend());
+    sendButton.addClickHandler(new ClickHandler() {
+      @Override
+      public void onClick(final ClickEvent event) {
+        enableButtons(false);
+        onSend();
+      }
+    });
+
+    cancelButton = new Button(Util.C.commentedActionButtonCancel());
+    DOM.setStyleAttribute(cancelButton.getElement(), "marginLeft", "300px");
+    cancelButton.addClickHandler(new ClickHandler() {
+      @Override
+      public void onClick(final ClickEvent event) {
+        hide();
+      }
+    });
+
+    final FlowPanel mwrap = new FlowPanel();
+    mwrap.setStyleName(Gerrit.RESOURCES.css().commentedActionMessage());
+    mwrap.add(message);
+
+    buttonPanel = new FlowPanel();
+    buttonPanel.add(sendButton);
+    buttonPanel.add(cancelButton);
+
+    panel = new FlowPanel();
+    panel.add(new SmallHeading(heading));
+    panel.add(mwrap);
+    panel.add(buttonPanel);
+    add(panel);
+
+    callback = createCallback();
+
+    addCloseHandler(this);
+  }
+
+  public void enableButtons(boolean enable) {
+    sendButton.setEnabled(enable);
+    cancelButton.setEnabled(enable);
+  }
+
+  @Override
+  public void center() {
+    super.center();
+    GlobalKey.dialog(this);
+    message.setFocus(true);
+  }
+
+  @Override
+  public void onClose(CloseEvent<PopupPanel> event) {
+    if (!sent) {
+      // the dialog was closed without the send button being pressed
+      // e.g. the user pressed Cancel or ESC to close the dialog
+      if (callback != null) {
+        callback.onFailure(null);
+      }
+    }
+    sent = false;
+  }
+
+  public abstract void onSend();
+
+  public String getMessageText() {
+    return message.getText().trim();
+  }
+
+  public AsyncCallback<T> createCallback() {
+    return new GerritCallback<T>(){
+      @Override
+      public void onSuccess(T result) {
+        sent = true;
+        if (callback != null) {
+          callback.onSuccess(result);
+        }
+        hide();
+      }
+
+      @Override
+      public void onFailure(Throwable caught) {
+        enableButtons(true);
+        super.onFailure(caught);
+      }
+    };
+  }
+}
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ProjectConstants.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ProjectConstants.java
deleted file mode 100644
index 8b13392..0000000
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ProjectConstants.java
+++ /dev/null
@@ -1,25 +0,0 @@
-// Copyright (C) 2010 The Android Open Source Project
-//
-// Licensed under the Apache License, Version 2.0 (the "License");
-// you may not use this file except in compliance with the License.
-// You may obtain a copy of the License at
-//
-// http://www.apache.org/licenses/LICENSE-2.0
-//
-// Unless required by applicable law or agreed to in writing, software
-// distributed under the License is distributed on an "AS IS" BASIS,
-// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-// See the License for the specific language governing permissions and
-// limitations under the License.
-
-package com.google.gerrit.client.ui;
-
-import com.google.gwt.i18n.client.Constants;
-
-public interface ProjectConstants extends Constants {
-  String projectName();
-  String projectDescription();
-  String projectListOpen();
-  String projectListPrev();
-  String projectListNext();
-}
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ProjectConstants.properties b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ProjectConstants.properties
deleted file mode 100644
index 15de117..0000000
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/ProjectConstants.properties
+++ /dev/null
@@ -1,5 +0,0 @@
-projectName = Project Name
-projectDescription = Project Description
-projectListOpen = Select project
-projectListPrev = Previous project
-projectListNext = Next project
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/UIConstants.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/UIConstants.java
new file mode 100644
index 0000000..ebbb049
--- /dev/null
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/UIConstants.java
@@ -0,0 +1,28 @@
+// Copyright (C) 2010 The Android Open Source Project
+//
+// Licensed under the Apache License, Version 2.0 (the "License");
+// you may not use this file except in compliance with the License.
+// You may obtain a copy of the License at
+//
+// http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software
+// distributed under the License is distributed on an "AS IS" BASIS,
+// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+// See the License for the specific language governing permissions and
+// limitations under the License.
+
+package com.google.gerrit.client.ui;
+
+import com.google.gwt.i18n.client.Constants;
+
+public interface UIConstants extends Constants {
+  String commentedActionButtonSend();
+  String commentedActionButtonCancel();
+
+  String projectName();
+  String projectDescription();
+  String projectListOpen();
+  String projectListPrev();
+  String projectListNext();
+}
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/UIConstants.properties b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/UIConstants.properties
new file mode 100644
index 0000000..aa00cee
--- /dev/null
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/UIConstants.properties
@@ -0,0 +1,8 @@
+commentedActionButtonSend = Submit
+commentedActionButtonCancel = Cancel
+
+projectName = Project Name
+projectDescription = Project Description
+projectListOpen = Select project
+projectListPrev = Previous project
+projectListNext = Next project
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/Util.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/Util.java
index ebed764..98f13ef 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/Util.java
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/ui/Util.java
@@ -17,5 +17,5 @@ package com.google.gerrit.client.ui;
 import com.google.gwt.core.client.GWT;
 
 public class Util {
-  public static final ProjectConstants C = GWT.create(ProjectConstants.class);
+  public static final UIConstants C = GWT.create(UIConstants.class);
 }
-- 
1.7.8

