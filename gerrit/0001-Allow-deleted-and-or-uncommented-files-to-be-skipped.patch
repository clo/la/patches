From 16e1db4c8995b40e79f744b57f01a624962798b2 Mon Sep 17 00:00:00 2001
From: Martin Fick <mfick@codeaurora.org>
Date: Mon, 16 Aug 2010 11:52:08 -0600
Subject: [PATCH] Allow deleted and/or uncommented files to be skipped in reviews

Add two checkboxes to the user diff preferences.  These
buttons enable/disable skipping deleted or uncommented files.
Skipping is with respect to the prev/next links above and
below the diff.

Bug: issue 584
Change-Id: I105ee3115f806f1637997e95e9e9931b4107a69e
---
 .../google/gerrit/client/changes/PatchTable.java   |   43 +++++++++++++++++---
 .../google/gerrit/client/patches/PatchScreen.java  |   14 ++++---
 .../client/patches/PatchScriptSettingsPanel.java   |   11 +++++
 .../client/patches/PatchScriptSettingsPanel.ui.xml |   18 ++++++++-
 .../gerrit/reviewdb/AccountDiffPreference.java     |   24 +++++++++++
 .../google/gerrit/server/schema/SchemaVersion.java |    2 +-
 .../com/google/gerrit/server/schema/Schema_42.java |   25 +++++++++++
 7 files changed, 123 insertions(+), 14 deletions(-)
 create mode 100644 gerrit-server/src/main/java/com/google/gerrit/server/schema/Schema_42.java

diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/PatchTable.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/PatchTable.java
index 34695dd..98c9282 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/PatchTable.java
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/changes/PatchTable.java
@@ -17,10 +17,12 @@ package com.google.gerrit.client.changes;
 import com.google.gerrit.client.Gerrit;
 import com.google.gerrit.client.patches.PatchScreen;
 import com.google.gerrit.client.ui.InlineHyperlink;
+import com.google.gerrit.client.ui.ListenableAccountDiffPreference;
 import com.google.gerrit.client.ui.NavigationTable;
 import com.google.gerrit.client.ui.PatchLink;
 import com.google.gerrit.common.data.PatchSetDetail;
 import com.google.gerrit.reviewdb.Patch;
+import com.google.gerrit.reviewdb.Patch.ChangeType;
 import com.google.gerrit.reviewdb.Patch.Key;
 import com.google.gerrit.reviewdb.Patch.PatchType;
 import com.google.gwt.core.client.GWT;
@@ -53,16 +55,22 @@ public class PatchTable extends Composite {
   private MyTable myTable;
   private String savePointerId;
   private List<Patch> patchList;
+  private ListenableAccountDiffPreference listenablePrefs;
 
   private List<ClickHandler> clickHandlers;
   private boolean active;
   private boolean registerKeys;
 
-  public PatchTable() {
+  public PatchTable(ListenableAccountDiffPreference prefs) {
+    listenablePrefs = prefs;
     myBody = new FlowPanel();
     initWidget(myBody);
   }
 
+  public PatchTable() {
+    this(new ListenableAccountDiffPreference());
+  }
+
   public int indexOf(Patch.Key patch) {
     for (int i = 0; i < patchList.size(); i++) {
       if (patchList.get(i).getKey().equals(patch)) {
@@ -166,9 +174,13 @@ public class PatchTable extends Composite {
    * @return a link to the previous file in this patch set, or null.
    */
   public InlineHyperlink getPreviousPatchLink(int index, PatchScreen.Type patchType) {
-    if (0 < index)
-      return createLink(index - 1, patchType, SafeHtml.asis(Util.C
+    for(index--; index > -1; index--) {
+      InlineHyperlink link = createLink(index, patchType, SafeHtml.asis(Util.C
           .prevPatchLinkIcon()), null);
+      if(link != null) {
+        return link;
+      }
+    }
     return null;
   }
 
@@ -176,9 +188,13 @@ public class PatchTable extends Composite {
    * @return a link to the next file in this patch set, or null.
    */
   public InlineHyperlink getNextPatchLink(int index, PatchScreen.Type patchType) {
-    if (index < patchList.size() - 1)
-      return createLink(index + 1, patchType, null, SafeHtml.asis(Util.C
+    for(index++; index < patchList.size(); index++) {
+      InlineHyperlink link = createLink(index, patchType, null, SafeHtml.asis(Util.C
           .nextPatchLinkIcon()));
+      if(link != null) {
+        return link;
+      }
+    }
     return null;
   }
 
@@ -192,6 +208,14 @@ public class PatchTable extends Composite {
   private PatchLink createLink(int index, PatchScreen.Type patchType,
       SafeHtml before, SafeHtml after) {
     Patch patch = patchList.get(index);
+    if (( listenablePrefs.get().isSkipDeleted() &&
+          patch.getChangeType().equals(ChangeType.DELETED) )
+        ||
+        ( listenablePrefs.get().isSkipUncommented() &&
+          patch.getCommentCount() == 0 ) ) {
+      return null;
+    }
+
     Key thisKey = patch.getKey();
     PatchLink link;
     if (patchType == PatchScreen.Type.SIDE_BY_SIDE
@@ -240,6 +264,14 @@ public class PatchTable extends Composite {
     }
   }
 
+  public ListenableAccountDiffPreference getPreferences() {
+    return listenablePrefs;
+  }
+
+  public void setPreferences(ListenableAccountDiffPreference prefs) {
+    listenablePrefs = prefs;
+  }
+
   private class MyTable extends NavigationTable<Patch> {
     private static final int C_PATH = 2;
     private static final int C_DRAFT = 3;
@@ -756,5 +788,4 @@ public class PatchTable extends Composite {
       return System.currentTimeMillis() - start > 200;
     }
   }
-
 }
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScreen.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScreen.java
index 789f1da..612b13b 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScreen.java
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScreen.java
@@ -135,6 +135,7 @@ public abstract class PatchScreen extends Screen implements
 
   /** The index of the file we are currently looking at among the fileList */
   private int patchIndex;
+  private ListenableAccountDiffPreference prefs;
 
   /** Keys that cause an action on this screen */
   private KeyCommandSet keysNavigation;
@@ -167,8 +168,7 @@ public abstract class PatchScreen extends Screen implements
     idSideB = diffSideB != null ? diffSideB : id.getParentKey();
     this.patchIndex = patchIndex;
 
-    ListenableAccountDiffPreference prefs =
-      new ListenableAccountDiffPreference();
+    prefs = fileList.getPreferences();
     prefs.addValueChangeHandler(new ValueChangeHandler<AccountDiffPreference>() {
           @Override
           public void onValueChange(ValueChangeEvent<AccountDiffPreference> event) {
@@ -328,11 +328,9 @@ public abstract class PatchScreen extends Screen implements
             public void onSuccess(PatchSetDetail result) {
               patchSetDetail = result;
               if (fileList == null) {
-                fileList = new PatchTable();
+                fileList = new PatchTable(prefs);
                 fileList.display(result);
                 patchIndex = fileList.indexOf(patchKey);
-                topNav.display(patchIndex, getPatchScreenType(), fileList);
-                bottomNav.display(patchIndex, getPatchScreenType(), fileList);
               }
               refresh(true);
             }
@@ -386,6 +384,7 @@ public abstract class PatchScreen extends Screen implements
   }
 
   private void onResult(final PatchScript script, final boolean isFirst) {
+
     final Change.Key cid = script.getChangeId();
     final String path = PatchTable.getDisplayFileName(patchKey);
     String fileName = path;
@@ -445,6 +444,9 @@ public abstract class PatchScreen extends Screen implements
     settingsPanel.setEnabled(true);
     lastScript = script;
 
+    topNav.display(patchIndex, getPatchScreenType(), fileList);
+    bottomNav.display(patchIndex, getPatchScreenType(), fileList);
+
     // Mark this file reviewed as soon we display the diff screen
     if (Gerrit.isSignedIn() && isFirst) {
       settingsPanel.getReviewedCheckBox().setValue(true);
@@ -477,7 +479,7 @@ public abstract class PatchScreen extends Screen implements
     public void onKeyPress(final KeyPressEvent event) {
       if (fileList == null || fileList.isAttached()) {
         final PatchSet.Id psid = patchKey.getParentKey();
-        fileList = new PatchTable();
+        fileList = new PatchTable(prefs);
         fileList.setSavePointerId("PatchTable " + psid);
         Util.DETAIL_SVC.patchSetDetail(psid,
             new GerritCallback<PatchSetDetail>() {
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScriptSettingsPanel.java b/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScriptSettingsPanel.java
index dade7f7..3550229 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScriptSettingsPanel.java
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScriptSettingsPanel.java
@@ -81,6 +81,13 @@ public class PatchScriptSettingsPanel extends Composite implements
   CheckBox reviewed;
 
   @UiField
+  CheckBox skipDeleted;
+
+  @UiField
+  CheckBox skipUncommented;
+
+
+  @UiField
   Button update;
 
   /**
@@ -199,6 +206,8 @@ public class PatchScriptSettingsPanel extends Composite implements
     intralineDifference.setValue(dp.isIntralineDifference());
     whitespaceErrors.setValue(dp.isShowWhitespaceErrors());
     showTabs.setValue(dp.isShowTabs());
+    skipDeleted.setValue(dp.isSkipDeleted());
+    skipUncommented.setValue(dp.isSkipUncommented());
   }
 
   @UiHandler("update")
@@ -216,6 +225,8 @@ public class PatchScriptSettingsPanel extends Composite implements
     dp.setIntralineDifference(intralineDifference.getValue());
     dp.setShowWhitespaceErrors(whitespaceErrors.getValue());
     dp.setShowTabs(showTabs.getValue());
+    dp.setSkipDeleted(skipDeleted.getValue());
+    dp.setSkipUncommented(skipUncommented.getValue());
 
     listenablePrefs.set(dp);
 
diff --git a/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScriptSettingsPanel.ui.xml b/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScriptSettingsPanel.ui.xml
index 7bbc8fe..a9902d9 100644
--- a/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScriptSettingsPanel.ui.xml
+++ b/gerrit-gwtui/src/main/java/com/google/gerrit/client/patches/PatchScriptSettingsPanel.ui.xml
@@ -116,6 +116,22 @@ limitations under the License.
     </td>
 
     <td valign='bottom' rowspan='2'>
+      <g:CheckBox
+          ui:field='skipUncommented'
+          text='Skip Uncommented Files'
+          tabIndex='11'>
+        <ui:attribute name='text'/>
+      </g:CheckBox>
+      <br/>
+      <g:CheckBox
+          ui:field='skipDeleted'
+          text='Skip Deleted Files'
+          tabIndex='11'>
+        <ui:attribute name='text'/>
+      </g:CheckBox>
+    </td>
+
+    <td valign='bottom' rowspan='2'>
       <g:Button
           ui:field='update'
           text='Update'
@@ -125,7 +141,7 @@ limitations under the License.
       </g:Button>
     </td>
 
-    <td>
+    <td valign='bottom' rowspan='2'>
       <g:CheckBox
           ui:field='reviewed'
           text='Reviewed'
diff --git a/gerrit-reviewdb/src/main/java/com/google/gerrit/reviewdb/AccountDiffPreference.java b/gerrit-reviewdb/src/main/java/com/google/gerrit/reviewdb/AccountDiffPreference.java
index 4a3dd18..38a2359 100644
--- a/gerrit-reviewdb/src/main/java/com/google/gerrit/reviewdb/AccountDiffPreference.java
+++ b/gerrit-reviewdb/src/main/java/com/google/gerrit/reviewdb/AccountDiffPreference.java
@@ -96,6 +96,12 @@ public class AccountDiffPreference {
   @Column(id = 9)
   protected short context;
 
+  @Column(id = 10)
+  protected boolean skipDeleted;
+
+  @Column(id = 11)
+  protected boolean skipUncommented;
+
   protected AccountDiffPreference() {
   }
 
@@ -112,6 +118,8 @@ public class AccountDiffPreference {
     this.showWhitespaceErrors = p.showWhitespaceErrors;
     this.intralineDifference = p.intralineDifference;
     this.showTabs = p.showTabs;
+    this.skipDeleted = p.skipDeleted;
+    this.skipUncommented = p.skipUncommented;
     this.context = p.context;
   }
 
@@ -185,4 +193,20 @@ public class AccountDiffPreference {
     assert 0 <= context || context == WHOLE_FILE_CONTEXT;
     this.context = context;
   }
+
+  public boolean isSkipDeleted() {
+    return skipDeleted;
+  }
+
+  public void setSkipDeleted(boolean skip) {
+    skipDeleted = skip;
+  }
+
+  public boolean isSkipUncommented() {
+    return skipUncommented;
+  }
+
+  public void setSkipUncommented(boolean skip) {
+    skipUncommented = skip;
+  }
 }
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/schema/SchemaVersion.java b/gerrit-server/src/main/java/com/google/gerrit/server/schema/SchemaVersion.java
index 78edbc3..77c5f05 100644
--- a/gerrit-server/src/main/java/com/google/gerrit/server/schema/SchemaVersion.java
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/schema/SchemaVersion.java
@@ -32,7 +32,7 @@ import java.util.List;
 /** A version of the database schema. */
 public abstract class SchemaVersion {
   /** The current schema version. */
-  private static final Class<? extends SchemaVersion> C = Schema_41.class;
+  private static final Class<? extends SchemaVersion> C = Schema_42.class;
 
   public static class Module extends AbstractModule {
     @Override
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/schema/Schema_42.java b/gerrit-server/src/main/java/com/google/gerrit/server/schema/Schema_42.java
new file mode 100644
index 0000000..83bca7b
--- /dev/null
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/schema/Schema_42.java
@@ -0,0 +1,25 @@
+// Copyright (C) 2010 The Android Open Source Project
+//
+// Licensed under the Apache License, Version 2.0 (the "License");
+// you may not use this file except in compliance with the License.
+// You may obtain a copy of the License at
+//
+// http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software
+// distributed under the License is distributed on an "AS IS" BASIS,
+// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+// See the License for the specific language governing permissions and
+// limitations under the License.
+
+package com.google.gerrit.server.schema;
+
+import com.google.inject.Inject;
+import com.google.inject.Provider;
+
+public class Schema_42 extends SchemaVersion {
+  @Inject
+  Schema_42(Provider<Schema_41> prior) {
+    super(prior);
+  }
+}
-- 
1.7.2.2

