From af624e72d047289e4ed7400a93b8733161bf492e Mon Sep 17 00:00:00 2001
From: Martin Fick <mfick@codeaurora.org>
Date: Fri, 23 Jul 2010 17:31:26 -0600
Subject: [PATCH] Use a template to set the contents of the MergedEmails.

Add an admin editable Merged.vm template used to format the
contents of the merged change emails.
---
 Documentation/config-mail.txt                      |  121 ++++++++++++++++++++
 Documentation/index.txt                            |    1 +
 .../gerrit/pgm/init/SitePathInitializer.java       |   11 ++
 .../google/gerrit/server/mail/AbandonedSender.java |    4 +-
 .../gerrit/server/mail/AddReviewerSender.java      |    2 +-
 .../com/google/gerrit/server/mail/ChangeEmail.java |   25 +++--
 .../google/gerrit/server/mail/CommentSender.java   |    4 +-
 .../gerrit/server/mail/CreateChangeSender.java     |    2 +-
 .../google/gerrit/server/mail/MergeFailSender.java |    4 +-
 .../google/gerrit/server/mail/MergedSender.java    |   86 ++++++--------
 .../google/gerrit/server/mail/NewChangeSender.java |    4 +-
 .../google/gerrit/server/mail/OutgoingEmail.java   |    2 +-
 .../gerrit/server/mail/RegisterNewEmailSender.java |    4 +-
 .../gerrit/server/mail/ReplacePatchSetSender.java  |    4 +-
 .../gerrit/server/mail/ReplyToChangeSender.java    |    2 +-
 .../com/google/gerrit/server/mail/Merged.vm        |   51 ++++++++
 16 files changed, 255 insertions(+), 72 deletions(-)
 create mode 100644 Documentation/config-mail.txt
 create mode 100644 gerrit-server/src/main/resources/com/google/gerrit/server/mail/Merged.vm

diff --git a/Documentation/config-mail.txt b/Documentation/config-mail.txt
new file mode 100644
index 0000000..970e0d6
--- /dev/null
+++ b/Documentation/config-mail.txt
@@ -0,0 +1,121 @@
+Gerrit Code Review - Mail Templates
+===================================
+
+Gerrit uses velocity templates for the bulk of the standard mails it sends out.
+There are builtin default templates which are used if they are not overridden.
+These defaults are also provided as examples so that administrators may copy
+them and easily modify them to tweak their contents.
+
+
+Template Locations and Extensions:
+----------------------------------
+
+The default example templates reside under:  `'$site_path'/etc/mail` and are
+terminated with the double extension `.vm.example`. Modifying these example
+files will have no effect on the behavior of Gerrit.  However, copying an
+example template to an equivalently named file without the `.example` extension
+and modifying it will allow an administrator to customize the template.
+
+
+Supported Mail Templates:
+-------------------------
+
+Each mail that Gerrit sends out is controlled by at least one template, these
+are listed below.  Change emails are influenced by two additional templates,
+one to set the subject line, and one to set the footer which gets appended to
+all the change emails (see `ChangeSubject.vm` and `ChangeFooter.vm` below.)
+
+Merged.vm
+~~~~~~~~~
+
+The `Merged.vm` template will determine the contents of the email related to
+a change successfully merged to the head.  It is a `ChangeEmail`: see
+`ChangeSubject.vm` and `ChangeFooter.vm`.
+
+
+Mail Variables and Methods
+--------------------------
+
+Mail templates can access and display objects currently made available to them
+via the velocity context.  While the base objects are documented here, it is
+possible to call public methods on these objects from templates.  Those methods
+are not documented here since they could change with every release.  As these
+templates are meant to be modified only by a qualified sysadmin, it is accepted
+that writing templates for Gerrit emails is likely to require some basic
+knowledge of the class structure to be useful.  Browsing the source code might
+be necessary for anything more than a minor formatting change.
+
+Warning
+~~~~~~~
+
+Be aware that modifying templates can cause them to fail to parse and therefor
+not send out the actual email, or worse, calling methods on the available
+objects could have internal side effects which would adversely affect the
+health of your Gerrit server and/or data.
+
+All OugoingEmails
+~~~~~~~~~~~~~~~~~
+
+All outgoing emails have the following variables available to them:
+
+$email::
++
+A reference to the class constructing the current `OutgoinEmail`.  With this
+reference it is possible to call any public method on the OutgoinEmail class
+or the current child class inherited from it.
+
+$messageClass::
++
+A String containing the messageClass
+
+$StringUtils::
++
+A reference to the Apache `StringUtils` class.  This can be very useful for
+formatting strings.
+
+Change Emails
+~~~~~~~~~~~~~
+
+All change related emails have the following additional variables available to them:
+
+$change::
++
+A reference to the current `Change` object
+
+$changeId::
++
+Id of the current change (a `Change.Key`)
+
+$coverLetter::
++
+The txt of the `ChangeMessage`
+
+$branch::
++
+A reference to the branch of this change (a `Branch.NameKey`)
+
+$fromName::
++
+The name of the from user
+
+$projectName::
++
+The name of this change's project
+
+$patchSet::
++
+A reference to the current `PatchSet`
+
+$patchSetInfo::
++
+A reference to the current `PatchSetInfo`
+
+
+See Also
+--------
+
+* link:http://velocity.apache.org/[velocity]
+
+GERRIT
+------
+Part of link:index.html[Gerrit Code Review]
diff --git a/Documentation/index.txt b/Documentation/index.txt
index 1e13e8d..e419455 100644
--- a/Documentation/index.txt
+++ b/Documentation/index.txt
@@ -31,6 +31,7 @@ Configuration
 * link:config-sso.html[Single Sign-On Systems]
 * link:config-apache2.html[Apache 2 Reverse Proxy]
 * link:config-hooks.html[Hooks]
+* link:config-mail.html[Mail Templates]
 
 Developer Documentation
 -----------------------
diff --git a/gerrit-pgm/src/main/java/com/google/gerrit/pgm/init/SitePathInitializer.java b/gerrit-pgm/src/main/java/com/google/gerrit/pgm/init/SitePathInitializer.java
index 74e7548..e33e95d 100644
--- a/gerrit-pgm/src/main/java/com/google/gerrit/pgm/init/SitePathInitializer.java
+++ b/gerrit-pgm/src/main/java/com/google/gerrit/pgm/init/SitePathInitializer.java
@@ -25,11 +25,13 @@ import static com.google.gerrit.pgm.init.InitUtil.version;
 import com.google.gerrit.pgm.Init;
 import com.google.gerrit.pgm.util.ConsoleUI;
 import com.google.gerrit.server.config.SitePaths;
+import com.google.gerrit.server.mail.OutgoingEmail;
 import com.google.inject.Binding;
 import com.google.inject.Inject;
 import com.google.inject.Injector;
 import com.google.inject.TypeLiteral;
 
+import java.io.File;
 import java.util.ArrayList;
 import java.util.List;
 
@@ -66,6 +68,7 @@ public class SitePathInitializer {
     mkdir(site.etc_dir);
     mkdir(site.lib_dir);
     mkdir(site.logs_dir);
+    mkdir(site.mail_dir);
     mkdir(site.static_dir);
 
     for (InitStep step : steps) {
@@ -82,11 +85,19 @@ public class SitePathInitializer {
     extract(site.gerrit_sh, Init.class, "gerrit.sh");
     chmod(0755, site.gerrit_sh);
 
+    extractMailExample("Merged.vm");
+
     if (!ui.isBatch()) {
       System.err.println();
     }
   }
 
+  private void extractMailExample(String orig) throws Exception {
+    File ex = new File(site.mail_dir, orig + ".example");
+    extract(ex, OutgoingEmail.class, orig);
+    chmod(0444, ex);
+  }
+
   private static List<InitStep> stepsOf(final Injector injector) {
     final ArrayList<InitStep> r = new ArrayList<InitStep>();
     for (Binding<InitStep> b : all(injector)) {
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/mail/AbandonedSender.java b/gerrit-server/src/main/java/com/google/gerrit/server/mail/AbandonedSender.java
index d2b5c29..d6bd223 100644
--- a/gerrit-server/src/main/java/com/google/gerrit/server/mail/AbandonedSender.java
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/mail/AbandonedSender.java
@@ -30,7 +30,7 @@ public class AbandonedSender extends ReplyToChangeSender {
   }
 
   @Override
-  protected void init() {
+  protected void init() throws EmailException {
     super.init();
 
     ccAllApprovals();
@@ -39,7 +39,7 @@ public class AbandonedSender extends ReplyToChangeSender {
   }
 
   @Override
-  protected void formatChange() {
+  protected void formatChange() throws EmailException {
     appendText(getNameFor(fromId));
     appendText(" has abandoned change " + change.getKey().abbreviate() + ":\n");
     appendText("\n");
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/mail/AddReviewerSender.java b/gerrit-server/src/main/java/com/google/gerrit/server/mail/AddReviewerSender.java
index be62ba0..e5437cf 100644
--- a/gerrit-server/src/main/java/com/google/gerrit/server/mail/AddReviewerSender.java
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/mail/AddReviewerSender.java
@@ -32,7 +32,7 @@ public class AddReviewerSender extends NewChangeSender {
   }
 
   @Override
-  protected void init() {
+  protected void init() throws EmailException {
     super.init();
 
     ccExistingReviewers();
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/mail/ChangeEmail.java b/gerrit-server/src/main/java/com/google/gerrit/server/mail/ChangeEmail.java
index fa10784..cf558d6 100644
--- a/gerrit-server/src/main/java/com/google/gerrit/server/mail/ChangeEmail.java
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/mail/ChangeEmail.java
@@ -122,7 +122,7 @@ public abstract class ChangeEmail extends OutgoingEmail {
   protected abstract void formatChange() throws EmailException;
 
   /** Setup the message headers and envelope (TO, CC, BCC). */
-  protected void init() {
+  protected void init() throws EmailException {
     if (args.projectCache != null) {
       projectState = args.projectCache.get(change.getProject());
       projectName =
@@ -267,21 +267,27 @@ public abstract class ChangeEmail extends OutgoingEmail {
 
   /** Format the change message and the affected file list. */
   protected void formatChangeDetail() {
+    appendText(getChangeDetail());
+  }
+
+  /** Create the change message and the affected file list. */
+  public String getChangeDetail() {
+    StringBuilder detail = new StringBuilder();
+
     if (patchSetInfo != null) {
-      appendText(patchSetInfo.getMessage().trim());
-      appendText("\n");
+      detail.append(patchSetInfo.getMessage().trim() + "\n");
     } else {
-      appendText(change.getSubject().trim());
-      appendText("\n");
+      detail.append(change.getSubject().trim() + "\n");
     }
 
     if (patchSet != null) {
-      appendText("---\n");
+      detail.append("---\n");
       for (PatchListEntry p : getPatchList().getPatches()) {
-        appendText(p.getChangeType().getCode() + " " + p.getNewName() + "\n");
+        detail.append(p.getChangeType().getCode() + " " + p.getNewName() + "\n");
       }
-      appendText("\n");
+      detail.append("\n");
     }
+    return detail.toString();
   }
 
   /** Get the patch list corresponding to this patch set. */
@@ -434,7 +440,10 @@ public abstract class ChangeEmail extends OutgoingEmail {
   protected void setupVelocityContext() {
     super.setupVelocityContext();
     velocityContext.put("change", change);
+    velocityContext.put("changeId", change.getKey());
+    velocityContext.put("coverLetter", getCoverLetter());
     velocityContext.put("branch", change.getDest());
+    velocityContext.put("fromName", getNameFor(fromId));
     velocityContext.put("projectName", projectName);
     velocityContext.put("patchSet", patchSet);
     velocityContext.put("patchSetInfo", patchSetInfo);
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/mail/CommentSender.java b/gerrit-server/src/main/java/com/google/gerrit/server/mail/CommentSender.java
index be6360f..8783d7c 100644
--- a/gerrit-server/src/main/java/com/google/gerrit/server/mail/CommentSender.java
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/mail/CommentSender.java
@@ -56,7 +56,7 @@ public class CommentSender extends ReplyToChangeSender {
   }
 
   @Override
-  protected void init() {
+  protected void init() throws EmailException {
     super.init();
 
     ccAllApprovals();
@@ -65,7 +65,7 @@ public class CommentSender extends ReplyToChangeSender {
   }
 
   @Override
-  protected void formatChange() {
+  protected void formatChange() throws EmailException {
     if (!"".equals(getCoverLetter()) || !inlineComments.isEmpty()) {
       appendText("Comments on Patch Set " + patchSet.getPatchSetId() + ":\n");
       appendText("\n");
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/mail/CreateChangeSender.java b/gerrit-server/src/main/java/com/google/gerrit/server/mail/CreateChangeSender.java
index ea57cde..18bfe97 100644
--- a/gerrit-server/src/main/java/com/google/gerrit/server/mail/CreateChangeSender.java
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/mail/CreateChangeSender.java
@@ -40,7 +40,7 @@ public class CreateChangeSender extends NewChangeSender {
   }
 
   @Override
-  protected void init() {
+  protected void init() throws EmailException {
     super.init();
 
     bccWatchers();
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/mail/MergeFailSender.java b/gerrit-server/src/main/java/com/google/gerrit/server/mail/MergeFailSender.java
index 00750ef..de7871b 100644
--- a/gerrit-server/src/main/java/com/google/gerrit/server/mail/MergeFailSender.java
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/mail/MergeFailSender.java
@@ -30,14 +30,14 @@ public class MergeFailSender extends ReplyToChangeSender {
   }
 
   @Override
-  protected void init() {
+  protected void init() throws EmailException {
     super.init();
 
     ccExistingReviewers();
   }
 
   @Override
-  protected void formatChange() {
+  protected void formatChange() throws EmailException {
     appendText("Change " + change.getKey().abbreviate());
     if (patchSetInfo != null && patchSetInfo.getAuthor() != null
         && patchSetInfo.getAuthor().getName() != null) {
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/mail/MergedSender.java b/gerrit-server/src/main/java/com/google/gerrit/server/mail/MergedSender.java
index caf19e4..40f4790 100644
--- a/gerrit-server/src/main/java/com/google/gerrit/server/mail/MergedSender.java
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/mail/MergedSender.java
@@ -51,7 +51,7 @@ public class MergedSender extends ReplyToChangeSender {
   }
 
   @Override
-  protected void init() {
+  protected void init() throws EmailException {
     super.init();
 
     ccAllApprovals();
@@ -61,58 +61,47 @@ public class MergedSender extends ReplyToChangeSender {
   }
 
   @Override
-  protected void formatChange() {
-    appendText("Change " + change.getKey().abbreviate());
-    if (patchSetInfo != null && patchSetInfo.getAuthor() != null
-        && patchSetInfo.getAuthor().getName() != null) {
-      appendText(" by ");
-      appendText(patchSetInfo.getAuthor().getName());
-    }
-    appendText(" submitted to ");
-    appendText(dest.getShortName());
-    appendText(":\n\n");
-    formatChangeDetail();
-    formatApprovals();
+  protected void formatChange() throws EmailException {
+    appendText(velocifyFile("Merged.vm"));
   }
 
-  private void formatApprovals() {
-    if (patchSet != null) {
-      try {
-        final Map<Account.Id, Map<ApprovalCategory.Id, PatchSetApproval>> pos =
-            new HashMap<Account.Id, Map<ApprovalCategory.Id, PatchSetApproval>>();
-
-        final Map<Account.Id, Map<ApprovalCategory.Id, PatchSetApproval>> neg =
-            new HashMap<Account.Id, Map<ApprovalCategory.Id, PatchSetApproval>>();
-
-        for (PatchSetApproval ca : args.db.get().patchSetApprovals()
-            .byPatchSet(patchSet.getId())) {
-          if (ca.getValue() > 0) {
-            insert(pos, ca);
-          } else if (ca.getValue() < 0) {
-            insert(neg, ca);
-          }
+  public String getApprovals() {
+    try {
+      final Map<Account.Id, Map<ApprovalCategory.Id, PatchSetApproval>> pos =
+          new HashMap<Account.Id, Map<ApprovalCategory.Id, PatchSetApproval>>();
+
+      final Map<Account.Id, Map<ApprovalCategory.Id, PatchSetApproval>> neg =
+          new HashMap<Account.Id, Map<ApprovalCategory.Id, PatchSetApproval>>();
+
+      for (PatchSetApproval ca : args.db.get().patchSetApprovals()
+          .byPatchSet(patchSet.getId())) {
+        if (ca.getValue() > 0) {
+          insert(pos, ca);
+        } else if (ca.getValue() < 0) {
+          insert(neg, ca);
         }
-
-        format("Approvals", pos);
-        format("Objections", neg);
-      } catch (OrmException err) {
-        // Don't list the approvals
       }
+
+      return format("Approvals", pos) + format("Objections", neg);
+    } catch (OrmException err) {
+      // Don't list the approvals
     }
+    return "";
   }
 
-  private void format(final String type,
+  private String format(final String type,
       final Map<Account.Id, Map<ApprovalCategory.Id, PatchSetApproval>> list) {
+    StringBuilder txt = new StringBuilder();
     if (list.isEmpty()) {
-      return;
+      return "";
     }
-    appendText(type + ":\n");
+    txt.append(type + ":\n");
     for (final Map.Entry<Account.Id, Map<ApprovalCategory.Id, PatchSetApproval>> ent : list
         .entrySet()) {
       final Map<ApprovalCategory.Id, PatchSetApproval> l = ent.getValue();
-      appendText("  ");
-      appendText(getNameFor(ent.getKey()));
-      appendText(": ");
+      txt.append("  ");
+      txt.append(getNameFor(ent.getKey()));
+      txt.append(": ");
       boolean first = true;
       for (ApprovalType at : approvalTypes.getApprovalTypes()) {
         final PatchSetApproval ca = l.get(at.getCategory().getId());
@@ -123,24 +112,25 @@ public class MergedSender extends ReplyToChangeSender {
         if (first) {
           first = false;
         } else {
-          appendText("; ");
+          txt.append("; ");
         }
 
         final ApprovalCategoryValue v = at.getValue(ca);
         if (v != null) {
-          appendText(v.getName());
+          txt.append(v.getName());
         } else {
-          appendText(at.getCategory().getName());
-          appendText("=");
+          txt.append(at.getCategory().getName());
+          txt.append("=");
           if (ca.getValue() > 0) {
-            appendText("+");
+            txt.append("+");
           }
-          appendText("" + ca.getValue());
+          txt.append("" + ca.getValue());
         }
       }
-      appendText("\n");
+      txt.append("\n");
     }
-    appendText("\n");
+    txt.append("\n");
+    return txt.toString();
   }
 
   private void insert(
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/mail/NewChangeSender.java b/gerrit-server/src/main/java/com/google/gerrit/server/mail/NewChangeSender.java
index dc8c2c2..54e72a8 100644
--- a/gerrit-server/src/main/java/com/google/gerrit/server/mail/NewChangeSender.java
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/mail/NewChangeSender.java
@@ -46,7 +46,7 @@ public abstract class NewChangeSender extends ChangeEmail {
   }
 
   @Override
-  protected void init() {
+  protected void init() throws EmailException {
     super.init();
 
     setHeader("Message-ID", getChangeMessageThreadId());
@@ -57,7 +57,7 @@ public abstract class NewChangeSender extends ChangeEmail {
   }
 
   @Override
-  protected void formatChange() {
+  protected void formatChange() throws EmailException {
     formatSalutation();
     formatChangeDetail();
 
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/mail/OutgoingEmail.java b/gerrit-server/src/main/java/com/google/gerrit/server/mail/OutgoingEmail.java
index e762b77..053c01d 100644
--- a/gerrit-server/src/main/java/com/google/gerrit/server/mail/OutgoingEmail.java
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/mail/OutgoingEmail.java
@@ -124,7 +124,7 @@ public abstract class OutgoingEmail {
   protected abstract void format() throws EmailException;
 
   /** Setup the message headers and envelope (TO, CC, BCC). */
-  protected void init() {
+  protected void init() throws EmailException {
     setupVelocityContext();
 
     smtpFromAddress = args.fromAddressGenerator.from(fromId);
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/mail/RegisterNewEmailSender.java b/gerrit-server/src/main/java/com/google/gerrit/server/mail/RegisterNewEmailSender.java
index 9b201fd..5bba5d9 100644
--- a/gerrit-server/src/main/java/com/google/gerrit/server/mail/RegisterNewEmailSender.java
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/mail/RegisterNewEmailSender.java
@@ -40,7 +40,7 @@ public class RegisterNewEmailSender extends OutgoingEmail {
   }
 
   @Override
-  protected void init() {
+  protected void init() throws EmailException {
     super.init();
     setHeader("Subject", "[Gerrit Code Review] Email Verification");
     add(RecipientType.TO, new Address(addr));
@@ -52,7 +52,7 @@ public class RegisterNewEmailSender extends OutgoingEmail {
   }
 
   @Override
-  protected void format() {
+  protected void format() throws EmailException {
     final StringBuilder url = new StringBuilder();
     url.append(getGerritUrl());
     url.append("#VE,");
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/mail/ReplacePatchSetSender.java b/gerrit-server/src/main/java/com/google/gerrit/server/mail/ReplacePatchSetSender.java
index 841aa35..526458a 100644
--- a/gerrit-server/src/main/java/com/google/gerrit/server/mail/ReplacePatchSetSender.java
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/mail/ReplacePatchSetSender.java
@@ -53,7 +53,7 @@ public class ReplacePatchSetSender extends ReplyToChangeSender {
   }
 
   @Override
-  protected void init() {
+  protected void init() throws EmailException {
     super.init();
 
     if (fromId != null) {
@@ -67,7 +67,7 @@ public class ReplacePatchSetSender extends ReplyToChangeSender {
   }
 
   @Override
-  protected void formatChange() {
+  protected void formatChange() throws EmailException {
     formatSalutation();
     formatChangeDetail();
 
diff --git a/gerrit-server/src/main/java/com/google/gerrit/server/mail/ReplyToChangeSender.java b/gerrit-server/src/main/java/com/google/gerrit/server/mail/ReplyToChangeSender.java
index 05d2753..4c3ed76 100644
--- a/gerrit-server/src/main/java/com/google/gerrit/server/mail/ReplyToChangeSender.java
+++ b/gerrit-server/src/main/java/com/google/gerrit/server/mail/ReplyToChangeSender.java
@@ -23,7 +23,7 @@ public abstract class ReplyToChangeSender extends ChangeEmail {
   }
 
   @Override
-  protected void init() {
+  protected void init() throws EmailException {
     super.init();
 
     final String threadId = getChangeMessageThreadId();
diff --git a/gerrit-server/src/main/resources/com/google/gerrit/server/mail/Merged.vm b/gerrit-server/src/main/resources/com/google/gerrit/server/mail/Merged.vm
new file mode 100644
index 0000000..9b89fa7
--- /dev/null
+++ b/gerrit-server/src/main/resources/com/google/gerrit/server/mail/Merged.vm
@@ -0,0 +1,51 @@
+## Copyright (c) 2010, Code Aurora Forum. All rights reserved.
+##
+## Redistribution and use in source and binary forms, with or without
+## modification, are permitted provided that the following conditions are
+## met:
+##     * Redistributions of source code must retain the above copyright
+##       notice, this list of conditions and the following disclaimer.
+##     * Redistributions in binary form must reproduce the above
+##       copyright notice, this list of conditions and the following
+##       disclaimer in the documentation and/or other materials provided
+##       with the distribution.
+##     * Neither the name of Code Aurora Forum, Inc. nor the names of its
+##       contributors may be used to endorse or promote products derived
+##       from this software without specific prior written permission.
+##
+## THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
+## WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
+## MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
+## ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
+## BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
+## CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
+## SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
+## BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
+## WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
+## OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
+## IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
+##
+##
+## Template Type:
+## -------------
+## This is a velocity mail template, see: http://velocity.apache.org and the
+## gerrit-docs:config-mail.txt for more info on modifying gerrit mail templates.
+##
+## Template File Names and extensions:
+## ----------------------------------
+## Gerrit will use templates ending in ".vm" but will ignore templates ending
+## in ".vm.example".  If a .vm template does not exist, the default internal
+## gerrit template which is the same as the .vm.example will be used.  If you
+## want to override the default template, copy the .vm.exmaple file to a .vm
+## file and edit it appropriately.
+##
+## This Template:
+## --------------
+## The Merged.vm template will determine the contents of the email related to
+## a change successfully merged to the head.  It is a ChangeEmail: see
+## ChangeSubject.vm and ChangeFooter.vm.
+##
+Change $changeId.abbreviate()#if ($patchSetInfo.author.name)
+ by $patchSetInfo.author.name#end submitted to $change.dest.shortName:
+
+$email.changeDetail$email.approvals
-- 
1.7.2

