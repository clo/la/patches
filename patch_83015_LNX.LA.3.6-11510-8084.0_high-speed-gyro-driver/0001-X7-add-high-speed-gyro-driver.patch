From 0e7e27884a8b268b9ee60dbf1516f7d56454ecb2 Mon Sep 17 00:00:00 2001
From: Mike Mager <mikemager@codeaurora.org>
Date: Tue, 19 Aug 2014 12:33:43 -0700
Subject: [PATCH] ODG X7: add high speed gyro driver

Change-Id: Ic5c97c991c861e70b872b2a8ba32b264cfd0b8a3
---
 drivers/char/Makefile   |   2 +
 drivers/char/thor_hsg.c | 319 ++++++++++++++++++++++++++++++++++++++++++++++++
 2 files changed, 321 insertions(+)
 create mode 100644 drivers/char/thor_hsg.c

diff --git a/drivers/char/Makefile b/drivers/char/Makefile
index d5b3d50..e4616fc 100644
--- a/drivers/char/Makefile
+++ b/drivers/char/Makefile
@@ -65,3 +65,5 @@ obj-$(CONFIG_DIAG_CHAR)		+= diag/
 obj-$(CONFIG_MSM_ADSPRPC)       += adsprpc.o
 obj-$(CONFIG_MSM_RDBG)       += rdbg.o
 obj-$(CONFIG_MSM_SMD_PKT)       += msm_smd_pkt.o
+
+obj-y				+= thor_hsg.o
diff --git a/drivers/char/thor_hsg.c b/drivers/char/thor_hsg.c
new file mode 100644
index 0000000..4fc869a
--- /dev/null
+++ b/drivers/char/thor_hsg.c
@@ -0,0 +1,319 @@
+/*
+ * Copyright (c) 2014, The Linux Foundation. All rights reserved.
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License version 2 and
+ * only version 2 as published by the Free Software Foundation.
+ *
+ * This program is distributed in the hope that it will be useful,
+ * but WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ * GNU General Public License for more details.
+ *
+ */
+#include <linux/types.h>
+#include <linux/cdev.h>
+#include <linux/gfp.h>
+#include <linux/device.h>
+#include <linux/fs.h>
+#include <linux/slab.h>
+#include <linux/module.h>
+#include <mach/msm_smsm.h>
+#include <linux/uaccess.h>
+#include <asm/ioctls.h>
+#include <linux/io.h>
+#include <linux/mman.h>
+#include <linux/mm.h>
+#include <mach/msm_smsm.h>
+
+#define THOR_HSG_IOCTL_BASE			(('T'<<24)|('h'<<16)|('o'<<8)|('r'))
+#define THOR_HSG_IOCTL_READ_DATA	(THOR_HSG_IOCTL_BASE+0)
+
+#define THOR_HSG_MAGIC_NUMBER		0xF037		// in our context to recognize damaged pointers
+
+#define THOR_HSG_SHARED_MEM_SIZE	4096
+
+struct thor_hsg_data {
+	struct device *device;
+	bool   device_initialized;
+	bool   device_opened;
+	void   *smem_addr;
+	size_t smem_size;
+	struct mutex lock;
+};
+
+struct thor_hsg_device {
+	struct cdev cdev;
+	struct class *class;
+	dev_t dev_no;
+	struct thor_hsg_data *thor_hsg_data;
+};
+
+struct thor_hsg_user_data {
+	unsigned int length;
+	void *data;
+};
+
+static struct thor_hsg_device g_thor_hsg_instance = {
+	{ {0} },
+	NULL,
+	0,
+	NULL
+};
+
+static int check_hsg_available(void *base_addr)
+{
+	unsigned int *adsp_data = (unsigned int*) base_addr;
+	if (*adsp_data == THOR_HSG_MAGIC_NUMBER) {
+		return 0;
+	}
+	pr_err("%s: bad magic number: %08x\n", __func__, *adsp_data);
+	return 1;
+}
+
+static int thor_hsg_init_shared_mem(struct thor_hsg_data *thor_hsg_data)
+{
+	int err = 0;
+
+	thor_hsg_data->smem_size = THOR_HSG_SHARED_MEM_SIZE;
+	thor_hsg_data->smem_addr = smem_find(
+		426,
+		thor_hsg_data->smem_size,
+		0,
+		SMEM_ANY_HOST_FLAG
+	);
+
+	if (!thor_hsg_data->smem_addr) {
+		dev_err(thor_hsg_data->device, "%s: Could not allocate smem memory",
+			__func__);
+		err = -ENOMEM;
+		goto bail;
+	}
+
+	dev_dbg(thor_hsg_data->device, "%s: SMEM address=0x%x smem_size=%d",
+		__func__, (unsigned int)thor_hsg_data->smem_addr,
+		thor_hsg_data->smem_size);
+
+	if (check_hsg_available(thor_hsg_data->smem_addr)) {
+		dev_err(thor_hsg_data->device, "%s: Data from ADSP is not available",
+			__func__);
+		err = -ECOMM;
+		goto bail;
+	}
+
+bail:
+	return err;
+}
+
+static int thor_hsg_open(struct inode *inode, struct file *filp)
+{
+	struct thor_hsg_device *device = &g_thor_hsg_instance;
+	struct thor_hsg_data *thor_hsg_data = NULL;
+	int err = 0;
+
+	if (!inode || !device->thor_hsg_data) {
+		pr_err("Memory not allocated yet");
+		err = -ENODEV;
+		goto bail;
+	}
+
+	thor_hsg_data = &device->thor_hsg_data[0];
+
+	if (thor_hsg_data->device_opened) {
+		dev_err(thor_hsg_data->device, "%s: Device already opened",
+			__func__);
+		err = -EEXIST;
+		goto bail;
+	}
+
+	err = thor_hsg_init_shared_mem(thor_hsg_data);
+	if (err != 0) {
+		goto bail;
+	}
+
+	mutex_init(&thor_hsg_data->lock);
+
+	thor_hsg_data->device_opened = 1;
+
+	filp->private_data = (void *)thor_hsg_data;
+
+	return 0;
+
+bail:
+	return err;
+}
+
+static int thor_hsg_release(struct inode *inode, struct file *filp)
+{
+	struct thor_hsg_device *thor_hsgdevice = &g_thor_hsg_instance;
+	struct thor_hsg_data *thor_hsg_data = NULL;
+	int err = 0;
+
+	if (!inode || !thor_hsgdevice->thor_hsg_data) {
+		pr_err("Memory not allocated yet");
+		err = -ENODEV;
+		goto bail;
+	}
+
+	thor_hsg_data = &thor_hsgdevice->thor_hsg_data[0];
+
+	if (thor_hsg_data->device_opened == 1) {
+		dev_dbg(thor_hsg_data->device, "%s: Destroying %s.", __func__,
+			"thor_hsg_adsp");
+		thor_hsg_data->device_opened = 0;
+		mutex_destroy(&thor_hsg_data->lock);
+	}
+
+	filp->private_data = NULL;
+
+bail:
+	return err;
+}
+
+static long thor_hsg_ioctl(struct file *file, unsigned int cmd,
+					     unsigned long arg)
+{
+	int ret;
+	struct thor_hsg_data *thor_hsg_data = file->private_data;
+
+	if (!thor_hsg_data)
+		return -EINVAL;
+
+	mutex_lock(&thor_hsg_data->lock);
+	switch (cmd) {
+	case THOR_HSG_IOCTL_READ_DATA:
+		if (arg != 0) {
+			struct thor_hsg_user_data udata;
+			ret = copy_from_user(&udata, (void*) arg, sizeof(udata));
+			if (ret != 0) {
+				pr_err("%s: Error in copy_from_user(). Err code = %d",
+					__func__, ret);
+				ret = -ENODATA;
+				break;
+			}
+			if (udata.length == 0 || udata.data == NULL) {
+				pr_err("%s: Invalid thor_hsg_user_data struct: %d %p.", __func__, udata.length, udata.data);
+				ret = -ENODATA;
+				break;
+			}
+			if (udata.length > thor_hsg_data->smem_size) {
+				pr_err("%s: Length too big: %d > %d.", __func__, udata.length, thor_hsg_data->smem_size);
+				ret = -ENODATA;
+				break;
+			}
+			ret = copy_to_user((void*) udata.data, thor_hsg_data->smem_addr, 
+				udata.length);
+			if (ret != 0) {
+				pr_err("%s: Error in copy_to_user(). Err code = %d",
+					__func__, ret);
+				ret = -ENODATA;
+				break;
+			}
+		} else {
+			ret = -1;
+		}
+		break;
+	default:
+		pr_err("%s: Unrecognized ioctl command %d\n", __func__, cmd);
+		ret = -ENOIOCTLCMD;
+	}
+	mutex_unlock(&thor_hsg_data->lock);
+
+	return ret;
+}
+
+static const struct file_operations thor_hsg_fops = {
+	.open = thor_hsg_open,
+	.release = thor_hsg_release,
+	.unlocked_ioctl = thor_hsg_ioctl,
+	.compat_ioctl = thor_hsg_ioctl,
+};
+
+static int __init thor_hsg_init(void)
+{
+	int err = 0;
+	struct thor_hsg_device *thor_hsgdevice = &g_thor_hsg_instance;
+	int major = 0;
+
+	pr_err("%s: IOCTL is %08x\n", __func__, THOR_HSG_IOCTL_READ_DATA);
+
+	thor_hsgdevice->thor_hsg_data = kcalloc(1,
+		sizeof(struct thor_hsg_data), GFP_KERNEL);
+	if (!thor_hsgdevice->thor_hsg_data) {
+		pr_err("Not enough memory for thor_hsg devices");
+		err = -ENOMEM;
+		goto bail;
+	}
+
+	err = alloc_chrdev_region(&thor_hsgdevice->dev_no, 0,
+		1, "thor_hsgctl");
+	if (err) {
+		pr_err("Error in alloc_chrdev_region.");
+		goto data_bail;
+	}
+	major = MAJOR(thor_hsgdevice->dev_no);
+
+	cdev_init(&thor_hsgdevice->cdev, &thor_hsg_fops);
+	thor_hsgdevice->cdev.owner = THIS_MODULE;
+	err = cdev_add(&thor_hsgdevice->cdev, MKDEV(major, 0), 1);
+	if (err) {
+		pr_err("Error in cdev_add");
+		goto chrdev_bail;
+	}
+
+	thor_hsgdevice->class = class_create(THIS_MODULE, "thor_hsg");
+	if (IS_ERR(thor_hsgdevice->class)) {
+		err = PTR_ERR(thor_hsgdevice->class);
+		pr_err("Error in class_create");
+		goto cdev_bail;
+	}
+
+	thor_hsgdevice->thor_hsg_data[0].device = device_create(
+		thor_hsgdevice->class, NULL, MKDEV(major, 0),
+		NULL, "%s", "thor_hsg");
+	if (IS_ERR(thor_hsgdevice->thor_hsg_data[0].device)) {
+		err = PTR_ERR(thor_hsgdevice->thor_hsg_data[0].device);
+		pr_err("Error in device_create");
+		goto device_bail;
+	}
+	thor_hsgdevice->thor_hsg_data[0].device_initialized = 1;
+	dev_dbg(thor_hsgdevice->thor_hsg_data[0].device,
+		"%s: created /dev/%s c %d %d'", __func__,
+		"thor_hsg_adsp", major, 0);
+
+	goto bail;
+
+device_bail:
+	if (thor_hsgdevice->thor_hsg_data[0].device_initialized)
+		device_destroy(thor_hsgdevice->class,
+			MKDEV(MAJOR(thor_hsgdevice->dev_no), 0));
+	class_destroy(thor_hsgdevice->class);
+cdev_bail:
+	cdev_del(&thor_hsgdevice->cdev);
+chrdev_bail:
+	unregister_chrdev_region(thor_hsgdevice->dev_no, 1);
+data_bail:
+	kfree(thor_hsgdevice->thor_hsg_data);
+bail:
+	return err;
+}
+
+static void __exit thor_hsg_exit(void)
+{
+	struct thor_hsg_device *thor_hsgdevice = &g_thor_hsg_instance;
+
+	if (thor_hsgdevice->thor_hsg_data[0].device_initialized) {
+		device_destroy(thor_hsgdevice->class,
+			MKDEV(MAJOR(thor_hsgdevice->dev_no), 0));
+	}
+	class_destroy(thor_hsgdevice->class);
+	cdev_del(&thor_hsgdevice->cdev);
+	unregister_chrdev_region(thor_hsgdevice->dev_no, 1);
+	kfree(thor_hsgdevice->thor_hsg_data);
+}
+
+module_init(thor_hsg_init);
+module_exit(thor_hsg_exit);
+
+MODULE_DESCRIPTION("Thor High Speed Gyro module");
+MODULE_LICENSE("GPL v2");
-- 
1.8.2.1

